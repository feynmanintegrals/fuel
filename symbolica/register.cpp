#include <iostream>
#include <string>

#include "../library/fuel.h"

int main(int argc, char* argv[]) {
    if (argc != 4) {
        std::cout << "Syntax is symbolica/register name email company" << std::endl;
        return 1;
    }

    unique_ptr<fuel::base> ext = fuel::libraries::create("symbolica");
    dynamic_cast<fuel::symbolica*>(ext.get())->requestTrialLicense(argv[1], argv[2], argv[3]);

    std::cout << "License requested, check your email" << std::endl;

    return 0;
}
