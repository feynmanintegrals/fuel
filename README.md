### Usage instructions

Compile the static library with the following shell commands. Options can be supplied to the `configure` script for installation of third-party simplifiers, e.g.,

    configure --enable-flint --enable-symbolica
    make dep
    make

You should include the `library/calc.h` header in your code and link it with `library/libfuel.a`. For this you can copy the library to a libary include location and add `-lfuel` to linking flags.

The main syntax is demonstrated in the benchmark program `benchmarks/test.cpp`. More simple examples will be added in future.

The following is available:
- `fuel::setLibraryPath(name, path)`: sets needed library binary path for library
- `fuel::readLibraryPathsFromFile(filename)`: reads paths from file. Syntax is library: path on separate lines. Lines starting from `#` are comments
- `fuel::library`: static string variable, library name, fermat by default
- `fuel::initialize(vars, count, silent, prime=0)`: launch `count` instances of library. It might be usefull to launch multiple if you are planning multithreaded usage
- `fuel::simplify(expr, number)`: uses library instance number to simplify `expr`. If primes are needed, expr begins from `%`
- `fuel::switchToConventional()`: by default a calculation library might use internal format for returned expressions suiatable for algebraic operations and sending back to the same library. But in case one needs a final human-readable result suitable elsewhere, call this method first
- `fuel::close()`

There is also a lower-level syntax that might use different libraries at the same time:
- `fuel::libraries::exists(string)`: check whether the library by the following name exists
- `fuel::libraries::supportsPrimes(string)`: check whether the library supports modular calculations
- `unique_ptr<fuel::base> ext = fuel::libraries::create(string)`: create evaluation instance library by name. The instance is destructed apon the destruction of the unique pointer
- `ext->initialize(libraryPath, vars, prime)`: should be called once to initialize the library. The libaryPath should point to the executable binary of the corresponsing simplifier. `vars` is of `const vector<string>&` type listing all possible variable names that will appear in your calculation. Some libraries such as `fermat` support only lower-case letters, please refer to library documentation. `prime` might be either 0 or a prime number that will be used for modular calculations
- `ext->evaluate(expr, modular)`: simplifies expression expr. If modular arithmetics should be used, the second parameter is set to true and expression begins from "%"
- `ext->switchToConventional()`: by default a calculation library might use internal format for returned expressions suiatable for algebraic operations and sending back to the same library. But in case one needs a final result suitable elsewhere, call this method first

There is no need to explicitly close the simplifier as it is done in the destructor.

The current list of library names is

- fermat
- ginac
- math
- maple
- pari
- symbolica
- flint

### Library recomendations

#### Fermat

Fermat is bundled in `extra/ferl64`. It can be downloaded from <http://home.bway.net/lewis/> as a binary. The executable binary is **ferl** for Linux and **ferm** for Max. For tests we used version 5.17.

#### Ginac

Ginac itself is a library, so it has no ready binary for calculations.

#### Mathematica

Mathematica is a commercial product obtained at <https://www.wolfram.com/mathematica/>. The executable binary is **math**. For tests we used version 13.0

#### Maple

Maple is a commercial product obtained at <https://www.maplesoft.com/>. The executable binary is **maple**. For tests we used version 2022.

#### Pari/GP

We recommend taking Pari/GP from <https://pari.math.u-bordeaux.fr/> and building it from sources. You should point to the **gp** binary. For tests we used version 2.14.0 or commit 871e35. Alternatively apt-get install pari-gp in Ubuntu 22.04 installs version 2.13.3-1.


#### Symbolica

We recommend storing the symbolica license in the ~/.symbolica/main_license file, not in an environment variable. Then FUEL can request temporary licences and put them in ~/.symbolica/main_license. The temporary license is valid for 24 hours so after 23 hours a new start will delete the temp file and request a new license. To force an update ealier call symbolica/update_temp_license.sh from the fuel folder. Call make symbolica to build a small tool in symbolica folder that can request temporary symbolica license
