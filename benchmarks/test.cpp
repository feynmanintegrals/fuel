#include <chrono>
#include <cstring>
#include <iostream>
#include <iterator>
#include <string>
#include <numeric>
#include <vector>
#include <fstream>
#include <algorithm>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <list>

#include "../library/fuel.h"

namespace {

    void PrintHelp() {
      std::cout << "test programm args:" << '\n' <<
                   "\t --calc LIB_NAME" << '\n' <<
                   "\t --file FILE_NAME (the first line - number of variables and then spacec-separated variables, other lines - file to simplify, one per line) " << '\n' <<
                   "\t --binaries LIBRARY_BINARIES_FILE (optional) " << '\n' <<
                   "\t --option OPTION (optional) - to pass with fuel::setOption " << '\n' <<
                   "\t --prime PRIME_NUMBER (optional, for modular arithmetics) " << '\n' <<
                   "\t --help" << '\n';
    }

    const bool kError = true;
    const bool kOK = false;

    bool f_stop = false;
    unsigned long long prime = 0;
    std::mutex f_mutex;
    std::condition_variable f_submit_cond;
    std::condition_variable f_receive_cond;
    std::queue<std::pair<size_t, std::string>> f_jobs;
    unsigned int threads = 0;
    std::string file_with_coeffs_name;
    std::string file_with_binaries = {};
    std::vector<std::string> variables;
    std::vector<std::string> coeffs;
    std::vector<std::chrono::microseconds> durations;
    std::vector<std::string> options;


    void f_worker(unsigned short fnum) {

        while (true) {
            unique_lock<mutex> guard(f_mutex); // it's locked
            f_submit_cond.wait(guard,[](){return (f_stop || !f_jobs.empty());});
            // predicate is checked on locked mutex. if we are out, is is still locked, but while we are waiting, it's not
            if (f_jobs.empty()) {
                break; // time to stop, and the mutex get's unlocked on contex end
            }
            auto [i, f_submit] = f_jobs.front();
            f_jobs.pop(); // we take the data out of the queue
            guard.unlock(); // mutex is unlocked for other threads, and we start working

            const auto start = std::chrono::steady_clock::now();
            fuel::simplify(f_submit, fnum, prime!=0);
            const auto end = std::chrono::steady_clock::now();
            durations[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);
        }
        {
            unique_lock<mutex> guard(f_mutex);
            f_receive_cond.notify_one();
        }
#ifdef ENABLE_FLINT
        flint_cleanup();
#endif
    }

    bool ParseArgs(const int argc, const char* const *argv) {
        for (int i = 1; i < argc; ++i) {
            if (!std::strcmp(argv[i], "--help")) {
                PrintHelp();
                return kError;
            }
        }

        bool miss_lib_name = true;
        bool miss_file_name = true;
        for (int i = 1; i < argc - 1; ++i) {
            if (!std::strcmp(argv[i], "--calc")) {
                fuel::setLibrary(argv[i + 1]);
                miss_lib_name = false;
            } else if (!std::strcmp(argv[i], "--file")) {
                file_with_coeffs_name = argv[i + 1];
                miss_file_name = false;
            } else if (!std::strcmp(argv[i], "--binaries")) {
                file_with_binaries = argv[i + 1];
            } else if (!std::strcmp(argv[i], "--prime")) {
                sscanf(argv[i + 1], "%llu", &prime);
            } else if (!std::strcmp(argv[i], "--threads")) {
                sscanf(argv[i + 1], "%u", &threads);
            } else if (!std::strcmp(argv[i], "--option")) {
                options.push_back(std::string(argv[i + 1]));
            }
        }

        if (miss_lib_name || miss_file_name) {
            PrintHelp();
        }
        return miss_lib_name || miss_file_name;
    }

    // std::cout << file.good() << file.fail() << file.bad() << file.eof() << '\n';
    bool ReadDataFromFile() {
        std::ifstream file(file_with_coeffs_name);
        if (!file.is_open()) {
            std::cout << "bad file with coeffs" << std::endl;
            return kError;
        }

        std::size_t number_of_variables;
        file >> number_of_variables;
        variables.reserve(number_of_variables);
        std::copy_n(
                std::istream_iterator<std::string>(file),
                number_of_variables,
                std::back_inserter(variables));
        if (variables.size() != number_of_variables) {
            return kError;
        }
        std::copy(
                std::istream_iterator<std::string>(file),
                std::istream_iterator<std::string>(),
                std::back_inserter(coeffs)
                );
        durations.reserve(coeffs.size());
        for (size_t i = 0; i != coeffs.size(); ++i) durations.push_back({});
        return kOK;
    }



    void Simplify() {

        const auto startAll = std::chrono::steady_clock::now();
        if (threads == 0) {
            for (size_t i = 0; i < coeffs.size(); ++i) {

                const auto start = std::chrono::steady_clock::now();
                fuel::simplify(coeffs[i], 0, prime!=0);
                const auto end = std::chrono::steady_clock::now();
                durations[i] = std::chrono::duration_cast<std::chrono::microseconds>(end - start);

            }
        } else {
            std::list<std::thread> workers;
            for (size_t i = 0; i != threads; ++i) {
                workers.emplace_back(std::thread(f_worker, i));
            }
            for (size_t i = 0; i < coeffs.size(); ++i) {
                lock_guard<mutex> guard(f_mutex);
                f_jobs.push(std::make_pair(i, coeffs[i]));
                f_submit_cond.notify_one();
            }
            f_stop = true;
            {
                std::unique_lock<mutex> guard(f_mutex);
                f_receive_cond.wait(guard,[](){return (f_jobs.empty());});
            }
            f_submit_cond.notify_all();
            while (!workers.empty()) {
                auto worker = std::move(workers.back());
                workers.pop_back();
                worker.join();
            }
        }
        std::cout << "Evaluation time used by threads: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::accumulate(durations.cbegin(), durations.cend(), std::chrono::microseconds(0))).count() << std::endl;
        const auto endAll = std::chrono::steady_clock::now();
        std::cout << "Absolute evaluation time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::duration_cast<std::chrono::microseconds>(endAll - startAll)).count() << endl;
    }

    /*void Print() {
        std::cout << "Variables:" << '\n' <<
            "\tsize - " << variables.size() <<
            "\t";
        for (const auto& variable: variables) {
            std::cout << "|" << variable << "|" << ' ';
        }
        std::cout << '\n';

        std::cout << "Coeffs:" << '\n' <<
            "\tsize - " << coeffs.size() << '\n';
        for (const auto& coef: coeffs) {
            std::cout << "\t|" << coef << "|" << '\n';
        }
    }*/

} // namespace

int main(int argc, char* argv[]) {
    // this snipped was copied from one of FIRE files
    char current[1024];
    if (!getcwd(current, 1024)) {
        cout << "Can't get current dir name" << endl;
        return 1;
    }
    const string scurrent = string(current);
    string srun = string(argv[0]);
    srun = srun.substr(0, srun.length() - 4);

    if (ParseArgs(argc, argv) || ReadDataFromFile()) {
        PrintHelp();
        exit(1);
    }

    fuel::savePids = true;

    if (file_with_binaries != "") {
        fuel::readLibraryPathsFromFile(file_with_binaries);
    }

    if (!fuel::initialize(variables, threads + !threads, false, prime)) {
        std::cout << "Can't initialize choosen library" << '\n';
        exit(1);
    };

    for (auto option : options) {
        fuel::setOption(option);
    }

    // t.Print();

    Simplify();

    fuel::close();

    return 0;
}
