# $1 - path to file with coeffs

for lib_name in fermat pari ginac math maple symbolica; do
  ./test --calc $lib_name --file $1 --binaries $2 && echo
  echo "- - - - - "
done
