#### Coeffs
Format:

- number of variables
- space
- all variables separeted by space
- coeffs without spaces and blank lines

Example:

```Text
4 a b c d
1+2
(2*3*a)/(4*9*b+2)
```

#### Time measurement

`./test --calc SIMPLIFIER_NAME --file PATH_TO_COEFFS`, where SIMPLIFIER_NAME is one of
`cocoa`, `fermat`, `form`, `ginac`, `macaulay`, `maple`, `maxima`, `nemo`, `pari`, `math`, `reduce`

#### Memory consumption

- Start testing like in previous paragraph
- While testing is taking place: `python ps_mem.py -t -p $(cat pid) -w HOW_OFTEN_MEASURE > FILE_TO_STORE_DATA`, HOW_OFTEN_MEASURE is set in seconds.
- Details about obtained values are in `ps_mem.py`
