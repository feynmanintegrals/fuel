ifneq ("$(wildcard options)","")
    include options
endif

CACHED_DEPS = flint symbolica pari gmp mpfr
DEP_ROOT = `pwd`/usr
dep_has = $(and $(findstring $(1), $(CACHED_DEPS)), $(shell test -e $(DEP_ROOT)/lib/$(2) && echo -n yes))

DEP_HAS_FLINT         = $(call dep_has,flint,libflint.so)
DEP_HAS_PARI          = $(call dep_has,symbolica,libpari.so)
DEP_HAS_SYMBOLICA     = $(call dep_has,pari,libsymbolica.a)
DEP_HAS_GMP           = $(or $(call dep_has,gmp,libgmp.a), $(call dep_has,gmp,libgmp.so))
DEP_HAS_MPFR          = $(or $(call dep_has,mpfr,libmpfr.a), $(call dep_has,mpfr,libmpfr.so))

default: library

.PHONY: library benchmarks symbolica clean dep cleandep flintLibrary pariLibrary symbolicaLibrary gmpLibrary mpfrLibrary

all: library benchmarks symbolica

UNAME_S := $(shell uname -s)

ifdef MPFR_REQUIRED
dep: | mpfrLibrary flintLibrary pariLibrary symbolicaLibrary
else
dep: | flintLibrary pariLibrary symbolicaLibrary
endif

.PHONY: doc documentation doc_pdf showdoc cleandoc
doc: documentation

documentation:
	doxygen documentation/Doxyfile

showdoc:
	xdg-open documentation/html/index.html

doc_pdf:
	if [ ! -f documentation/latex/Makefile ]; then $(MAKE) doc; fi;
	$(MAKE) -C ./documentation/latex/
	mv ./documentation/latex/refman.pdf ./documentation/

cleandoc:
	rm -rf ./documentation/latex/
	rm -rf ./documentation/html/
	rm -f ./documentation/refman.pdf

cleandep:
	rm -rf usr/lib/*
	rm -rf usr/include/*
	rm -rf usr/bin/*
	rm -rf usr/share/*
	cd extra/flint/ && make clean

.PHONY: submodule
submodule:
	git submodule update --init --recursive

ifdef ENABLE_FLINT
ifndef PREBUILT_FLINT
flintLibrary: submodule gmpLibrary mpfrLibrary
ifneq ($(DEP_HAS_FLINT),yes)
	cd extra/flint && ./bootstrap.sh
	cd extra/flint && automake --force-missing --add-missing || echo "This error is not important"
	cd extra/flint && rm -rf autom4te.cache/
	export CXX=$(CCPLUS) && export CC=$(CC) && cd extra/flint && ./configure $(FLINT_OPTIONS) --with-mpfr=`pwd`/../../usr/ --with-gmp-include=`pwd`/../../usr/include --with-gmp-lib=`pwd`/../../usr/include --prefix=`pwd`/../../usr
	$(MAKE) -C extra/flint
	$(MAKE) -C extra/flint install
endif
else
flintLibrary:
ifeq ($(UNAME_S),Darwin)
	cd usr; \
	tar --warning=no-timestamp -xf FLINT.v200.900.9.x86_64-apple-darwin.tar.gz; \
	cd ..
else
	cd usr; \
	tar --warning=no-timestamp -xf FLINT.v200.900.9.x86_64-linux-gnu.tar.gz; \
	cd ..
endif
endif
else
flintLibrary:
endif

ifdef INSTALL_PARI
pariLibrary: submodule
ifneq ($(DEP_HAS_PARI),yes)
	export CXX=$(CCPLUS) && export CC=$(CC) && cd extra/pari && ./Configure --prefix=`pwd`/../../usr
	$(MAKE) -C ./extra/pari gp
	$(MAKE) -C ./extra/pari install
endif
else
pariLibrary:
endif

ifdef INSTALL_SYMBOLICA
symbolicaLibrary: submodule
ifneq ($(DEP_HAS_SYMBOLICA),yes)
	cd extra/symbolica && cargo rustc --release --features faster_alloc --crate-type=staticlib
	cp extra/symbolica/target/release/libsymbolica.a usr/lib/
endif
else
symbolicaLibrary:
endif

library:
	export CXX=$(CCPLUS) && $(MAKE) -C ./library

benchmarks:
	$(MAKE) -C benchmarks

symbolica:
	$(MAKE) -C symbolica

clean_libs_benchmarks:
	cd benchmarks && make clean && cd .. && cd library && make clean && cd ..

clean:
	make -C ./library clean

ifndef PREBUILT_GMP
gmpLibrary:
ifneq ($(DEP_HAS_GMP),yes)
	cd temp && \
	rm -rf gmp-6.2.* && \
	curl https://ftp.gnu.org/gnu/gmp/gmp-6.2.1.tar.xz -O && \
	tar -xvf ./gmp-6.2.1.tar.xz && \
	curl https://raw.githubusercontent.com/Homebrew/formula-patches/03cf8088210822aa2c1ab544ed58ea04c897d9c4/libtool/configure-big_sur.diff -O && \
	patch -d ./gmp-6.2.1 < configure-big_sur.diff && \
	cd gmp-6.2.1 && \
	export CXX=$(CCPLUS) && export CC=$(CC) && ./configure --enable-fat --enable-cxx --with-pic --prefix=`pwd`/../../usr/ && \
	$(MAKE) && \
	$(MAKE) install
endif
else
gmpLibrary:
ifneq ($(DEP_HAS_GMP),yes)
ifeq ($(UNAME_S),Darwin)
	cd usr; \
	tar --warning=no-timestamp -xf GMP.v6.2.1.x86_64-apple-darwin.tar.gz; \
	cd ..
else
	cd usr; \
	tar --warning=no-timestamp -xf GMP.v6.2.1.x86_64-linux-gnu-cxx11.tar.gz; \
	cd ..
endif
endif
endif

ifndef PREBUILT_MPFR
mpfrLibrary: gmpLibrary
ifneq ($(DEP_HAS_MPFR),yes)
	cd temp && \
	rm -rf mpfr-4* && \
	wget https://www.mpfr.org/mpfr-4.2.1/mpfr-4.2.1.tar.gz && \
	tar -zxf mpfr-4.2.1.tar.gz && \
	cd mpfr-4.2.1 && \
	export CXX=$(CCPLUS) && export CC=$(CC) && ./configure --prefix=`pwd`/../../usr/ --with-gmp=`pwd`/../../usr/ && \
	$(MAKE) && \
	$(MAKE) install
endif
else
mpfrLibrary: gmpLibrary
ifneq ($(DEP_HAS_MPFR),yes)
ifeq ($(UNAME_S),Darwin)
	cd usr; \
	tar --warning=no-timestamp -xf MPFR.v4.2.1.x86_64-apple-darwin.tar.gz; \
	cd ..
else
	cd usr; \
	tar --warning=no-timestamp -xf MPFR.v4.2.1.x86_64-linux-gnu.tar.gz; \
	cd ..
endif
endif
endif
