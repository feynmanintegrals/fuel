#include "flint.h"

namespace fuel
{
    void flint::printMonomes(fmpz_mpoly_struct *poly, fmpz_mpoly_ctx_t ctx) {
        size_t varsCount = ctx->minfo->nvars;
        if (fmpz_mpoly_is_zero(poly, ctx)) {
            this->addToOut('0');
            return;
        }

        ulong *exp = new ulong[varsCount];

        slong polyLength = fmpz_mpoly_length(poly, ctx);
        for (slong i = 0; i < polyLength; ++i) {
            this->addToOut('<');

            fmpz *coeff = fmpz_mpoly_term_coeff_ref(poly, i, ctx);

            char *coeffStr = nullptr;
            coeffStr = fmpz_get_str(coeffStr, 10, coeff);
            char *tmp = coeffStr;

            while (tmp && *tmp) {
                this->addToOut(*tmp);
                ++tmp;
            }
            free(coeffStr);

            fmpz_mpoly_get_term_exp_ui(exp, poly, i, ctx);

            for (size_t j = 0; j < varsCount; ++j) {
                this->addToOut('.');
                for (char c : std::to_string(exp[j])) {
                    this->addToOut(c);
                }
            }

            this->addToOut('>');
        }

        delete[] exp;
    }

    void flint::switchToConventional()
    {
        conventional_output = true;
    }

    void flint::initializeInternal(const vector<string> &vars1)
    {
        vector<string> vars = vars1;
        if (vars.empty()) {
            vars.push_back("d");
        }
        parser = generic_parser::parser<ratfunc_flint::rational_function>(vars);
        supportsBrackets = true;
        unsigned long long prime;
        sscanf(this->primeBuf, "%llu", &prime);
        if (prime != 0) {
            parser_mod = generic_parser::parser<mod_ratfunc_flint::rational_function>(vars, prime);
        }
    }

    void flint::evaluateInternal(const char *expr, bool modular)
    {
        if (modular) {
            evaluateInternalModular(expr);
            return;
        }

        auto result = parser.eval_string(expr);

        if (decomposed_output && !conventional_output) {
            // Print <<<num_monome1>..<num_monomeN>>.<<den_monome1>..<den_monomeM>>>, where monome=<coeff.exp_1.exp_2. ... .exp_i>
            auto& ctx = ratfunc_flint::rational_function::ctx_wrapper.ctx;

            fmpz_mpoly_struct *numerator = result.data->GetNumerator();

            if (fmpz_mpoly_is_zero(numerator, ctx)) {
                this->addToOut('0');
                return;
            }

            this->addToOut('<');
            this->addToOut('<');

            printMonomes(numerator, ctx);

            this->addToOut('>');
            this->addToOut('.');
            this->addToOut('<');

            fmpz_mpoly_struct *denominator = result.data->GetDenominator();

            printMonomes(denominator, ctx);

            this->addToOut('>');
            this->addToOut('>');

            return;
        }

        bool returning_number;

        if (conventional_output || !store_expressions) {
            returning_number = false;
        } else {
            if (fmpz_mpoly_is_zero((result.data)->GetNumerator(), ratfunc_flint::rational_function::ctx_wrapper.ctx)) {
                returning_number = false;
            } else if (fmpz_mpoly_is_fmpz((result.data)->GetNumerator(), ratfunc_flint::rational_function::ctx_wrapper.ctx) &&
                fmpz_mpoly_is_fmpz((result.data)->GetDenominator(), ratfunc_flint::rational_function::ctx_wrapper.ctx)) {
                returning_number = false;
            } else {
                returning_number = true;
            }
        }

        if (returning_number) {
            uint64_t number = ++parser.n_stored;
            parser.stored_expressions[number] = result;
            char buf[32];
            snprintf(buf, 32, "%llu", static_cast<unsigned long long>(number));
            this->addToOut('{');
            for (const char* c = buf; *c != '\0'; ++c) {
                this->addToOut(*c);
            }
            this->addToOut('}');
            return;
        }

        if (fmpz_mpoly_is_zero((result.data)->GetNumerator(), ratfunc_flint::rational_function::ctx_wrapper.ctx))
        {
            this->addToOut('0');
            return;
        }

        std::pair<char *, char *> num_den_strings = result.get_num_den_strings();
        char *numstring = num_den_strings.first;
        char *denstring = num_den_strings.second;

        if (fmpz_mpoly_is_one((result.data)->GetDenominator(), ratfunc_flint::rational_function::ctx_wrapper.ctx))
        {
            if (conventional_output || (fmpz_mpoly_length(result.data->GetNumerator(), ratfunc_flint::rational_function::ctx_wrapper.ctx) <= 2))
            {
                for (char *c = numstring; *c != '\0'; ++c)
                    this->addToOut(*c);
            }
            else
            {
                this->addToOut('[');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(']');
            }
        }
        else
        {
            if (conventional_output)
            {
                // Print (numstring)/(denstring)
                this->addToOut('(');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(')');
                this->addToOut('/');
                this->addToOut('(');
                for (char *c = denstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(')');
            }
            else
            {
                // Print [numstring,denstring]
                this->addToOut('[');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(',');
                for (char *c = denstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(']');
            }
        }

        free(numstring);
        free(denstring);
    }

    void flint::evaluateInternalModular(const char *expr)
    {
        auto result = parser_mod.eval_string(expr);

        bool returning_number;

        if (conventional_output || !store_expressions) {
            returning_number = false;
        } else {
            if (nmod_mpoly_is_zero(&(result.numerator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx)) {
                returning_number = false;
            } else if (nmod_mpoly_is_ui(&(result.numerator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx) &&
                nmod_mpoly_is_ui(&(result.denominator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx)) {
                returning_number = false;
            } else {
                returning_number = true;
            }
        }

        if (returning_number) {
            uint64_t number = ++parser.n_stored;
            parser_mod.stored_expressions[number] = result;
            char buf[32];
            snprintf(buf, 32, "%llu", static_cast<unsigned long long>(number));
            this->addToOut('{');
            for (const char* c = buf; *c != '\0'; ++c) {
                this->addToOut(*c);
            }
            this->addToOut('}');
            return;
        }

        if (nmod_mpoly_is_zero(&(result.numerator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx))
        {
            this->addToOut('0');
            return;
        }

        std::pair<char *, char *> num_den_strings = result.get_num_den_strings();
        char *numstring = num_den_strings.first;
        char *denstring = num_den_strings.second;

        if (nmod_mpoly_is_one(&(result.denominator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx))
        {
            if (conventional_output || (nmod_mpoly_length(&(result.numerator), mod_ratfunc_flint::rational_function::ctx_wrapper.ctx) <= 2))
            {
                for (char *c = numstring; *c != '\0'; ++c)
                    this->addToOut(*c);
            }
            else
            {
                this->addToOut('[');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(']');
            }
        }
        else
        {
            if (conventional_output)
            {
                // Print (numstring)/(denstring)
                this->addToOut('(');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(')');
                this->addToOut('/');
                this->addToOut('(');
                for (char *c = denstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(')');
            }
            else
            {
                // Print [numstring,denstring]
                this->addToOut('[');
                for (char *c = numstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(',');
                for (char *c = denstring; *c != '\0'; ++c)
                {
                    this->addToOut(*c);
                }
                this->addToOut(']');
            }
        }

        free(numstring);
        free(denstring);
    }

    void flint::setOption(const string& option) {
        if (option == "store_expressions") {
            store_expressions = true;
        } else if (option == "no_store_expressions") {
            store_expressions = false;
        } else if (option == "clear_stored_expressions") {
            parser.clear_stored_expressions();
            if (this->primeBuf[0] != '0') {
                parser_mod.clear_stored_expressions();
            }
        } else if (option == "factorize_denominators") {
            factorize_denominators = true;
        } else if (option == "decomposed_output") {
            decomposed_output = true;
        }
    }

    flint::~flint() {
        if (!called_cleanup) {
            parser.clear_stored_expressions();
            if (this->primeBuf[0] != '0') {
                parser_mod.clear_stored_expressions();
            }
            flint_cleanup_master();
            called_cleanup = true;
        }
    }
}
