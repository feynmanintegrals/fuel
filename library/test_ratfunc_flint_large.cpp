#include "ratfunc_flint.h"
#include <string>
#include <cassert>
#include <iostream>

namespace ratfunc_flint
{
    /**
     * Test rational function arithmetic of our FLINT wrapper
     * @return 0 if the test is successful
    */
    int test_ratfunc_flint()
    {
        std::vector<std::string> vars1{"a", "b", "c", "d", "f", "g"};
        initialize_vars(vars1);

        rational_function x("(a+b+c+d+f+g)^14+3", "(2*a+b+c+d+f+g)^14+4", true);
        rational_function y("(3*a+b+c+d+f+g)^14+5", "(4*a+b+c+d+f+g)^14+6", true);

	// rational_function z = x - y;
	x -= y;
	
        return 0;
    }

    /**
     * Test parsing math expression strings and performing rational function arithmetic
     * @return 0 if the test is successful
    */
    int test_simplify_string()
    {
        std::vector<std::string> vars{"a", "b", "c", "d", "f", "g"};
        auto p = parser_with_vars(vars);
	auto result = p.eval_string("[(a+b+c+d+f+g)^15+3]*[(4*a+b+c+d+f+g)^15+6]");
	return 0;
    }

    /**
     * Run all tests: `test_ratfunc_flint()`, `test_evalmath_flint()`, `test_simplify_string()`.
     * @return 0 if the test is successful
    */
    int test_all()
    {
        // assert(test_ratfunc_flint() == 0);
	assert(test_simplify_string() == 0);
        flint_cleanup_master();
        return 0;
    }

}

int main()
{
    return ratfunc_flint::test_all();
}

