#include "pari.h"

namespace fuel {

    void pari::initializeInternal(const vector<string>& vars) {
        this->writeExpression("default(prompt, \"\")\n");
        this->writeExpression("default(simplify,1)\n");
        this->writeExpression("default(histsize,1)\n");
        this->writeExpression("2+2\n", true);
        this->readUntil("parisize");
        this->readUntil("%4 = 4");
        this->writeExpression("default(parisizemax,\"256M\");\n");
        this->writeExpression("2+2\n", true);
        this->readUntil("%5 = 4");
    }

    void pari::evaluateInternal(const char* expr, bool modular) {
        this->writeExpression(expr);
        if (modular) {
            this->writeExpression("%");
            this->writeExpression(this->primeBuf);
        }
        this->writeExpression("\n", true);

        char* c = this->readLine();
        while (*c != '=') ++c;
        c += 2;

        while (*c != '\n') {
            if (*c > ' ') addToOut(*c);
            c++;
            if (*c == '\0') c = this->readLine();
        }
    }

    pari::~pari() {
        if (this->childpid) {
            this->writeExpression("quit\n", true);
        }
    }
}
