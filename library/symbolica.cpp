#include "symbolica.h"

#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <chrono>

namespace fuel {

    typedef struct SYMBOLICA* Symbolica; //!< stuct for symbolica instance

    void symbolica::initializeInternal(const vector<string>& vars) {
        supportsBrackets = true;
        const char* homeDir = getenv("HOME");
        std::ifstream temp_license_file(std::string(homeDir) + "/.symbolica/temp_license");
        bool temp_is_ok = false;
        if (!is_licensed()) {
            if (!temp_license_file.fail()) {
                // we try a license from a temp file if it is not too old
                std::string line;
                if (std::getline(temp_license_file, line)) {
                    unsigned long long previous_time = stoll(line);
                    unsigned long long current_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
                    if (current_time - previous_time < 23*60*60ll) {
                        if (std::getline(temp_license_file, line)) {
                            if (set_license_key(line.c_str())) {
                                temp_is_ok = true;
                            }
                        }
                    }
                }
                temp_license_file.close();
                if (!temp_is_ok) {
                    std::string fname = std::string(homeDir) + "/.symbolica/temp_license";
                    unlink(fname.c_str());
                }
            }
            if (!temp_is_ok) {
                std::ifstream license_file(std::string(homeDir) + "/.symbolica/main_license");
                if (!license_file.fail()) {
                    // we try a license from a main file
                    std::stringstream buffer;
                    buffer << license_file.rdbuf();
                    std::string license = buffer.str();
                    for (auto &c : license) {
                        if (c=='\n') c = '\0';
                    }
                    if (set_license_key(license.c_str())) {
                        // it's fine, getting a temp one and storing it
                        char temp_buf[128];
                        if (get_offline_license_key(temp_buf)) {
                            std::ofstream overwrite_temp_license_file(std::string(homeDir) + "/.symbolica/temp_license");
                            overwrite_temp_license_file << std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count() << std::endl;
                            overwrite_temp_license_file << temp_buf << std::endl;
                            overwrite_temp_license_file.close();
                        }
                    }
                    license_file.close();
                }
            }
        }
        if (!is_licensed()) {
            std::cout << "Symbolica is not licensed" << std::endl;
            std::cout << "Run make symbolica && symbolica/register to request a trial license" << std::endl;
            std::cout << "Refer to https://symbolica.io/docs/get_started.html#license for more details" << std::endl;
            std::cout << "Set a license with creaing a file ~/.symbolica/main_license and putting the license inside (no extra symbols and new line, only the code)" << std::endl;
            abort();
        }
        s = init();
        string varsAll;
        if (!vars.empty()) {
            for (auto& var : vars) {
                varsAll += var;
                varsAll += ",";
            }
            varsAll.pop_back();
        }
        set_vars(s, varsAll.c_str());
        set_options(s, input_has_rational_numbers, exp_fits_in_u8);
    }

    void symbolica::evaluateInternal(const char* expr, bool modular) {
        const char* c;
        if (modular) {
            unsigned long long prime;
            sscanf(primeBuf, "%llu", &prime);
            if (factorize_denominators) {
                c = simplify_factorized(s, expr, prime, explicit_rat);
            } else {
                c = simplify(s, expr, prime, explicit_rat);
            }
        } else {
            if (factorize_denominators) {
                c = simplify_factorized(s, expr, 0, explicit_rat);
            } else {
                c = simplify(s, expr, 0, explicit_rat);
            }
        }
        while (*c != '\0') {
            this->addToOut(*c);
            c++;
        }
    }

    void symbolica::switchToConventional() {
        explicit_rat = 0;
    }

    void symbolica::setOption(const string& option) {
        if (option == "rational_input") {
            input_has_rational_numbers = true;
        } else if (option == "integer_input") {
            input_has_rational_numbers = false;
        } else if (option == "u16exp") {
            exp_fits_in_u8 = false;
        } else if (option == "u8exp") {
            exp_fits_in_u8 = true;
        } else if (option == "factorize_denominators") {
            factorize_denominators = true;
        }
        set_options(s, input_has_rational_numbers, exp_fits_in_u8);
    }

    void symbolica::requestTrialLicense(const char* name, const char* email, const char* company) {
        request_trial_license(name, email, company);
    }

    symbolica::~symbolica() {
        if (s) {
            drop(s);
        }
    }
}
