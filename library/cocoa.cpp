#include "cocoa.h"

namespace fuel {

    void cocoa::initializeInternal(const vector<string>& vars) {
#ifdef COCOA_AS_A_LIBRARY
        std::vector<CoCoA::symbol> vars_array;
        if (vars.size() == 0) {
            vars_array = CoCoA::NewSymbols(1);
        } else {
            for (const auto& var : vars) {
                vars_array.push_back(CoCoA::symbol(var));
            }
        }
//        auto vars_array = (vars.size() == 0) ?  CoCoA::NewSymbols(1) : vars;

        CoCoA::ring ZZ = CoCoA::RingZZ();
        CoCoA::FractionField ZZFF = CoCoA::NewFractionField(ZZ);
        CoCoA::PolyRing poly = CoCoA::NewPolyRing(ZZFF, vars_array);
        polyFF_ZZ = CoCoA::NewFractionField(poly);

        // in case prime is not set we just define those things for compilation reasons but do not use
        CoCoA::ring ZZ_mod = strcmp(primeBuf, "0") ? CoCoA::ring(CoCoA::NewPolyRing(CoCoA::NewZZmod(CoCoA::BigIntFromString(primeBuf)), CoCoA::NewSymbols(1))) : CoCoA::RingZZ();
        CoCoA::FractionField ZZFF_mod = CoCoA::FractionField(CoCoA::NewFractionField(ZZ_mod));
        CoCoA::PolyRing poly_mod = CoCoA::PolyRing(CoCoA::NewPolyRing(ZZFF_mod, vars_array));
        polyFF_ZZ_mod = CoCoA::FractionField(CoCoA::NewFractionField(poly_mod));
#else
        this->readUntil("Enter");
        for (size_t i = 0; i != vars.size(); ++i) {
            this->writeExpression(vars[i].c_str());
            if (i != vars.size() - 1) {
                this->writeExpression(",");
            }
        }
        this->writeExpression("\n", true); 
        this->writeExpression(this->primeBuf); 
        this->writeExpression("\n", true); 
        this->writeExpression("2+2\n", true); 
        this->readUntil("4");
#endif
    }

    void cocoa::evaluateInternal(const char* expr, bool modular) {
#ifdef COCOA_AS_A_LIBRARY
        stringstream ss;
        
        if (modular) {
            std::string coef(expr);
            CoCoA::RingElem elem(polyFF_ZZ_mod, coef);
            ss << elem;
        } else {
            std::string coef(expr);
            CoCoA::RingElem elem(polyFF_ZZ, coef);
            ss << elem;
        }
        string rc_res = ss.str();
        for (auto c : rc_res) {
            if (c > ' ') addToOut(c);
        }
#else
        if (modular) {
            this->writeExpression("%");
        }
        this->writeExpression(expr);
        this->writeExpression("\n", true);
        
        const char* c = this->readLine();

        while (*c != '\n') {
            if (*c > ' ') this->addToOut(*c);
            c++;
            if (*c == '\0') c = this->readLine(); 
        }
#endif
    }

    cocoa::~cocoa() {
#ifdef COCOA_AS_A_LIBRARY
#else
        if (this->childpid) {
            this->writeExpression("&q\n", true);
        }
#endif
    }
}
