
/** @file cocoa.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

#ifdef COCOA_AS_A_LIBRARY
#include <CoCoA/library.H>
#endif

namespace fuel {

    /**
     * @brief Communication with cocoa: https://cocoa.dima.unige.it/cocoa/cocoalib/ 
     */
    class cocoa : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;
#ifdef COCOA_AS_A_LIBRARY
            // CoCoA provides an opportunity to use GMP rather than a system allocator (faster but NOT THREADSAFE)
            CoCoA::GlobalManager CoCoAFoundations{CoCoA::UseNonNegResidues + CoCoA::UseGMPAllocator};
            //CoCoA::GlobalManager CoCoAFoundations{CoCoA::UseNonNegResidues};
            CoCoA::ring polyFF_ZZ = CoCoA::RingZZ();
            CoCoA::ring polyFF_ZZ_mod = CoCoA::RingZZ();
#endif

        public:
            cocoa() {}

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<cocoa>();
            }

            ~cocoa();
    };

}
