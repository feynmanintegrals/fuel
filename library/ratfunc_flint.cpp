#include "ratfunc_flint.h"
#include <cassert>
#include <queue>
#include <string>
#include <mutex>

namespace fuel::ratfunc_flint
{
    void TMpolyFractionHolder::AddAnotherAndStoreHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_mpoly_is_zero(x_num, ctx)) {
            fmpz_mpoly_set(res_num, y_num, ctx);
            fmpz_mpoly_set(res_den, y_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_zero(y_num, ctx)) {
            fmpz_mpoly_set(res_num, x_num, ctx);
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_equal(x_den, y_den, ctx)) {
            fmpz_mpoly_add(res_num, x_num, y_num, ctx);

            if (fmpz_mpoly_is_one(x_den, ctx) || fmpz_mpoly_is_zero(res_num, ctx)) {
                fmpz_mpoly_one(res_den, ctx);
            } else if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
                fmpz_t t;
                fmpz_init(t);

                FMPZVectorHelper(t, res_num->coeffs, res_num->length, x_den->coeffs);

                if (fmpz_is_one(t)) {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, t, ctx);
                    fmpz_mpoly_scalar_divexact_fmpz(res_den, x_den, t, ctx);
                }

                fmpz_clear(t);
            } else {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);

                AssertFMPZMpolyGcdSuccessful(t, res_num, x_den, ctx);

                if (fmpz_mpoly_is_one(t, ctx)) {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                } else {
                    DivExactByMpoly(res_num, res_num, t, ctx);
                    DivExactByMpoly(res_den, x_den, t, ctx);
                }

                fmpz_mpoly_clear(t, ctx);
            }
            return;
        }

        if (fmpz_mpoly_is_one(x_den, ctx)) {
            if (res_num == y_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_mul(t, x_num, y_den, ctx);
                fmpz_mpoly_add(res_num, t, y_num, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_mul(res_num, x_num, y_den, ctx);
                fmpz_mpoly_add(res_num, res_num, y_num, ctx);
            }
            fmpz_mpoly_set(res_den, y_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_one(y_den, ctx)) {
            if (res_num == x_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_mul(t, y_num, x_den, ctx);
                fmpz_mpoly_add(res_num, x_num, t, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_mul(res_num, y_num, x_den, ctx);
                fmpz_mpoly_add(res_num, x_num, res_num, ctx);
            }
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_fmpz(y_den, ctx)) {
            AddFMPZDenominatorHelper(res_num, res_den, x_num, x_den, y_num, y_den->coeffs, ctx);
            return;
        }

        if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
            AddFMPZDenominatorHelper(res_num, res_den, y_num, y_den, x_num, x_den->coeffs, ctx);
            return;
        }

        {
            fmpz_mpoly_t g;
            fmpz_mpoly_init(g, ctx);

            AssertFMPZMpolyGcdSuccessful(g, x_den, y_den, ctx);

            if (fmpz_mpoly_is_one(g, ctx)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_add does not work inplace */
                fmpz_mpoly_mul(t, x_num, y_den, ctx);
                fmpz_mpoly_mul(u, y_num, x_den, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);
                fmpz_mpoly_mul(res_den, x_den, y_den, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_mpoly_t a, b, t, u;

                fmpz_mpoly_init(a, ctx);
                fmpz_mpoly_init(b, ctx);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                DivExactByMpoly(a, x_den, g, ctx);
                DivExactByMpoly(b, y_den, g, ctx);

                fmpz_mpoly_mul(t, x_num, b, ctx);
                fmpz_mpoly_mul(u, y_num, a, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);

                AssertFMPZMpolyGcdSuccessful(t, res_num, g, ctx);

                if (fmpz_mpoly_is_one(t, ctx)) {
                    fmpz_mpoly_mul(res_den, x_den, b, ctx);
                } else {
                    DivExactByMpoly(res_num, res_num, t, ctx);
                    DivExactByMpoly(g, x_den, t, ctx);
                    fmpz_mpoly_mul(res_den, g, b, ctx);
                }

                fmpz_mpoly_clear(a, ctx);
                fmpz_mpoly_clear(b, ctx);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_mpoly_clear(g, ctx);
        }
    }

    void TMpolyFractionHolder::AddFMPZDenominatorHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        fmpz_t g;
        fmpz_init(g);

        if (fmpz_mpoly_is_fmpz(y_num, ctx)) {
            if (res_num == x_num || res_num == y_num) {
                fmpz_t t, u;
                fmpz_init_set(t, y_num->coeffs);
                fmpz_init_set(u, y_den);
                AddFMPQHelper(res_num, res_den, x_num, x_den, t, u, ctx);
                fmpz_clear(t);
                fmpz_clear(u);
            } else {
                AddFMPQHelper(res_num, res_den, x_num, x_den, y_num->coeffs, y_den, ctx);
            }
            return;
        }

        if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
            fmpz_gcd(g, x_den->coeffs, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_add does not work inplace */
                fmpz_mpoly_scalar_mul_fmpz(t, y_num, x_den->coeffs, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, y_den, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);
                fmpz_mul(g, x_den->coeffs, y_den);
                fmpz_mpoly_set_fmpz(res_den, g, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_t a, b;
                fmpz_mpoly_t t, u;

                fmpz_init(a);
                fmpz_init(b);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_divexact(a, y_den, g);
                fmpz_divexact(b, x_den->coeffs, g);

                fmpz_mpoly_scalar_mul_fmpz(t, y_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, a, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);

                if (fmpz_mpoly_is_zero(res_num, ctx)) {
                    fmpz_one(a);
                } else {
                    FMPZVectorHelper(a, res_num->coeffs, res_num->length, g);
                }

                if (fmpz_is_one(a)) {
                    fmpz_mul(g, b, y_den);
                    fmpz_mpoly_set_fmpz(res_den, g, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, a, ctx);
                    fmpz_divexact(g, y_den, a);
                    fmpz_mul(g, g, b);
                    fmpz_mpoly_set_fmpz(res_den, g, ctx);
                }

                fmpz_clear(a);
                fmpz_clear(b);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }
        } else {
            FMPZVectorHelper(g, x_den->coeffs, x_den->length, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_add does not work inplace */
                fmpz_mpoly_mul(t, y_num, x_den, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, y_den, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);
                fmpz_set(g, y_den);
                fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, g, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_t a;
                fmpz_mpoly_t b, t, u;

                fmpz_init(a);
                fmpz_mpoly_init(b, ctx);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_divexact(a, y_den, g);
                fmpz_mpoly_scalar_divexact_fmpz(b, x_den, g, ctx);

                fmpz_mpoly_mul(t, y_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, a, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);

                if (fmpz_mpoly_is_zero(res_num, ctx)) {
                    fmpz_one(a);
                } else {
                    FMPZVectorHelper(a, res_num->coeffs, res_num->length, g);
                }

                if (fmpz_is_one(a)) {
                    fmpz_set(g, y_den);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, b, g, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, a, ctx);
                    fmpz_divexact(g, y_den, a);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, b, g, ctx);
                }

                fmpz_clear(a);
                fmpz_mpoly_clear(b, ctx);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }
        }

        fmpz_clear(g);
    }

    void TMpolyFractionHolder::AddFMPQHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_t y_num, const fmpz_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_mpoly_is_zero(x_num, ctx)) {
            fmpz_mpoly_set_fmpz(res_num, y_num, ctx);
            fmpz_mpoly_set_fmpz(res_den, y_den, ctx);
            return;
        }

        if (fmpz_is_zero(y_num)) {
            fmpz_mpoly_set(res_num, x_num, ctx);
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_equal_fmpz(x_den, y_den, ctx)) {
            fmpz_mpoly_add_fmpz(res_num, x_num, y_num, ctx);

            if (fmpz_is_one(y_den)) {
                fmpz_mpoly_one(res_den, ctx);
            } else {
                fmpz_t t;
                fmpz_init(t);

                FMPZVectorHelper(t, res_num->coeffs, res_num->length, y_den);

                if (fmpz_is_one(t)) {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, t, ctx);
                    fmpz_divexact(t, y_den, t);
                    fmpz_mpoly_set_fmpz(res_den, t, ctx);
                }

                fmpz_clear(t);
            }

            return;
        }

        if (fmpz_mpoly_is_one(x_den, ctx)) {
            fmpz_mpoly_scalar_mul_fmpz(res_num, x_num, y_den, ctx);
            fmpz_mpoly_add_fmpz(res_num, res_num, y_num, ctx);
            fmpz_mpoly_set_fmpz(res_den, y_den, ctx);
            return;
        }

        if (fmpz_is_one(y_den)) {
            if (res_num == x_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_scalar_mul_fmpz(t, x_den, y_num, ctx);
                fmpz_mpoly_add(res_num, x_num, t, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_scalar_mul_fmpz(res_num, x_den, y_num, ctx);
                fmpz_mpoly_add(res_num, x_num, res_num, ctx);
            }
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        {
            fmpz_t g;
            fmpz_init(g);

            FMPZVectorHelper(g, x_den->coeffs, x_den->length, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_mpoly_scalar_mul_fmpz(t, x_num, y_den, ctx);  /* todo: avoid one alloc? */
                fmpz_mpoly_scalar_mul_fmpz(u, x_den, y_num, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);
                fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, y_den, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_mpoly_t t, u;
                fmpz_t b, c;

                fmpz_init(b);
                fmpz_init(c);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_mpoly_scalar_divexact_fmpz(u, x_den, g, ctx);
                fmpz_divexact(b, y_den, g);

                fmpz_mpoly_scalar_mul_fmpz(t, x_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, u, y_num, ctx);
                fmpz_mpoly_add(res_num, t, u, ctx);

                FMPZVectorHelper(c, res_num->coeffs, res_num->length, g);

                if (fmpz_is_one(c)) {
                    fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, b, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, c, ctx);
                    fmpz_mpoly_scalar_divexact_fmpz(res_den, x_den, c, ctx);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, res_den, b, ctx);
                }

                fmpz_clear(b);
                fmpz_clear(c);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_clear(g);
        }
    }

    void TMpolyFractionHolder::SubAnotherAndStoreHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_mpoly_is_zero(x_num, ctx)) {
            fmpz_mpoly_neg(res_num, y_num, ctx);
            fmpz_mpoly_set(res_den, y_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_zero(y_num, ctx)) {
            fmpz_mpoly_set(res_num, x_num, ctx);
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_equal(x_den, y_den, ctx)) {
            fmpz_mpoly_sub(res_num, x_num, y_num, ctx);

            if (fmpz_mpoly_is_one(x_den, ctx) || fmpz_mpoly_is_zero(res_num, ctx)) {
                fmpz_mpoly_one(res_den, ctx);
            } else if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
                fmpz_t t;
                fmpz_init(t);

                FMPZVectorHelper(t, res_num->coeffs, res_num->length, x_den->coeffs);

                if (fmpz_is_one(t)) {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, t, ctx);
                    fmpz_mpoly_scalar_divexact_fmpz(res_den, x_den, t, ctx);
                }

                fmpz_clear(t);
            } else {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);

                AssertFMPZMpolyGcdSuccessful(t, res_num, x_den, ctx);

                if (fmpz_mpoly_is_one(t, ctx)) {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                } else {
                    DivExactByMpoly(res_num, res_num, t, ctx);
                    DivExactByMpoly(res_den, x_den, t, ctx);
                }

                fmpz_mpoly_clear(t, ctx);
            }

            return;
        }

        if (fmpz_mpoly_is_one(x_den, ctx)) {
            if (res_num == y_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_mul(t, x_num, y_den, ctx);
                fmpz_mpoly_sub(res_num, t, y_num, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_mul(res_num, x_num, y_den, ctx);
                fmpz_mpoly_sub(res_num, res_num, y_num, ctx);
            }
            fmpz_mpoly_set(res_den, y_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_one(y_den, ctx)) {
            if (res_num == x_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_mul(t, y_num, x_den, ctx);
                fmpz_mpoly_sub(res_num, x_num, t, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_mul(res_num, y_num, x_den, ctx);
                fmpz_mpoly_sub(res_num, x_num, res_num, ctx);
            }
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_fmpz(y_den, ctx)) {
            SubFMPZDenominatorHelper(res_num, res_den, x_num, x_den, y_num, y_den->coeffs, ctx);
            return;
        }

        if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
            SubFMPZDenominatorHelper(res_num, res_den, y_num, y_den, x_num, x_den->coeffs, ctx);
            fmpz_mpoly_neg(res_num, res_num, ctx);
            return;
        }

        {
            fmpz_mpoly_t g;
            fmpz_mpoly_init(g, ctx);

            AssertFMPZMpolyGcdSuccessful(g, x_den, y_den, ctx);

            if (fmpz_mpoly_is_one(g, ctx)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_sub does not work inplace */
                fmpz_mpoly_mul(t, x_num, y_den, ctx);
                fmpz_mpoly_mul(u, y_num, x_den, ctx);
                fmpz_mpoly_sub(res_num, t, u, ctx);
                fmpz_mpoly_mul(res_den, x_den, y_den, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_mpoly_t a, b, t, u;

                fmpz_mpoly_init(a, ctx);
                fmpz_mpoly_init(b, ctx);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                DivExactByMpoly(a, x_den, g, ctx);
                DivExactByMpoly(b, y_den, g, ctx);

                fmpz_mpoly_mul(t, x_num, b, ctx);
                fmpz_mpoly_mul(u, y_num, a, ctx);
                fmpz_mpoly_sub(res_num, t, u, ctx);

                AssertFMPZMpolyGcdSuccessful(t, res_num, g, ctx);

                if (fmpz_mpoly_is_one(t, ctx)) {
                    fmpz_mpoly_mul(res_den, x_den, b, ctx);
                } else {
                    DivExactByMpoly(res_num, res_num, t, ctx);
                    DivExactByMpoly(g, x_den, t, ctx);
                    fmpz_mpoly_mul(res_den, g, b, ctx);
                }

                fmpz_mpoly_clear(a, ctx);
                fmpz_mpoly_clear(b, ctx);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_mpoly_clear(g, ctx);
        }
    }

    void TMpolyFractionHolder::SubFMPZDenominatorHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        fmpz_t g;
        fmpz_init(g);

        if (fmpz_mpoly_is_fmpz(y_num, ctx)) {
            if (res_num == x_num || res_num == y_num) {
                fmpz_t t, u;
                fmpz_init_set(t, y_num->coeffs);
                fmpz_init_set(u, y_den);
                SubFMPQHelper(res_num, res_den, x_num, x_den, t, u, ctx);
                fmpz_clear(t);
                fmpz_clear(u);
            } else {
                SubFMPQHelper(res_num, res_den, x_num, x_den, y_num->coeffs, y_den, ctx);
            }
            return;
        }

        if (fmpz_mpoly_is_fmpz(x_den, ctx)) {
            fmpz_gcd(g, x_den->coeffs, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_sub does not work inplace */
                fmpz_mpoly_scalar_mul_fmpz(t, y_num, x_den->coeffs, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, y_den, ctx);
                fmpz_mpoly_sub(res_num, u, t, ctx);
                fmpz_mul(g, x_den->coeffs, y_den);
                fmpz_mpoly_set_fmpz(res_den, g, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_t a, b;
                fmpz_mpoly_t t, u;

                fmpz_init(a);
                fmpz_init(b);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_divexact(a, y_den, g);
                fmpz_divexact(b, x_den->coeffs, g);

                fmpz_mpoly_scalar_mul_fmpz(t, y_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, a, ctx);
                fmpz_mpoly_sub(res_num, u, t, ctx);

                if (fmpz_mpoly_is_zero(res_num, ctx)) {
                    fmpz_one(a);
                } else {
                    FMPZVectorHelper(a, res_num->coeffs, res_num->length, g);
                }

                if (fmpz_is_one(a)) {
                    fmpz_mul(g, b, y_den);
                    fmpz_mpoly_set_fmpz(res_den, g, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, a, ctx);
                    fmpz_divexact(g, y_den, a);
                    fmpz_mul(g, g, b);
                    fmpz_mpoly_set_fmpz(res_den, g, ctx);
                }

                fmpz_clear(a);
                fmpz_clear(b);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }
        } else {
            FMPZVectorHelper(g, x_den->coeffs, x_den->length, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                /* todo: avoid one alloc? not helpful right now because
                fmpz_mpoly_sub does not work inplace */
                fmpz_mpoly_mul(t, y_num, x_den, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, y_den, ctx);
                fmpz_mpoly_sub(res_num, u, t, ctx);
                fmpz_set(g, y_den);
                fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, g, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_t a;
                fmpz_mpoly_t b, t, u;

                fmpz_init(a);
                fmpz_mpoly_init(b, ctx);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_divexact(a, y_den, g);
                fmpz_mpoly_scalar_divexact_fmpz(b, x_den, g, ctx);

                fmpz_mpoly_mul(t, y_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, x_num, a, ctx);
                fmpz_mpoly_sub(res_num, u, t, ctx);

                if (fmpz_mpoly_is_zero(res_num, ctx)) {
                    fmpz_one(a);
                } else {
                    FMPZVectorHelper(a, res_num->coeffs, res_num->length, g);
                }

                if (fmpz_is_one(a)) {
                    fmpz_set(g, y_den);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, b, g, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, a, ctx);
                    fmpz_divexact(g, y_den, a);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, b, g, ctx);
                }

                fmpz_clear(a);
                fmpz_mpoly_clear(b, ctx);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }
        }

        fmpz_clear(g);
    }

    void TMpolyFractionHolder::SubFMPQHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_t y_num, const fmpz_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_is_zero(y_num)) {
            fmpz_mpoly_set(res_num, x_num, ctx);
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        if (fmpz_mpoly_is_zero(x_num, ctx)) {
            fmpz_mpoly_set_fmpz(res_num, y_num, ctx);
            fmpz_neg(res_num->coeffs, res_num->coeffs);
            fmpz_mpoly_set_fmpz(res_den, y_den, ctx);
            return;
        }

        /* todo: special-case integer x_den */

        if (fmpz_mpoly_equal_fmpz(x_den, y_den, ctx)) {
            fmpz_mpoly_sub_fmpz(res_num, x_num, y_num, ctx);

            if (fmpz_is_one(y_den)) {
                fmpz_mpoly_one(res_den, ctx);
            } else {
                fmpz_t t;
                fmpz_init(t);

                FMPZVectorHelper(t, res_num->coeffs, res_num->length, y_den);

                if (fmpz_is_one(t))
                {
                    fmpz_mpoly_set(res_den, x_den, ctx);
                }
                else
                {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, t, ctx);
                    fmpz_divexact(t, y_den, t);
                    fmpz_mpoly_set_fmpz(res_den, t, ctx);
                }

                fmpz_clear(t);
            }

            return;
        }

        if (fmpz_mpoly_is_one(x_den, ctx)) {
            fmpz_mpoly_scalar_mul_fmpz(res_num, x_num, y_den, ctx);
            fmpz_mpoly_sub_fmpz(res_num, res_num, y_num, ctx);
            fmpz_mpoly_set_fmpz(res_den, y_den, ctx);
            return;
        }

        if (fmpz_is_one(y_den)) {
            if (res_num == x_num) {
                fmpz_mpoly_t t;
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_scalar_mul_fmpz(t, x_den, y_num, ctx);
                fmpz_mpoly_sub(res_num, x_num, t, ctx);
                fmpz_mpoly_clear(t, ctx);
            } else {
                fmpz_mpoly_scalar_mul_fmpz(res_num, x_den, y_num, ctx);
                fmpz_mpoly_sub(res_num, x_num, res_num, ctx);
            }
            fmpz_mpoly_set(res_den, x_den, ctx);
            return;
        }

        {
            fmpz_t g;
            fmpz_init(g);

            FMPZVectorHelper(g, x_den->coeffs, x_den->length, y_den);

            if (fmpz_is_one(g)) {
                fmpz_mpoly_t t, u;

                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_mpoly_scalar_mul_fmpz(t, x_num, y_den, ctx);  /* todo: avoid one alloc? */
                fmpz_mpoly_scalar_mul_fmpz(u, x_den, y_num, ctx);
                fmpz_mpoly_sub(res_num, t, u, ctx);
                fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, y_den, ctx);

                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            } else {
                fmpz_mpoly_t t, u;
                fmpz_t b, c;

                fmpz_init(b);
                fmpz_init(c);
                fmpz_mpoly_init(t, ctx);
                fmpz_mpoly_init(u, ctx);

                fmpz_mpoly_scalar_divexact_fmpz(u, x_den, g, ctx);
                fmpz_divexact(b, y_den, g);

                fmpz_mpoly_scalar_mul_fmpz(t, x_num, b, ctx);
                fmpz_mpoly_scalar_mul_fmpz(u, u, y_num, ctx);
                fmpz_mpoly_sub(res_num, t, u, ctx);

                FMPZVectorHelper(c, res_num->coeffs, res_num->length, g);

                if (fmpz_is_one(c)) {
                    fmpz_mpoly_scalar_mul_fmpz(res_den, x_den, b, ctx);
                } else {
                    fmpz_mpoly_scalar_divexact_fmpz(res_num, res_num, c, ctx);
                    fmpz_mpoly_scalar_divexact_fmpz(res_den, x_den, c, ctx);
                    fmpz_mpoly_scalar_mul_fmpz(res_den, res_den, b, ctx);
                }

                fmpz_clear(b);
                fmpz_clear(c);
                fmpz_mpoly_clear(t, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_clear(g);
        }
    }

    void TMpolyFractionHolder::MulAnotherAndStoreHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_mpoly_is_zero(x_num, ctx) || fmpz_mpoly_is_zero(y_num, ctx)) {
            fmpz_mpoly_zero(res_num, ctx);
            fmpz_mpoly_one(res_den, ctx);
            return;
        }

        if (fmpz_mpoly_equal(x_den, y_den, ctx)) {
            fmpz_mpoly_mul(res_num, x_num, y_num, ctx);
            fmpz_mpoly_mul(res_den, x_den, y_den, ctx);
            return;
        }

        /* todo: special-case integer denominators; scalar multiplication */

        if (fmpz_mpoly_is_one(x_den, ctx)) {
            fmpz_mpoly_t t, u;
            fmpz_mpoly_init(t, ctx);

            AssertFMPZMpolyGcdSuccessful(t, x_num, y_den, ctx);

            if (fmpz_mpoly_is_one(t, ctx)) {
                fmpz_mpoly_mul(res_num, x_num, y_num, ctx);
                fmpz_mpoly_mul(res_den, x_den, y_den, ctx);
            } else {
                fmpz_mpoly_init(u, ctx);
                DivExactByMpoly(u, x_num, t, ctx);
                fmpz_mpoly_mul(res_num, u, y_num, ctx);
                DivExactByMpoly(u, y_den, t, ctx);
                fmpz_mpoly_mul(res_den, x_den, u, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_mpoly_clear(t, ctx);
            return;
        }

        if (fmpz_mpoly_is_one(y_den, ctx)) {
            fmpz_mpoly_t t, u;
            fmpz_mpoly_init(t, ctx);

            AssertFMPZMpolyGcdSuccessful(t, y_num, x_den, ctx);

            if (fmpz_mpoly_is_one(t, ctx)) {
                fmpz_mpoly_mul(res_num, x_num, y_num, ctx);
                fmpz_mpoly_mul(res_den, x_den, y_den, ctx);
            } else {
                fmpz_mpoly_init(u, ctx);
                DivExactByMpoly(u, y_num, t, ctx);
                fmpz_mpoly_mul(res_num, u, x_num, ctx);
                DivExactByMpoly(u, x_den, t, ctx);
                fmpz_mpoly_mul(res_den, y_den, u, ctx);
                fmpz_mpoly_clear(u, ctx);
            }

            fmpz_mpoly_clear(t, ctx);
            return;
        }

        {
            fmpz_mpoly_t t, u, x, y;

            fmpz_mpoly_init(t, ctx);
            fmpz_mpoly_init(u, ctx);
            fmpz_mpoly_init(x, ctx);
            fmpz_mpoly_init(y, ctx);

            AssertFMPZMpolyGcdSuccessful(t, x_num, y_den, ctx);

            if (fmpz_mpoly_is_one(t, ctx)) {
                AssertFMPZMpolyGcdSuccessful(u, x_den, y_num, ctx);

                if (fmpz_mpoly_is_one(u, ctx)) {
                    fmpz_mpoly_mul(res_num, x_num, y_num, ctx);
                    fmpz_mpoly_mul(res_den, x_den, y_den, ctx);
                } else {
                    fmpz_mpoly_div(y, y_num, u, ctx);
                    fmpz_mpoly_mul(res_num, x_num, y, ctx);

                    fmpz_mpoly_div(x, x_den, u, ctx);
                    fmpz_mpoly_mul(res_den, x, y_den, ctx);
                }
            } else {
                AssertFMPZMpolyGcdSuccessful(u, x_den, y_num, ctx);

                if (fmpz_mpoly_is_one(u, ctx)) {
                    fmpz_mpoly_div(x, x_num, t, ctx);
                    fmpz_mpoly_mul(res_num, x, y_num, ctx);

                    fmpz_mpoly_div(y, y_den, t, ctx);
                    fmpz_mpoly_mul(res_den, x_den, y, ctx);
                } else {
                    fmpz_mpoly_div(x, x_num, t, ctx);
                    fmpz_mpoly_div(y, y_num, u, ctx);
                    fmpz_mpoly_mul(res_num, x, y, ctx);

                    fmpz_mpoly_div(x, x_den, u, ctx);
                    fmpz_mpoly_div(y, y_den, t, ctx);
                    fmpz_mpoly_mul(res_den, x, y, ctx);
                }
            }

            fmpz_mpoly_clear(t, ctx);
            fmpz_mpoly_clear(u, ctx);
            fmpz_mpoly_clear(x, ctx);
            fmpz_mpoly_clear(y, ctx);
        }
    }

    void TMpolyFractionHolder::DivAnotherAndStoreHelper(
        fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
        const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
        const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
        const fmpz_mpoly_ctx_t ctx
    ) {
        if (fmpz_mpoly_is_zero(y_num, ctx)) {
            flint_printf("DivAnotherAndStoreHelper: division by zero\n");
            flint_abort();
        }

        if (fmpz_mpoly_is_zero(x_num, ctx) || fmpz_mpoly_is_zero(y_num, ctx)) {
            fmpz_mpoly_zero(res_num, ctx);
            fmpz_mpoly_one(res_den, ctx);
            return;
        }

        if (res_num == y_num) {
            fmpz_mpoly_t t, u;
            fmpz_mpoly_init(t, ctx);
            fmpz_mpoly_init(u, ctx);
            MulAnotherAndStoreHelper(t, u, x_num, x_den, y_den, y_num, ctx);
            fmpz_mpoly_swap(res_num, t, ctx);
            fmpz_mpoly_swap(res_den, u, ctx);
            fmpz_mpoly_clear(t, ctx);
            fmpz_mpoly_clear(u, ctx);
        } else {
            MulAnotherAndStoreHelper(res_num, res_den, x_num, x_den, y_den, y_num, ctx);
        }

        if (fmpz_sgn(res_den->coeffs) < 0) {
            fmpz_mpoly_neg(res_num, res_num, ctx);
            fmpz_mpoly_neg(res_den, res_den, ctx);
        }
    }

    void TMpolyFractionHolder::Canonicalize(const fmpz_mpoly_ctx_t ctx) {
        if (fmpz_mpoly_is_one(&Denominator_, ctx)) {
            return;
        } else if (fmpz_mpoly_is_zero(&Numerator_, ctx)) {
            fmpz_mpoly_one(&Denominator_, ctx);
            return;
        } else if (fmpz_mpoly_is_fmpz(&Denominator_, ctx)) {
            fmpz_t gcd;
            fmpz_init(gcd);

            _fmpz_vec_content(gcd, Numerator_.coeffs, Numerator_.length);
            fmpz_gcd(gcd, gcd, Denominator_.coeffs);

            if (fmpz_sgn(Denominator_.coeffs) < 0) {
                fmpz_neg(gcd, gcd);
            }

            if (!fmpz_is_one(gcd)) {
                fmpz_mpoly_scalar_divexact_fmpz(&Numerator_, &Numerator_, gcd, ctx);
                fmpz_divexact(Denominator_.coeffs, Denominator_.coeffs, gcd);
            }

            fmpz_clear(gcd);
        } else if (fmpz_mpoly_is_fmpz(&Numerator_, ctx)) {
            fmpz_t gcd;
            fmpz_init(gcd);

            _fmpz_vec_content(gcd, Denominator_.coeffs, Denominator_.length);
            fmpz_gcd(gcd, gcd, Numerator_.coeffs);

            if (fmpz_sgn(Denominator_.coeffs) < 0) {
                fmpz_neg(gcd, gcd);
            }

            if (!fmpz_is_one(gcd)) {
                fmpz_mpoly_scalar_divexact_fmpz(&Denominator_, &Denominator_, gcd, ctx);
                fmpz_divexact(Numerator_.coeffs, Numerator_.coeffs, gcd);
            }

            fmpz_clear(gcd);
        } else {
            fmpz_mpoly_t gcd;
            fmpz_mpoly_init(gcd, ctx);

            AssertFMPZMpolyGcdSuccessful(gcd, &Numerator_, &Denominator_, ctx);

            if (fmpz_sgn(Denominator_.coeffs) < 0) {
                fmpz_mpoly_neg(gcd, gcd, ctx);
            }

            if (!fmpz_mpoly_is_one(gcd, ctx)) {
                DivExactByMpoly(&Numerator_, &Numerator_, gcd, ctx);
                DivExactByMpoly(&Denominator_, &Denominator_, gcd, ctx);
            }

            fmpz_mpoly_clear(gcd, ctx);
        }
    }

    inline void TMpolyFractionHolder::FMPZVectorHelper(fmpz_t res, const fmpz * vec, slong len, const fmpz_t inp) {
        if (fmpz_is_pm1(inp)) {
            fmpz_one(res);
        } else {
            slong i;
            fmpz_abs(res, inp);
            for (i = len - 1; i >= 0; i--) {
                fmpz_gcd(res, res, vec + i);
                if (fmpz_is_one(res)) {
                    break;
                }
            }
        }
    }

    inline void TMpolyFractionHolder::DivExactByMpoly(fmpz_mpoly_t res, const fmpz_mpoly_t x, const fmpz_mpoly_t y, const fmpz_mpoly_ctx_t ctx) {
        if (fmpz_mpoly_is_fmpz(y, ctx)) {
            fmpz_mpoly_scalar_divexact_fmpz(res, x, y->coeffs, ctx);
        } else {
            fmpz_mpoly_div(res, x, y, ctx);
        }
    }

    inline void TMpolyFractionHolder::AssertFMPZMpolyGcdSuccessful(fmpz_mpoly_t res, const fmpz_mpoly_t x, const fmpz_mpoly_t y, const fmpz_mpoly_ctx_t ctx) {
        if (!fmpz_mpoly_gcd(res, x, y, ctx)) {
            flint_printf("fmpz_mpoly_gcd failed\n");
            flint_abort();
        }
    }

    void MyDeleter::operator()(char *p)
    {
        free(p);
    }

    std::mutex ctx_wrapper_init_mutex; //!< For initialization of `ctx_wrapper` by different threads

    void wrapper_fmpz_mpoly_ctx_t::initialize_vars(const std::vector<std::string> &vars1)
    {
        size_t nvars = vars1.size();
        if (initialized)
            fmpz_mpoly_ctx_clear(ctx);
        fmpz_mpoly_ctx_init(ctx, nvars, ORD_LEX);

        vars_vector.resize(nvars);
        vars_string_vector.resize(nvars);
        for (size_t i = 0; i < nvars; ++i)
        {
            vars_string_vector[i] = vars1[i];
            vars_vector[i] = (char *)vars_string_vector[i].c_str();
        }
        vars = &vars_vector[0];

        initialized = true; // ctx and vars have been initialized
    }

    wrapper_fmpz_mpoly_ctx_t::~wrapper_fmpz_mpoly_ctx_t()
    {
        if (initialized)
            fmpz_mpoly_ctx_clear(ctx);
    }

    std::vector <std::pair<std::string, rational_function>> rational_function::variable_mapping;

    wrapper_fmpz_mpoly_ctx_t rational_function::ctx_wrapper;

    void rational_function::initialize_vars(const std::vector<std::string> &vars1, uint64_t modulus)
    {
        std::lock_guard<std::mutex> guard(ctx_wrapper_init_mutex);
        ctx_wrapper.initialize_vars(vars1);
        variable_mapping.clear();
        for (auto &varstring : vars1)
        {
            rational_function value = rational_function::parse_poly_to_ratfunc(varstring);
            auto mapping_rule = std::make_pair(varstring, value);
            variable_mapping.push_back(mapping_rule);
        }
    }

    rational_function::rational_function()
    {
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);
    }

    rational_function::rational_function(const rational_function &another)
    {
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);
        data->SetByAnother(another.data, ctx_wrapper.ctx);
    }

    rational_function &rational_function::operator=(const rational_function &another)
    {
        if (data != static_cast<TMpolyFractionHolder *>(nullptr))
        {
            data->Clear(ctx_wrapper.ctx);
            delete data;
        }
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);
        data->SetByAnother(another.data, ctx_wrapper.ctx);
        return *this;
    }

    rational_function::rational_function(rational_function &&another) noexcept
    {
        data = another.data;
        another.data = static_cast<TMpolyFractionHolder *>(nullptr);
    }

    rational_function &rational_function::operator=(rational_function &&a) noexcept
    {
        if (data != static_cast<TMpolyFractionHolder *>(nullptr))
        {
            data->Clear(ctx_wrapper.ctx);
            delete data;
        }
        data = a.data;
        a.data = static_cast<TMpolyFractionHolder *>(nullptr);
        return *this;
    }

    rational_function::rational_function(bool is_fast, const char *numer_str, const char *denom_str, bool canonicalize) {
        data = new TMpolyFractionHolder;

        auto vars = (const char **)ctx_wrapper.vars;
        auto ctx = ctx_wrapper.ctx;
        data->InitByCtx(ctx);


        if (is_fast) {
            char *copiedNumerStr = new char[strlen(numer_str)+1];
            strcpy(copiedNumerStr, numer_str);

            char *copiedDenomStr = new char[strlen(denom_str)+1];
            strcpy(copiedDenomStr, denom_str);

            SetPolyEqualToDecomposedStr(data->GetNumerator(), copiedNumerStr, vars, ctx);
            SetPolyEqualToDecomposedStr(data->GetDenominator(), copiedDenomStr, vars, ctx);

            delete[] copiedNumerStr;
            delete[] copiedDenomStr;
        }

        if (canonicalize) {
            data->Canonicalize(ctx);
        }
    }

    rational_function::rational_function(const char *numer_str, const char *denom_str, bool canonicalize)
    {
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);

        fmpz_mpoly_set_str_pretty(data->GetNumerator(), numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        fmpz_mpoly_set_str_pretty(data->GetDenominator(), denom_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);

        if (canonicalize)
            data->Canonicalize(ctx_wrapper.ctx);

    }

    rational_function::rational_function(const char *numer_str)
    {
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);

        fmpz_mpoly_set_str_pretty(data->GetNumerator(), numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);

        fmpz_mpoly_one(data->GetDenominator(), ctx_wrapper.ctx);
    }

    rational_function::rational_function(fmpz_t i)
    {
        data = new TMpolyFractionHolder;
        data->InitByCtx(ctx_wrapper.ctx);

        data->SetByNumber(i, ctx_wrapper.ctx);
    }

    rational_function::~rational_function()
    {
        if (data != static_cast<TMpolyFractionHolder *>(nullptr))
        {
            data->Clear(ctx_wrapper.ctx);
            delete data;
        }
    }

    std::string rational_function::to_string() const
    {
        char *numstring = fmpz_mpoly_get_str_pretty(data->GetNumerator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        char *denstring = fmpz_mpoly_get_str_pretty(data->GetDenominator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        if (!strcmp(numstring, "0"))
        {
            return "0";
        }
        std::string result = "(";
        result += numstring;
        result += ")/(";
        result += denstring;
        result += ")";
        free(numstring);
        free(denstring);
        return result;
    }

    std::pair<char *, char *> rational_function::get_num_den_strings() const
    {
        char *numstring = fmpz_mpoly_get_str_pretty(data->GetNumerator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        char *denstring = fmpz_mpoly_get_str_pretty(data->GetDenominator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        return std::make_pair(numstring, denstring);
    }

    std::unique_ptr<char[], MyDeleter> rational_function::num_string() const
    {
        char *numstring = fmpz_mpoly_get_str_pretty(data->GetNumerator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        std::unique_ptr<char[], MyDeleter> result(numstring);
        return result;
    }

    std::unique_ptr<char[], MyDeleter> rational_function::den_string() const
    {
        char *denstring = fmpz_mpoly_get_str_pretty(data->GetDenominator(), (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        std::unique_ptr<char[], MyDeleter> result(denstring);
        return result;
    }

    rational_function rational_function::operator+(const rational_function &another)
    {
        rational_function storage;
        data->AddAnotherAndStore(storage.data, another.data, ctx_wrapper.ctx);
        return storage;
    }

    rational_function rational_function::operator-(const rational_function &another)
    {
        rational_function storage;
        data->SubAnotherAndStore(storage.data, another.data, ctx_wrapper.ctx);
        return storage;
    }

    rational_function rational_function::operator-()
    {
        rational_function result;
        result.data->StoreNegatedTo(data, ctx_wrapper.ctx);
        return result;
    }

    rational_function rational_function::operator*(const rational_function &another)
    {
        rational_function storage;
        data->MulAnotherAndStore(storage.data, another.data, ctx_wrapper.ctx);
        return storage;
    }

    rational_function rational_function::operator/(const rational_function &another)
    {
        rational_function storage;
        data->DivAnotherAndStore(storage.data, another.data, ctx_wrapper.ctx);
        return storage;
    }

    bool rational_function::operator==(const rational_function &another) const
    {
        return data->IsEqual(another.data, ctx_wrapper.ctx);
    }

    // in-place versions
    rational_function &rational_function::operator+=(const rational_function &another)
    {
        data->AddAnotherAndStore(data, another.data, ctx_wrapper.ctx);
        return *this;
    }

    rational_function &rational_function::operator-=(const rational_function &another)
    {
        data->SubAnotherAndStore(data, another.data, ctx_wrapper.ctx);
        return *this;
    }

    rational_function &rational_function::operator*=(const rational_function &another)
    {
        data->MulAnotherAndStore(data, another.data, ctx_wrapper.ctx);
        return *this;
    }

    rational_function &rational_function::operator/=(const rational_function &another)
    {
        data->DivAnotherAndStore(data, another.data, ctx_wrapper.ctx);
        return *this;
    }

    rational_function &rational_function::neg()
    {
        data->StoreNegatedTo(data, ctx_wrapper.ctx);
        return *this;
    }

    int rational_function::size()
    {
        return fmpz_mpoly_length(data->GetNumerator(), ctx_wrapper.ctx) + fmpz_mpoly_length(data->GetDenominator(), ctx_wrapper.ctx);
    }

    rational_function pow(const rational_function &a, int b)
    {
        rational_function result;

        if (b > 0)
        {
            fmpz_mpoly_pow_ui((result.data)->GetNumerator(), (a.data)->GetNumerator(), b, rational_function::ctx_wrapper.ctx);
            fmpz_mpoly_pow_ui((result.data)->GetDenominator(), (a.data)->GetDenominator(), b, rational_function::ctx_wrapper.ctx);
            return result;
        }
        else
        {
            fmpz_mpoly_pow_ui((result.data)->GetNumerator(), (a.data)->GetDenominator(), -b, rational_function::ctx_wrapper.ctx);
            fmpz_mpoly_pow_ui((result.data)->GetDenominator(), (a.data)->GetNumerator(), -b, rational_function::ctx_wrapper.ctx);
            return result;
        }
    }

    std::ostream &operator<<(std::ostream &os, const rational_function &q)
    {
        // fmpz_mpoly_q_print_pretty(q.data, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        os << q.to_string();
        return os;
    }

    rational_function rational_function::parse_poly_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        return rational_function(s.c_str());
    }

    rational_function rational_function::parse_int_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        // return rational_function(s.c_str());
        fmpz_t intresult;
        fmpz_init(intresult);
        fmpz_set_str(intresult, s.c_str(), 10); // parse integer base 10
        rational_function result(intresult);
        fmpz_clear(intresult);
        return result;
    }

} // namespace fuel::ratfunc_flint

void SetPolyEqualToDecomposedStr(fmpz_mpoly_t poly, char *polyString, const char **vars, const fmpz_mpoly_ctx_t ctx) {
    size_t varsCount = ctx->minfo->nvars;

    char *ptr = polyString;

    // Read until '\0'
    while (*ptr) {
        ptr++; // Skip '<'

        char *startOfNumber = ptr;
        while (std::isdigit(*ptr) || *ptr == '-' || *ptr == '+') ptr++; // Skip number

        char remembered = *ptr; // temporary change symbol after number to '\0'
        *ptr = '\0';

        TMonome current_monome(varsCount);
        current_monome.coeff.SetStr(startOfNumber);

        *ptr = remembered; // change symbol back

        size_t i = 0;
        // Read exponents until '>'
        while (*ptr && *ptr != '>') {
            if (*ptr == '.') {
                ptr++; // Skip '.'
            } else if (std::isdigit(*ptr) || *ptr == '-' || *ptr == '+') {
                ulong exponent = std::atoll(ptr);
                current_monome.exponents[i] = exponent;
                ++i;
                while (std::isdigit(*ptr) || *ptr == '-' || *ptr == '+') ptr++; // Skip number
            }
        }

        ptr++; // Skip '>'

        fmpz_mpoly_push_term_fmpz_ui(poly, current_monome.coeff.f, &(current_monome.exponents[0]), ctx);
    }
}
