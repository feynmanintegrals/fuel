#include "ginac.h"

namespace fuel {

    void ginac::initializeInternal(const vector<string>& vars) {
        ginac_vars = {};
        if (vars.size() > 0) {
            ginac_vars_array = new GiNaC::symbol[vars.size()];
            int i = 0;
            for (const auto& var : vars) {                
                ginac_vars_array[i] = GiNaC::symbol(var);
                ginac_vars.append(ginac_vars_array[i]);
                ++i;
            }
        }
    }

    void ginac::evaluateInternal(const char* expr, bool modular) {
        GiNaC::ex expr_ginac(expr, ginac_vars);

        //GiNaC simplification
        expr_ginac = expr_ginac.normal();

        if (modular) {
            GiNaC::numeric n_nm = GiNaC::ex_to < GiNaC::numeric > (expr_ginac);
            //std::cout << "We need " << n_nm << std::endl;
            GiNaC::numeric n_prime = GiNaC::ex_to < GiNaC::numeric > (GiNaC::ex(this->primeBuf, GiNaC::lst()));
         
            // separately taking numerator and denumatator modular prime to fit both in needed range 
            GiNaC::numeric n_nnm, n_dnm;            
            if (n_nm.is_rational()) {
                n_nnm = n_nm.numer();
                n_dnm = irem(n_nm.denom(), n_prime);
            } else {
                n_nnm = n_nm;
                n_dnm = 1;
            }
            n_nnm = irem(n_nnm, n_prime);
            if (n_nnm.is_negative() && n_nnm != 0) {
                n_nnm = n_nnm + n_prime;
            }
            expr_ginac = n_nnm / n_dnm;
            //std::cout << "We got " << expr_ginac << std::endl;
        }

        stringstream ss;
        ss << expr_ginac;
        string rc_res = ss.str();
        
        for (auto c : rc_res) {
            if (c > ' ') addToOut(c);
        }
    }

    ginac::~ginac() {
        if (ginac_vars_array) {
            delete[] ginac_vars_array;
            ginac_vars_array = nullptr;
        }
    }
}
