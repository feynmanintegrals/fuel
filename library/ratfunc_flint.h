/** @file ratfunc_flint.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "monome_flint.h"

#include <string_view>
#include <utility>
#include <memory>
#include <vector>
#include "flint/fmpz.h"
#include "flint/fmpz_mpoly.h"
#include "flint/fmpz_vec.h"
#include <iostream>
#include <cinttypes>
#include <unordered_map>

/**
 * function used to set fmpz_mpoly_t poly to polynom contained in polyString. polyString contains a polynom in decomposed representation
 * @param poly fmpz_mpoly_t polynom to set
 * @param polyString is c-style string that contains polynom in decomposed representation
 * @param vars vector of strings, representing the list of variables
 * @param ctx context
*/
void SetPolyEqualToDecomposedStr(fmpz_mpoly_t poly, char *polyString, const char **vars, const fmpz_mpoly_ctx_t ctx);

/**
 * Custom C++ wrapper for FLINT's multivariate rational functions `fmpz_mpoly_q`
*/
namespace fuel::ratfunc_flint
{

    /**
     * Class used instead of fmpz_mpoly_q structure.
     * It contains numerator and denominator of FLINT's multivariate rational function and operators overloads
    */
    class TMpolyFractionHolder {
    public:
        TMpolyFractionHolder() {}

        /**
         * Init class instance with ctx
         * @param ctx context
        */
        void InitByCtx(fmpz_mpoly_ctx_t ctx) {
            fmpz_mpoly_init(&Numerator_, ctx);
            fmpz_mpoly_init(&Denominator_, ctx);
        }

        /**
         * Set class instance equal to 'another' instance
         * @param another another instance
         * @param ctx context
        */
        void SetByAnother(TMpolyFractionHolder *another, fmpz_mpoly_ctx_t ctx) {
            fmpz_mpoly_set(&Numerator_, another->GetNumerator(), ctx);
            fmpz_mpoly_set(&Denominator_, another->GetDenominator(), ctx);
        }

        /**
         * Set class instance equal to fmpz_t
         * @param number number to which self will be equal
         * @param ctx context
        */
        void SetByNumber(const fmpz_t number, const fmpz_mpoly_ctx_t ctx) {
            fmpz_mpoly_set_fmpz(&Numerator_, number, ctx);
            fmpz_mpoly_one(&Denominator_, ctx);
        }

        /**
         * Store negated class instance to 'another' instance
         * @param another another instance to store negated self in
         * @param ctx context
        */
        void StoreNegatedTo(TMpolyFractionHolder *another, fmpz_mpoly_ctx_t ctx) {
            fmpz_mpoly_neg(&Numerator_, another->GetNumerator(), ctx);
            fmpz_mpoly_set(&Denominator_, another->GetDenominator(), ctx);
        }

        /**
         * Add self and 'another' instances; Store result to 'storage' instance
         * @param storage instance to store result in
         * @param another instance to add to self
         * @param ctx context
        */
        void AddAnotherAndStore(TMpolyFractionHolder *storage, const TMpolyFractionHolder *another, const fmpz_mpoly_ctx_t ctx) {
            AddAnotherAndStoreHelper(
                storage->GetNumerator(), storage->GetDenominator(),
                &Numerator_, &Denominator_,
                another->GetNumerator(), another->GetDenominator(),
                ctx
            );
        }

        /**
         * Subtract 'another' from self instance; Store result to 'storage' instance
         * @param storage instance to store result in
         * @param another instance to subtract from self
         * @param ctx context
        */
        void SubAnotherAndStore(TMpolyFractionHolder *storage, const TMpolyFractionHolder *another, const fmpz_mpoly_ctx_t ctx) {
            SubAnotherAndStoreHelper(
                storage->GetNumerator(), storage->GetDenominator(),
                &Numerator_, &Denominator_,
                another->GetNumerator(), another->GetDenominator(),
                ctx
            );
        }

        /**
         * Multiply self by 'another' instances; Store result to 'storage' instance
         * @param storage instance to store result in
         * @param another instance by which self will be multiplied
         * @param ctx context
        */
        void MulAnotherAndStore(TMpolyFractionHolder *storage, const TMpolyFractionHolder *another, const fmpz_mpoly_ctx_t ctx) {
            MulAnotherAndStoreHelper(
                storage->GetNumerator(), storage->GetDenominator(),
                &Numerator_, &Denominator_,
                another->GetNumerator(), another->GetDenominator(),
                ctx
            );
        }

        /**
         * Divide self by 'another' instance; Store result to 'storage' instance
         * @param storage instance to store result in
         * @param another instance by which self will be divided
         * @param ctx context
        */
        void DivAnotherAndStore(TMpolyFractionHolder *storage, const TMpolyFractionHolder *another, const fmpz_mpoly_ctx_t ctx) {
            DivAnotherAndStoreHelper(
                storage->GetNumerator(), storage->GetDenominator(),
                &Numerator_, &Denominator_,
                another->GetNumerator(), another->GetDenominator(),
                ctx
            );
        }

        /**
         * Check if 'another' and self are equal
         * @param another instance to compare self with
         * @param ctx context
         * @return boolean result of comparing
        */
        bool IsEqual(const TMpolyFractionHolder *another, const fmpz_mpoly_ctx_t ctx) {
            return fmpz_mpoly_equal(&Numerator_, another->GetNumerator(), ctx)
                && fmpz_mpoly_equal(&Denominator_, another->GetDenominator(), ctx);
        }

        /**
         * Canonicalize self. Puts the numerator and denominator of self in canonical form by removing common content and making the leading term of the denominator positive
         * @param ctx context
        */
        void Canonicalize(const fmpz_mpoly_ctx_t ctx);

        /**
         * Returns pointer to numerator
         * @return pointer
        */
        fmpz_mpoly_struct* GetNumerator() const {
            return const_cast<fmpz_mpoly_struct*>(&Numerator_);
        }

        /**
         * Returns pointer to denominator
         * @return pointer
        */
        fmpz_mpoly_struct* GetDenominator() const {
            return const_cast<fmpz_mpoly_struct*>(&Denominator_);
        }

        /**
         * Clears both numerator and denominator. Freeing or recycling its allocated memory
         * @param ctx context
        */
        void Clear(const fmpz_mpoly_ctx_t ctx) {
            fmpz_mpoly_clear(&Numerator_, ctx);
            fmpz_mpoly_clear(&Denominator_, ctx);
        }

    private:
        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void AddAnotherAndStoreHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void AddFMPZDenominatorHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void AddFMPQHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_t y_num, const fmpz_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void SubAnotherAndStoreHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void SubFMPZDenominatorHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void SubFMPQHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_t y_num, const fmpz_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void MulAnotherAndStoreHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res_num numerator of instance where result will be stored
         * @param res_den denominator of instance where result will be stored
         * @param x_num x numerator
         * @param x_den x denominator
         * @param y_num y numerator
         * @param y_den y denominator
         * @param ctx context
        */
        void DivAnotherAndStoreHelper(
            fmpz_mpoly_t res_num, fmpz_mpoly_t res_den,
            const fmpz_mpoly_t x_num, const fmpz_mpoly_t x_den,
            const fmpz_mpoly_t y_num, const fmpz_mpoly_t y_den,
            const fmpz_mpoly_ctx_t ctx
        );

        /**
         * Helper function
         * @param res variable to store result in
         * @param vec vector of coeffs
         * @param len length of vec
         * @param inp input
        */
        void FMPZVectorHelper(fmpz_t res, const fmpz * vec, slong len, const fmpz_t inp);

        /**
         * Helper function
         * @param res variable to store result in
         * @param x polynomial, which will be divided
         * @param y polynomial by which the division will be performed
         * @param ctx context
        */
        void DivExactByMpoly(fmpz_mpoly_t res, const fmpz_mpoly_t x, const fmpz_mpoly_t y, const fmpz_mpoly_ctx_t ctx);

        /**
         * Helper function
         * @param res variable to store result in
         * @param x first arg of gcd
         * @param y second arg of gcd
         * @param ctx context
        */
        void AssertFMPZMpolyGcdSuccessful(fmpz_mpoly_t res, const fmpz_mpoly_t x, const fmpz_mpoly_t y, const fmpz_mpoly_ctx_t ctx);

    private:
        /**
         * Variable holds numerator
        */
        fmpz_mpoly_struct Numerator_;

        /**
         * Variable holds denominator
        */
        fmpz_mpoly_struct Denominator_;
    };

    /**
     * Wrapper struct storing the list of variables and the related FLINT context variable which essentially contains the same information
    */
    struct wrapper_fmpz_mpoly_ctx_t
    {
        /**
         * FLINT context variable for multivariate polynomials and rational functions
        */
        fmpz_mpoly_ctx_t ctx;

        /**
         * Whether initialization has previously taken place. If so, calling `initialize_vars()` will trigger deallocation of previous allocations first
        */
        bool initialized{false};

        /**
         * List of variable strings
        */
        std::vector<std::string> vars_string_vector;

        /**
         * List of variable strings as C strings
        */
        std::vector<char *> vars_vector;

        /**
         * List of variable strings in the `char**` type, required for interfacing with FLINT
        */
        char **vars;

        /**
         * Initialize the variable list and the related FLINT context
         * @param vars1 vector of strings, representing the list of variables
        */
        void initialize_vars(const std::vector<std::string> &vars1);

        /**
         * Destructor which handles deallocation of FLINT context
        */
        ~wrapper_fmpz_mpoly_ctx_t();
    };

    /**
     * Custom deleter for char* arrays allocated by C libraries, which need `free()` rather than `delete` for cleanup
     * @param p the array
     */
    struct MyDeleter
    {
        /**
         * The operator to be called
         * @param p the array
         */
        void operator()(char *p);
    };

    /**
     * C++ wrapper class for FLINT's `fmpz_mpoly_q` rational functions, with overloaded arithmetic operators and IO functions
    */
    class rational_function
    {

    public:

        static std::vector<std::pair<std::string, rational_function>> variable_mapping; //!< correspondance of variable string and same variables as flint functions

        /**
         * Static class member holding the context
         */
        static wrapper_fmpz_mpoly_ctx_t ctx_wrapper;

        /**
         * Set the list of variables for the multivariate rational functions
         * @param vars1 vector of strings, representing the list of variables
         * @param modulus an unused parameter (with default value) that can be ignored, solely for the purpose of making the interface identical to that of rational function modulo a prime, for the technical convenience of implementing a generic parser
         */
        static void initialize_vars(const std::vector<std::string> &vars1, uint64_t modulus = 0);

        /**
         * Pointer to FLINT rational function data
        */
        TMpolyFractionHolder *data;

        /**
         * Constructor which allocates memory but does not set the content
        */
        rational_function();

        /**
         * Copy constructor
         * @param a rational function being copied
        */
        rational_function(const rational_function &a);

        /**
         * Copy assignment operator
         * @param a rational function being copied
         * @return the copy
        */
        rational_function &operator=(const rational_function &a);

        /**
         * Move constructor
         * @param a rational function being moved
        */
        rational_function(rational_function &&a) noexcept;

        /**
         * Move assignment operator
         * @param a rational function being moved
         * @return the moved rational function
        */
        rational_function &operator=(rational_function &&a) noexcept;

        /**
         * Unimplemented parsing constructor used for constructing polynomes from decomposed representation contained in numer_str for numer and denom_str for denom strings
         * @param is_slow param used to distinguish decomposed representation constructor from others
         * @param numer_str numerator string
         * @param denom_str denominator string
         * @param canonicalize whether we canonicalize the fraction
         */
        rational_function(bool is_slow, const char *numer_str, const char *denom_str, bool canonicalize);

        /**
         * Parsing constructor
         * @param numer_str numerator string
         * @param denom_str denominator string
         * @param canonicalize whether we will call the flint canonicalization of a fraction
        */
        rational_function(const char *numer_str, const char *denom_str, bool canonicalize = false);

        /**
         * Construct a polynomial, i.e. rational function with trivial unit denominator
         * @param numer_str numerator string
        */
        rational_function(const char *numer_str); // With unit denominator

        /**
         * Construct a rational function that is a constant integer
         * @param i arbitray-precision integer of FLINT type `fmpz_t`
        */
        rational_function(fmpz_t i);

        /**
         * Destructor which frees memory
        */
        ~rational_function();

        /**
         * Convert the ratinonal function to a string
         * @return the string representation
        */
        std::string to_string() const;

        /**
         * Print the rational function into two C strings, one for the numerator and the other for the denominator. The user is reponsible for freeing the memory allocated to the `char*` strings by calling `free()`
         * @return the pair of C strings
        */
        std::pair<char *, char *> get_num_den_strings() const;

        /**
         * Convert the numerator of the rational function to a string
         * @return A smart pointer that manages the `char*` string
        */
        std::unique_ptr<char[], MyDeleter> num_string() const;

        /**
         * Convert the denominator of the rational function to a string
         * @return A smart pointer that manages the `char*` string
        */
        std::unique_ptr<char[], MyDeleter> den_string() const;

        /**
         * Add a rational function
         * @param a rational function to be added
         * @return result
         */
        rational_function operator+(const rational_function &a);

        /**
         * Substract a rational rational function
         * @param a rational function to be substracted
         * @return result
         */
        rational_function operator-(const rational_function &a);

        /**
         * Unary minus
         * @return minus original rational function
         */
        rational_function operator-();

        /**
         * Multiply a rational function
         * @param a rational function to be multiplied
         * @return result
         */
        rational_function operator*(const rational_function &a);

        /**
         * Divide a rational function
         * @param a rational function to be divided by
         * @return result
         */
        rational_function operator/(const rational_function &a);

        /**
         * Check equality of a rational function
         * @param a rational function to be checked
         * @return result
         */
        bool operator==(const rational_function &a) const;

        /**
         * In-place addition of a rational function
         * @param a rational function to be added
         * @return result
         */
        rational_function &operator+=(const rational_function &a);

        /**
         * In-place substraction of a rational function
         * @param a rational function to be substracted
         * @return result
         */
        rational_function &operator-=(const rational_function &a);

        /**
         * In-place multiplication of a rational function
         * @param a rational function to be multiplied
         * @return result
         */
        rational_function &operator*=(const rational_function &a);

        /**
         * In-place division of a rational function
         * @param a rational function to be divided by
         * @return result
         */
        rational_function &operator/=(const rational_function &a);

        /**
         * Turn the rational function to (-1) times itself in-place
         * @return the opposite rational function
        */
        rational_function &neg();

        /**
         * Size of the rational function, defined as the total number of monomial terms in the numerator polynomial and denominator polynomial
         * @return the size
        */
        int size();

        // Currently keeping everything as public, no need for friend declarations
        // friend rational_function pow(const rational_function &a, int b);
        // friend std::ostream &operator<<(std::ostream &os, const rational_function &q);

        /**
         * parse a polynomial string into a rational function
         * @param a a polynomial string which cannot contain `/` characters
         * @return rational function with unit denominator, i.e. actually a polynomial
        */
        static rational_function parse_poly_to_ratfunc(std::string_view a);

        /**
         * parse an integer string into a rational function
         * @param a a string which must represent a valid integer
         * @return rational function with unit denominator and integer numerator
        */
        static rational_function parse_int_to_ratfunc(std::string_view a);
    };

    /**
     * Compute an integer power of a rational function
     * @param a the rational function
     * @param b the integer exponent
     * @return `a` raised to the power `b`
    */
    rational_function pow(const rational_function &a, int b);

    /**
     * Put rational function to an ostream
     * @param os stream
     * @param q rational function
     * @return same stream
     */
    std::ostream &operator<<(std::ostream &os, const rational_function &q);
}
