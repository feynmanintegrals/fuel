
/** @file symbolica.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

namespace fuel {

    typedef struct SYMBOLICA* Symbolica;
    extern "C" {
        /**
         * Initilize symbolica library
         * @return symbolica instance
         */
        extern Symbolica *init();

        /**
         * Simplify an expression with the symbolica instance
         * @param symbolica Symbolica instance, should be initilized before
         * @param input expression to ber simplified
         * @param prime prime number for modular arithmetics, 0 for rationals
         * @param explicit_rat whether the answer should be in conventional form
         * @return resulting expression
         */
        extern char* simplify(Symbolica* symbolica, const char *input, unsigned long long prime, char explicit_rat);

        /**
         * Simplify an expression with the symbolica instance factorizing denominator
         * @param symbolica Symbolica instance, should be initilized before
         * @param input expression to ber simplified
         * @param prime prime number for modular arithmetics, 0 for rationals
         * @param explicit_rat whether the answer should be in conventional form
         * @return resulting expression
         */
        extern char* simplify_factorized(Symbolica* symbolica, const char *input, unsigned long long prime, char explicit_rat);

        /**
         * Set variables for the symbolica evaluations
         * @param symbolica Symbolica instance, should be initilized before
         * @param vars comma-separated variables
         */
        extern void set_vars(Symbolica* symbolica, const char *vars);

        /**
         * Drop symbolica instance
         * @param symbolica Symbolica instance, should be initilized before
         */
        extern void drop(Symbolica *symbolica);

        /**
         * Set options for symbolica instance
         * @param symbolica Symbolica instance, should be initilized before
         * @param input_has_rational_numbers whether input can have rational numbers (slower parsing)
         * @param exp_fits_in_u8 whether exponents fit into 2^8, faster if true
         */
        extern void set_options(Symbolica *symbolica, bool input_has_rational_numbers, bool exp_fits_in_u8);

        /**
         * Check whether symbolica is licensed
         * @return answer
         */
        extern bool is_licensed();

        /**
         * Set license key for symbolica
         * @param key the license key
         * @return whether it was set correctly
         */
        extern bool set_license_key(const char* key);

        /**
         * Request a temporary offline key for 24 hours from symbolica
         * @param key the buffer that should hold at least 100 symbols
         * @return whether it was obtained correctly
         */
        extern bool get_offline_license_key(char *key);

        /**
         * Request trial license for symbolica
         * @param name your name
         * @param email ypur email
         * @param company your company
         */
        extern void request_trial_license(const char* name, const char* email, const char* company);
    }

    /**
     * @brief Communication with symbolica: https://github.com/benruijl/symbolica
     */
    class symbolica : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;
            char explicit_rat = 1; //!< whether we switched to conventional
            Symbolica* s = nullptr; //!< symbolica instance
            bool input_has_rational_numbers = false; //!< whether input has rational numbers
            bool exp_fits_in_u8 = false; //!< whether exponents fit into 2^8
            bool factorize_denominators = false; //!< whether to call simplify_factorized instead of simplify
        public:
            symbolica() {}

            void switchToConventional() override;

            void setOption(const string& option) override;

            /**
             * Request trial license for symbolica
             * @param name your name
             * @param email ypur email
             * @param company your company
             */
            void requestTrialLicense(const char* name, const char* email, const char* company);

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<symbolica>();
            }

            ~symbolica();
    };

}
