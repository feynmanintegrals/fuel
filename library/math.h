
/** @file math.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

namespace fuel {

    /**
     * @brief Communication with Wolfram Mathematica: https://www.wolfram.com/mathematica/
     */
    class math : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;

        public:
            math() {}

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<math>();
            }

            ~math();
    };

}
