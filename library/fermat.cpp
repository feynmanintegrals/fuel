#include "fermat.h"

namespace fuel {

    void fermat::initializeInternal(const vector<string>& vars) {
        /*Switch off floating point representation:*/
        this->writeExpression("&d\n0\n");
        /*Set prompt as '\n':*/
        this->writeExpression("&(M=' ')\n", true);
        this->readUntil("> Prompt");
        this->readUntil("Elapsed");
        this->readLine();

        /*Switch off timing:*/
        this->writeExpression("&(t=0)\n", true);
        this->readUntil("0");
        this->readLine();

        /*Switch on "ugly" printing: no spaces in int integers;
         do not omit '*' for multiplication:*/
        this->writeExpression("&U\n", true);
        this->readUntil("0");
        this->readLine();

        /*Switch off suppression of output to terminal of int polys.:*/
        this->writeExpression("&(_s=0)\n", true);
        this->readUntil("0");
        this->readLine();

        /*Set polymomial variables:*/
        for (const auto& var: vars) {
            this->writeExpression("&(J=");
            this->writeExpression(var.c_str());
            this->writeExpression(")\n", true);
            this->readUntil("0");
            this->readLine();
        }

        /*Switch off fastest shortcut polygcd method (it is buggy in fermat-3.6.4!):*/
        this->writeExpression("&(_t=0)\n", true);
        this->readUntil("0");

        /*Set the number of bits for each symbol power. Fermat provides 128 bits in total.
         We choose 16 bits per symbol here, for a maximum of 128/16 = 8 symbols.
         If the problem exceeds this, Fermat reverts to the slow method.
         The default is 9 bits, for a maximum power of 511. This is *not* sufficient!
         16 b`its allows a maximum symbol power of 65535, which "should be" sufficient.*/
        this->writeExpression("&(_o=16)\n", true);
        this->readUntil("0");
        this->readLine();
    }

    void fermat::evaluateInternal(const char* expr, bool modular) {
        this->writeExpressionWithoutSpaces(expr);
        if (modular) {
            this->writeExpression("|");
            this->writeExpression(this->primeBuf);
        }
        this->writeExpression("\n", true);

        const char* c = this->readLine();
        while (*c == '\n') c = this->readLine(); // ignore first new lines

        while (*c != '\n') { // now we are waiting for \n\n as terminator
            if (*c > ' ') this->addToOut(*c);
            c++;
            if (*c == '\0') c = this->readLine(); // we get new line, now *c won't be \0 anyway
            if (*c == '\n') c = this->readLine(); // intentionally not OR with previous line; we read new line. If the next line is empty, it is again \n
        }
    }

    fermat::~fermat() {
        if (this->childpid) {
            this->writeExpression("&q\n", true);
        }
    }
}
