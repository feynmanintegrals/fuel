#include "base.h"

namespace fuel {

    bool base::startProgram(const string& binaryPath) {
        if (access(binaryPath.c_str(), 0) == -1) {
            cout << "Simplification library binary not found. Path: " << binaryPath << endl;
            return false;
        }
        pid_t childpid;
        if ((childpid = fork()) == -1) {
            perror("fork");
            return (0);
        }
        if (childpid == 0) {
            /* Child process closes up input side of pipe */
            close(this->fdin[1]);
            close(this->fdout[0]);
            /*redirect stdin*/
            if (dup2(this->fdin[0], STDIN_FILENO) < 0) {
                perror("dup2 stdin");
                return 0;
            }
            /*redirect stdout*/
            if (dup2(this->fdout[1], STDOUT_FILENO) < 0) {
                perror("dup2 stdout");
                return 0;
            }
            /*stderr > /dev/null*/
            dup2(open("/dev/null", O_WRONLY), STDERR_FILENO);
            char** argv = new char*[this->launchArguments.size()+2];
            argv[0] = new char[binaryPath.size()+1];
            strncpy(argv[0], binaryPath.c_str(), binaryPath.size()+1);
            for (size_t i = 0; i != this->launchArguments.size(); ++i) {
                if (this->launchArguments[i][0] == '.' && this->launchArguments[i][1] == '/') {
                    const char* pos = binaryPath.c_str() + strlen(binaryPath.c_str()) - 1;
                    while (*pos != '/') --pos;
                    argv[i + 1] = new char[this->launchArguments[i].size() + (pos - binaryPath.c_str())];
                    strncpy(argv[i + 1], binaryPath.c_str(), pos - binaryPath.c_str());
                    strncpy(argv[i + 1] + (pos - binaryPath.c_str()), this->launchArguments[i].c_str() + 1, this->launchArguments[i].size());
                } else {
                    argv[i + 1] = new char[this->launchArguments[i].size()+1];
                    strncpy(argv[i + 1], this->launchArguments[i].c_str(), this->launchArguments[i].size()+1);
                }
            }
            argv[this->launchArguments.size()+1] = nullptr;
            for (auto &envString : this->launchEnvironments) {
                putenv((char*)envString.c_str());
            }
            execvp(binaryPath.c_str(), argv);
            exit(0);
        } else { /* Parent process closes up output side of pipe */
            close(this->fdin[0]);
            close(this->fdout[1]);
            if ((this->to = fdopen(this->fdin[1], "w")) == nullptr) {
                kill(childpid, SIGKILL);
                return 0;
            }
            if ((this->from = fdopen(this->fdout[0], "r")) == nullptr) {
                fclose(to);
                kill(childpid, SIGKILL);
                return 0;
            }
            this->childpid = childpid;
            return true;
        }
    }

    void base::readUntil(const char *terminator) {
        size_t thesize = strlen(terminator);
        char *c;
        for (;;) {
            c = fgets(this->fbuf, ANSWERBUFSIZE, this->from);
            if (!c) {
                printf("EOF or file error when communicating with library\n");
                abort();
            }
            //cout <<"?"<< c <<"?"<< endl;
            while ((*c != '\0') && (*c <= ' ')) ++c;
            if (strncmp(terminator, c, thesize) == 0) {
                //cout << "out of readup " << endl; fflush(stdout);
                return;
            }
        }
    }

    char* base::readLine() {
        char *temp = fgets(this->fbuf, ANSWERBUFSIZE, this->from);
        if (!temp) {
            printf("EOF or file error when communicating with library\n");
            abort();
        }
        //cout<<"!"<<temp<<"!"<<endl;
        return temp;
    }

    void base::writeExpression(const char* str, bool flush) {
        if (fputs(str, this->to) == EOF) {
            cout << endl << endl << "Crash on fputs!" << endl << str << endl;
            abort();
        };
        if (flush) fflush(this->to);
    }

    void base::writeExpressionWithoutSpaces(const char* str, bool flush) {
        this->fullout = this->baseout;
        for (const char* pos = str; *pos!='\0'; ++pos) {
            if (*pos != ' ') this->addToOut(*pos);
        }
        this->addToOut('\0');
        if (fputs(this->baseout, this->to) == EOF) {
            cout << endl << endl << "Crash on fputs!" << endl << str << endl;
            abort();
        };
        if (flush) fflush(this->to);
        this->fullout = this->baseout;
    }

    void base::writeSymbol(const char c) {
        if (putc(c, this->to) != c) {
            cout << endl << endl << "Crash on putc!" << endl << c << endl;
            abort();
        }
    }

    pid_t base::initialize(const string& binaryPath, const vector<string>& vars, uint64_t prime) {
        this->baseout = static_cast<char*>(malloc(ANSWERBUFSIZE));
        this->fullout = this->baseout;
        this->stopout = this->baseout + ANSWERBUFSIZE;
        this->hasVariables = (vars.size() > 0);
        snprintf(this->primeBuf, sizeof(this->primeBuf), "%llu", static_cast<unsigned long long>(prime));
        if (binaryPath != "") {
            this->fbuf = static_cast<char*>(malloc(ANSWERBUFSIZE));
            if (!this->startProgram(binaryPath)) {
                std::cout << endl << "Could not initialize binary with path: " << binaryPath << endl;
                abort();
            }
        }
        initializeInternal(vars);
        return this->childpid;
    }

    void base::evaluate(string& s, bool modular = false) {
        this->fullout = this->baseout;
        const char* pos = s.c_str();
        string sc;
        if (*pos == '[' && !supportsBrackets) {
            sc = s + s;
            char* posc = sc.data();
            while (*pos != '\0') {
                if (*pos == '[') {
                    *posc = '(';
                    ++posc;
                    *posc = '(';
                    ++posc;
                } else
                if (*pos == ']') {
                    *posc = ')';
                    ++posc;
                    *posc = ')';
                    ++posc;
                } else
                if (*pos == ',') {
                    *posc = ')';
                    ++posc;
                    *posc = '/';
                    ++posc;
                    *posc = '(';
                    ++posc;
                } else {
                    *posc = *pos;
                    ++posc;
                }
                ++pos;
            }
            *posc = '\0';
            pos = sc.c_str();
        }
        this->evaluateInternal(pos, modular);
        this->addToOut('\0'); /*Complete the line*/
        if ((this->baseout[0] == '0') && (this->baseout[1] == '/')) {
            this->baseout[1] = '\0'; //strange case of library returning 0/...
        }
        s = this->baseout;
    }

    void base::switchToConventional() {
        // no default implementation
    }

    void base::setOption(const string& option) {
        // no default implementation
    }

    void base::expandBuffer(const size_t addedLen) {
        size_t currentLen = this->stopout - this->baseout;
        size_t newLen = currentLen;
        size_t neededLen = this->fullout - this->baseout + addedLen;
        while (newLen <= neededLen) {
            newLen *= 2;
        }
        const size_t fill = this->fullout - this->baseout;
        char *ptr = static_cast<char*>(std::realloc(this->baseout, newLen));
        if (ptr == nullptr) {
            std::cout << "Cannot reallocate in expandBuffer" << std::endl;
            abort();
        }
        this->fullout = ptr + fill;
        this->stopout = ptr + newLen;
        this->baseout = ptr;
    }

    bool base::hasEnoughSize(const size_t addedLen) {
        return this->fullout + addedLen < this->stopout;
    }

    /*The inline function places one char to the output buffer with possible
     expansion of the buffer:*/
    void base::addToOut(char ch) {
        if (!this->hasEnoughSize(1)) {
            this->expandBuffer(1);
        }
        *this->fullout++ = ch;
    }

}
