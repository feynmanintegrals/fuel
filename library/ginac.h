
/** @file ginac.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

#include <ginac/ginac.h>

namespace fuel {

    /**
     * @brief Communication with ginac: https://www.ginac.de/
     */
    class ginac : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;
            GiNaC::symbol* ginac_vars_array = nullptr; //!< variables as array of ginac symbolc
            GiNaC::lst ginac_vars = {}; //!< variables as ginac list

        public:
            ginac() {}

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<ginac>();
            }

            ~ginac();
    };

}
