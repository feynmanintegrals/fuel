#include "maple.h"

namespace fuel {

    void maple::initializeInternal(const vector<string>& vars) {
#ifndef FUEL_MULTI_THREADING
        this->writeExpression("kernelopts(numcpus=0):\n");
#endif
    }

    void maple::evaluateInternal(const char* expr, bool modular) {
        this->writeExpression("convert(normal(");
        this->writeExpression(expr);
        this->writeExpression(",expanded),string);");
        this->writeExpression("\n", true);

        char* c = this->readLine();
        while (*c == '\n') c = this->readLine();

        while (*c != '\n') {
            if (*c > ' ' && *c!='"' && *c!='\\') addToOut(*c);
            if (*c == '\\') c++;// there will be new line to be skipped
            c++;
            if (*c == '\0') c = this->readLine();
        }
    }

    maple::~maple() {
        if (this->childpid) {
            this->writeExpression("quit\n", true);
        }
    }
}
