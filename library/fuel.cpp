/**
 * @file fuel.cpp
 * @author Alexander Smirnov
 *
 * This file provides interfaces from FIRE to different algrbraic simplification programs
 */

#include "fuel.h"

#include <fstream>

namespace fuel {

    vector<std::unique_ptr<fuel::base>> externalLibraries = {}; //!< internal list of pointers to registered libraries

    bool savePids = false; //!< whether pids of libraries should be saved to a file

    /**
     *  Stores a mapping from library names to their binary paths.
     */
    map<string, string> libraryBinaries =
        {
            {"fermat", "/usr/local/bin/fer64"},
            {"math", "/usr/local/bin/math"},
            {"maple", "/usr/local/bin/maple"},
            {"pari", "/usr/bin/gp"},
        };

    /**
     * Simplify library name, that will be used
     */
    string currentLibrary = "fermat";

    void setLibrary(const string& library) {
        currentLibrary = library;
    }

    string getLibrary() {
        return currentLibrary;
    }

    void setLibraryPath(const string& library, const string& binaryPath) {
        auto itr = fuel::libraryBinaries.find(library);
        if (itr == fuel::libraryBinaries.end()) {
            fuel::libraryBinaries.emplace(library, binaryPath);
        } else {
            itr->second = binaryPath;
        }
    }

    bool readLibraryPathsFromFile(const string& filename) {
        std::ifstream file(filename); //file just has some sentences
        if (!file) {
            return false;
        }
        string line;
        while (getline(file, line)) {
            if (line[0] == '#') continue;
            size_t pos = line.find(":");
            if (pos != string::npos) {
                string begin = line.substr(0, pos);
                ++pos;
                while (line[pos] == ' ') ++pos;
                if (line[pos] != '\0') {
                    string end = line.substr(pos);
                    if (end[0] != '/' && filename.find("/") != string::npos) {
                        const char* posc = filename.c_str() + filename.size() - 1;
                        while (*posc != '/') --posc;
                        string path = string(filename.c_str(), posc - filename.c_str() + 1) + string(end);
                        if (path[0] != '/') {
                            path = string("./") + path;
                        }
                        fuel::setLibraryPath(begin, path);
                    } else {
                        fuel::setLibraryPath(begin, end);
                    }
                }
            }
        }
        return true;
    }


    bool initialize(const vector<string>& vars, int threads, bool silent, uint64_t prime) {
        if (!fuel::libraries::exists(fuel::currentLibrary)) {
            std::cout << "Calculation library " << fuel::currentLibrary << " is not registered"<< std::endl;
            return false;
        }
        if (prime != 0) {
            if (!fuel::libraries::supportsPrimes(fuel::currentLibrary)) {
                if (!silent) {
                    std::cout << "Library " << fuel::currentLibrary << " does not support modular arithmetics, falling back to default" << endl;
                }
                fuel::currentLibrary = "fermat";
            }
        }
        string libraryPath;
        if (!fuel::libraries::usedAsALibrary(fuel::currentLibrary)) {
            auto itr = fuel::libraryBinaries.find(fuel::currentLibrary);
            if (itr == fuel::libraryBinaries.end()) {
                std::cout << "Binary path for calculation library " << fuel::currentLibrary << " is not specified"<< std::endl;
                return false;
            }
            libraryPath = itr->second;
            if (savePids) {
                std::ofstream ofile("pids", std::ios::trunc);
            }
        }
        for (int thread_number = 0; thread_number != threads; ++thread_number) {
            unique_ptr<fuel::base> ext = fuel::libraries::create(fuel::currentLibrary);
            if (fuel::libraries::usedAsALibrary(fuel::currentLibrary)) {
                ext->initialize("", vars, prime);
                if (!silent) {
                    std::cout << "Initialized " << fuel::currentLibrary << " as a library" << std::endl;
                }
            } else {
                pid_t childpid = ext->initialize(libraryPath, vars, prime);
                if (savePids) {
                    std::ofstream ofile("pids", std::ios::app);
                    ofile << childpid << '\n';
                }
                if (!silent) {
                    std::cout << "Started " << fuel::currentLibrary << ". Pid: " << childpid << std::endl;
                }
            }
            externalLibraries.push_back(std::move(ext));
        }
        return true;
    }

    void simplify(string &expr, int thread_number, bool modular) {
        // cout << "Before: " << expr << endl;
        externalLibraries[thread_number]->evaluate(expr, modular);
        // cout << "After: " << expr <<endl;
    }

    void switchToConventional() {
        for (auto& library: externalLibraries) {
            library->switchToConventional();
        }
    }

    void setOption(const string& option) {
        for (auto& library: externalLibraries) {
            library->setOption(option);
        }
    }

    void close() {
        externalLibraries.clear();
    }

}
