/** @file ratfunc_flint.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */
#pragma once

#include "flint/fmpz.h"

#include <iostream>
#include <vector>
#include <string>


/**
 * flint fmpz_t wrapper class
 */
class TFmpzWrapper {
public:
    /**
     * constructor inits fmpz_t and sets it to 1
     */
    TFmpzWrapper() {
        fmpz_init(f);
        SetOne();
    }

    TFmpzWrapper(const TFmpzWrapper&) = delete;
    TFmpzWrapper(const TFmpzWrapper&&) = delete;
    TFmpzWrapper& operator=(const TFmpzWrapper&) = delete;
    TFmpzWrapper& operator=(const TFmpzWrapper&&) = delete;

    /**
     * destructor releases memory allocated for wrapped fmpz_t
     */
    ~TFmpzWrapper() {
        fmpz_clear(f);
    }

    /**
     * function returns std::string representation of fmpz_t
     * @return std::string representation of fmpz_t
     */
    const std::string GetStr() const {
        char *str = nullptr;
        str = fmpz_get_str(str, 10, f);
        std::string out = str;
        free(str);
        return out;
    }

    /**
     * function sets fmpz_t to number contained in null-terminated c-style string
     * @param begin is the begining of null-terminated c-style string
     */
    void SetStr(char *begin) {
        fmpz_set_str(f, begin, 10);
    }

    /**
     * function sets fmpz_t to 1
     */
    void SetOne() {
        fmpz_one(f);
    }

    /**
     * function sets fmpz_t to 0
     */
    void SetZero() {
        fmpz_zero(f);
    }

    /**
     * function sets fmpz_t f to -f
     */
    void Negate() {
        fmpz_neg(f, f);
    }

    /**
     * function returns true if fmpz_t is zero
     * @return true if fmpz_t is zero
     */
    bool IsZero() {
        return fmpz_is_zero(f);
    }

    /**
     * flint struct representing number with arbitrary length
     */
    fmpz_t f;
};

/**
 * class represents monome
 */
struct TMonome {
    /**
     * constructor sets vector of exponents to size varsCount
     * @param varsCount amount of variables monome has
     */
    TMonome(size_t varsCount) : exponents(varsCount) {}

    /**
     * flint fmpz_t wrapper
     */
    TFmpzWrapper coeff;      // Coefficient before variables

    /**
     * vector of exponents
     */
    std::vector<ulong> exponents; // Vector of exponents of variables
};
