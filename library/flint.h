/** @file flint.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "generic_parser.h"
#include "ratfunc_flint.h"
#include "mod_ratfunc_flint.h"
#include "base.h"
#include <sstream>

namespace fuel
{

    /**
     * @brief Communication with flint: https://github.com/flintlib/flint
     */
    class flint : public base
    {
    private:

        /**
         * Prints fmpz_mpoly_struct in decomposed representation
         * @param poly polynome to print in decomposed representation
         * @param ctx context
         */
        void printMonomes(fmpz_mpoly_struct *poly, fmpz_mpoly_ctx_t ctx);

        void initializeInternal(const vector<string> &vars) override;
        void evaluateInternal(const char *expr, bool modular) override;

        /**
         * Evaluates expression with the flint library in modular case
         * @param expr expression
         */
        void evaluateInternalModular(const char *expr);
        inline static bool called_cleanup = false; //!< To call master_cleanup only once on destructor

        generic_parser::parser<ratfunc_flint::rational_function> parser; //!< the internal parser for expressions
        generic_parser::parser<mod_ratfunc_flint::rational_function> parser_mod; //!< the internal parser for expressions
        bool conventional_output{false}; //!< whether we are returning conventional output
        bool store_expressions{false}; //!< whether in non-convential output we store expressions and return numbers instead of results
        bool factorize_denominators = false; //!< whether to factorize denominators
        bool decomposed_output{false}; //!< whether we are returning decomposed output

    public:
        flint() {}

        /**
         * Creates an instance of this class
         * @return smart pointer
         */
        static std::unique_ptr<base> make()
        {
            return std::make_unique<flint>();
        }

        void setOption(const string& option) override;

        void switchToConventional() override;

        ~flint();
    };

}
