#include "mod_uni_ratfunc_flint.h"
#include <mutex>
#include <cassert>
#include <vector>

namespace fuel::mod_uni_ratfunc_flint
{
    void MyDeleter::operator()(char *p)
    {
        free(p);
    }

    std::mutex ctx_wrapper_init_mutex; //!< For initialization of `ctx_wrapper` by different threads

    void wrapper_mod_mpoly_ctx_t::initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime)
    {
        size_t nvars = 1;
        if (initialized)
            nmod_mpoly_ctx_clear(ctx);
        nmod_mpoly_ctx_init(ctx, nvars, ORD_LEX, prime);
        nmod_init(&flint_prime, prime);

        this->prime = prime;
        vars_vector.resize(nvars);
        vars_string_vector.resize(nvars);
        for (size_t i = 0; i < nvars; ++i)
        {
            vars_string_vector[i] = vars1[i];
            vars_vector[i] = (char *)vars_string_vector[i].c_str();
        }
        vars = &vars_vector[0];

        initialized = true; // ctx and vars have been initialized
    }

    wrapper_mod_mpoly_ctx_t::~wrapper_mod_mpoly_ctx_t()
    {
        if (initialized)
        {
            nmod_mpoly_ctx_clear(ctx);
        }
    }

    std::vector<std::pair<std::string, rational_function>> rational_function::variable_mapping;

    wrapper_mod_mpoly_ctx_t rational_function::ctx_wrapper;

    void rational_function::initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime)
    {
        std::lock_guard<std::mutex> guard(ctx_wrapper_init_mutex);
        ctx_wrapper.initialize_vars(vars1, prime);
        variable_mapping.clear();
        for (auto &varstring : vars1)
        {
            rational_function value = rational_function::parse_poly_to_ratfunc(varstring);
            auto mapping_rule = std::make_pair(varstring, value);
            variable_mapping.push_back(mapping_rule);
        }
    }

    rational_function::rational_function()
    {
        nmod_poly_init(&numerator, ctx_wrapper.prime);
        nmod_poly_init(&denominator, ctx_wrapper.prime);
        alive = true;
    }

    rational_function::rational_function(const rational_function &another)
    {
        nmod_poly_init(&numerator, ctx_wrapper.prime);
        nmod_poly_init(&denominator, ctx_wrapper.prime);
        nmod_poly_set(&numerator, &(another.numerator));
        nmod_poly_set(&denominator, &(another.denominator));
        alive = true;
    }

    rational_function &rational_function::operator=(const rational_function &another)
    {
        if (!alive)
        {
            nmod_poly_init(&numerator, ctx_wrapper.prime);
            nmod_poly_init(&denominator, ctx_wrapper.prime);
        }
        nmod_poly_set(&numerator, &(another.numerator));
        nmod_poly_set(&denominator, &(another.denominator));
        alive = true;
        return *this;
    }

    rational_function::rational_function(rational_function &&another) noexcept
    {
        if (another.alive) {
            nmod_poly_swap(&numerator, &another.numerator);
            nmod_poly_swap(&denominator, &another.denominator);
            if (alive) {
                nmod_poly_clear(&another.numerator);
                nmod_poly_clear(&another.denominator);
            }
            another.alive = false;
        }
        alive = true;
    }

    rational_function &rational_function::operator=(rational_function &&another) noexcept
    {
        if (another.alive) {
            nmod_poly_swap(&numerator, &another.numerator);
            nmod_poly_swap(&denominator, &another.denominator);
            if (alive) {
                nmod_poly_clear(&another.numerator);
                nmod_poly_clear(&another.denominator);
            }
            another.alive = false;
        }
        alive = true;
        return *this;
    }

    rational_function::~rational_function()
    {
        if (alive)
        {
            nmod_poly_clear(&numerator);
            nmod_poly_clear(&denominator);
        }
    }

    std::string rational_function::to_string() const
    {
        if (nmod_poly_is_zero(&numerator))
        {
            return "0";
        }
        char *numstring = nmod_poly_get_str_pretty(&numerator, ctx_wrapper.vars[0]);
        char *denstring = nmod_poly_get_str_pretty(&denominator, ctx_wrapper.vars[0]);
        std::string result = "(";
        result += numstring;
        result += ")/(";
        result += denstring;
        result += ")";
        free(numstring);
        free(denstring);
        return result;
    }

    std::pair<char *, char *> rational_function::get_num_den_strings() const
    {
        char *numstring = nmod_poly_get_str_pretty(&numerator, ctx_wrapper.vars[0]);
        char *denstring = nmod_poly_get_str_pretty(&denominator, ctx_wrapper.vars[0]);
        return std::make_pair(numstring, denstring);
    }

    std::unique_ptr<char[], MyDeleter> rational_function::num_string() const
    {
        char *numstring = nmod_poly_get_str_pretty(&numerator, ctx_wrapper.vars[0]);
        std::unique_ptr<char[], MyDeleter> result(numstring);
        return result;
    }

    std::unique_ptr<char[], MyDeleter> rational_function::den_string() const
    {
        char *denstring = nmod_poly_get_str_pretty(&denominator, ctx_wrapper.vars[0]);
        std::unique_ptr<char[], MyDeleter> result(denstring);
        return result;
    }

    rational_function::rational_function(mp_limb_t i)
    {
        nmod_poly_init(&numerator, ctx_wrapper.prime);
        nmod_poly_init(&denominator, ctx_wrapper.prime);
        alive = true;
        nmod_poly_set_coeff_ui(&numerator, 0, i);
        nmod_poly_set_coeff_ui(&denominator, 0, 1);
    }

    rational_function::rational_function(const char *numer_str)
    {
        nmod_poly_init(&numerator, ctx_wrapper.prime);
        nmod_poly_init(&denominator, ctx_wrapper.prime);
        alive = true;

        nmod_mpoly_struct temp;
        nmod_mpoly_init(&temp, ctx_wrapper.ctx);
        nmod_mpoly_set_str_pretty(&temp, numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        nmod_mpoly_get_nmod_poly(&numerator, &temp, 0, ctx_wrapper.ctx);
        nmod_mpoly_clear(&temp, ctx_wrapper.ctx);
        nmod_poly_set_coeff_ui(&denominator, 0, 1);
    }

    rational_function::rational_function(bool is_slow, const char *numer_str, const char *denom_str, bool canonicalize) {
        // unimplemented
    }

    rational_function::rational_function(const char *numer_str, const char *denom_str, bool canonicalize) 
    {
        nmod_poly_init(&numerator, ctx_wrapper.prime);
        nmod_poly_init(&denominator, ctx_wrapper.prime);
        alive = true;

        nmod_mpoly_struct temp;
        nmod_mpoly_init(&temp, ctx_wrapper.ctx);
        nmod_mpoly_set_str_pretty(&temp, numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        nmod_mpoly_get_nmod_poly(&numerator, &temp, 0, ctx_wrapper.ctx);
        nmod_mpoly_set_str_pretty(&temp, denom_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        nmod_mpoly_get_nmod_poly(&denominator, &temp, 0, ctx_wrapper.ctx);
        nmod_mpoly_clear(&temp, ctx_wrapper.ctx);
        

        if (canonicalize)
        {
            nmod_poly_struct gd;
            nmod_poly_init(&gd, ctx_wrapper.prime);
            nmod_poly_gcd(&gd, &numerator, &denominator);
            nmod_poly_div(&numerator, &numerator, &gd);
            nmod_poly_div(&denominator, &denominator, &gd);
            nmod_poly_clear(&gd);
            cancel_trivial_denominator();
        }
    }

    rational_function rational_function::parse_poly_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        return rational_function(s.c_str());
    }

    rational_function rational_function::parse_int_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        return rational_function(s.c_str());

        // The version below is faster but only works for small enough integers to be parsed, e.g. integers that were already reduced modulo the prime
        // mp_limb_t intresult = std::stoul(s);
        // rational_function result(intresult);
        // return result;
    }

    void rational_function::cancel_trivial_denominator()
    {
        if (nmod_poly_degree(&denominator) == 0 && !nmod_poly_is_one(&denominator))
        {
            mp_limb_t denom = nmod_poly_get_coeff_ui(&denominator, 0);
            denom = nmod_inv(denom, ctx_wrapper.flint_prime);
            nmod_poly_scalar_mul_nmod(&numerator, &numerator, denom);
            nmod_poly_set_coeff_ui(&denominator, 0, 1);
        }
    }

    rational_function rational_function::operator+(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_poly_struct &d1 = this->denominator;
        const nmod_poly_struct &d2 = a.denominator;
        const nmod_poly_struct &n1 = this->numerator;
        const nmod_poly_struct &n2 = a.numerator;

        // case 1
        if (nmod_poly_equal(&d1, &d2))
        {
            nmod_poly_add(&rnum, &n1, &n2);
            if (nmod_poly_is_one(&d1))
                nmod_poly_set(&rden, &d1);
            else
            {
                nmod_poly_struct gd;
                nmod_poly_init(&gd, ctx_wrapper.prime);
                nmod_poly_gcd(&gd, &rnum, &d1);

                if (nmod_poly_is_one(&gd))
                    nmod_poly_set(&rden, &d1);
                else
                {
                    nmod_poly_div(&rnum, &rnum, &gd);
                    nmod_poly_div(&rden, &d1, &gd);
                }

                nmod_poly_clear(&gd);
            }
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_poly_is_one(&d1))
        {
            nmod_poly_struct product;
            nmod_poly_init(&product, ctx_wrapper.prime);
            nmod_poly_mul(&product, &n1, &d2);
            nmod_poly_add(&rnum, &product, &n2);
            nmod_poly_set(&rden, &d2);
            nmod_poly_clear(&product);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_poly_is_one(&d2))
        {
            nmod_poly_struct product;
            nmod_poly_init(&product, ctx_wrapper.prime);
            nmod_poly_mul(&product, &n2, &d1);
            nmod_poly_add(&rnum, &product, &n1);
            nmod_poly_set(&rden, &d1);
            nmod_poly_clear(&product);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_poly_struct gd, product1, product2;
        nmod_poly_init(&gd, ctx_wrapper.prime);
        nmod_poly_init(&product1, ctx_wrapper.prime);
        nmod_poly_init(&product2, ctx_wrapper.prime);

        nmod_poly_gcd(&gd, &d1, &d2);
        if (nmod_poly_is_one(&gd))
        {
            nmod_poly_mul(&product1, &n1, &d2);
            nmod_poly_mul(&product2, &n2, &d1);
            nmod_poly_add(&rnum, &product1, &product2);
            nmod_poly_mul(&rden, &d1, &d2);
        }
        else
        {
            nmod_poly_struct q1, q2, t;
            nmod_poly_init(&q1, ctx_wrapper.prime);
            nmod_poly_init(&q2, ctx_wrapper.prime);
            nmod_poly_init(&t, ctx_wrapper.prime);

            nmod_poly_div(&q1, &d1, &gd);
            nmod_poly_div(&q2, &d2, &gd);
            nmod_poly_mul(&product1, &q1, &n2);
            nmod_poly_mul(&product2, &q2, &n1);
            nmod_poly_add(&rnum, &product1, &product2);
            nmod_poly_gcd(&t, &rnum, &gd);
            if (nmod_poly_is_one(&t))
                nmod_poly_mul(&rden, &q2, &d1);
            else
            {
                nmod_poly_div(&rnum, &rnum, &t);
                nmod_poly_div(&gd, &d1, &t);
                nmod_poly_mul(&rden, &gd, &q2);
            }

            nmod_poly_clear(&q1);
            nmod_poly_clear(&q2);
            nmod_poly_clear(&t);

        }
        nmod_poly_clear(&gd);
        nmod_poly_clear(&product1);
        nmod_poly_clear(&product2);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator-(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_poly_struct &d1 = this->denominator;
        const nmod_poly_struct &d2 = a.denominator;
        const nmod_poly_struct &n1 = this->numerator;
        const nmod_poly_struct &n2 = a.numerator;

        // case 1
        if (nmod_poly_equal(&d1, &d2))
        {
            nmod_poly_sub(&rnum, &n1, &n2);
            if (nmod_poly_is_one(&d1))
                nmod_poly_set(&rden, &d1);
            else
            {
                nmod_poly_struct gd;
                nmod_poly_init(&gd, ctx_wrapper.prime);
                nmod_poly_gcd(&gd, &rnum, &d1);

                if (nmod_poly_is_one(&gd))
                    nmod_poly_set(&rden, &d1);
                else
                {
                    nmod_poly_div(&rnum, &rnum, &gd);
                    nmod_poly_div(&rden, &d1, &gd);
                }

                nmod_poly_clear(&gd);
            }
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_poly_is_one(&d1))
        {
            nmod_poly_struct product;
            nmod_poly_init(&product, ctx_wrapper.prime);
            nmod_poly_mul(&product, &n1, &d2);
            nmod_poly_sub(&rnum, &product, &n2);
            nmod_poly_set(&rden, &d2);
            nmod_poly_clear(&product);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_poly_is_one(&d2))
        {
            nmod_poly_struct product;
            nmod_poly_init(&product, ctx_wrapper.prime);
            nmod_poly_mul(&product, &n2, &d1);
            nmod_poly_sub(&rnum, &n1, &product);
            nmod_poly_set(&rden, &d1);
            nmod_poly_clear(&product);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_poly_struct gd, product1, product2;
        nmod_poly_init(&gd, ctx_wrapper.prime);
        nmod_poly_init(&product1, ctx_wrapper.prime);
        nmod_poly_init(&product2, ctx_wrapper.prime);

        nmod_poly_gcd(&gd, &d1, &d2);
        if (nmod_poly_is_one(&gd))
        {
            nmod_poly_mul(&product1, &n1, &d2);
            nmod_poly_mul(&product2, &n2, &d1);
            nmod_poly_sub(&rnum, &product1, &product2);
            nmod_poly_mul(&rden, &d1, &d2);
        }
        else
        {
            nmod_poly_struct q1, q2, t;
            nmod_poly_init(&q1, ctx_wrapper.prime);
            nmod_poly_init(&q2, ctx_wrapper.prime);
            nmod_poly_init(&t, ctx_wrapper.prime);

            nmod_poly_div(&q1, &d1, &gd);
            nmod_poly_div(&q2, &d2, &gd);
            nmod_poly_mul(&product1, &q1, &n2);
            nmod_poly_mul(&product2, &q2, &n1);
            nmod_poly_sub(&rnum, &product2, &product1);
            nmod_poly_gcd(&t, &rnum, &gd);
            if (nmod_poly_is_one(&t))
                nmod_poly_mul(&rden, &q2, &d1);
            else
            {
                nmod_poly_div(&rnum, &rnum, &t);
                nmod_poly_div(&gd, &d1, &t);
                nmod_poly_mul(&rden, &gd, &q2);
            }

            nmod_poly_clear(&q1);
            nmod_poly_clear(&q2);
            nmod_poly_clear(&t);

        }
        nmod_poly_clear(&gd);
        nmod_poly_clear(&product1);
        nmod_poly_clear(&product2);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator*(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_poly_struct &d1 = this->denominator;
        const nmod_poly_struct &d2 = a.denominator;
        const nmod_poly_struct &n1 = this->numerator;
        const nmod_poly_struct &n2 = a.numerator;

        // case 1
        if (nmod_poly_equal(&d1, &d2))
        {
            nmod_poly_mul(&rnum, &n1, &n2);
            nmod_poly_mul(&rden, &d1, &d2);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_poly_is_one(&d1))
        {
            nmod_poly_struct gd;
            nmod_poly_init(&gd, ctx_wrapper.prime);
            nmod_poly_gcd(&gd, &n1, &d2);
            if (nmod_poly_is_one(&gd))
            {
                nmod_poly_mul(&rnum, &n1, &n2);
                nmod_poly_set(&rden, &d2);
            }
            else
            {
                nmod_poly_struct q;
                nmod_poly_init(&q, ctx_wrapper.prime);
                nmod_poly_div(&q, &n1, &gd);
                nmod_poly_mul(&rnum, &q, &n2);
                nmod_poly_div(&rden, &d2, &gd);
                nmod_poly_clear(&q);
            }
            nmod_poly_clear(&gd);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_poly_is_one(&d2))
        {
            nmod_poly_struct gd;
            nmod_poly_init(&gd, ctx_wrapper.prime);
            nmod_poly_gcd(&gd, &n2, &d1);
            if (nmod_poly_is_one(&gd))
            {
                nmod_poly_mul(&rnum, &n2, &n1);
                nmod_poly_set(&rden, &d1);
            }
            else
            {
                nmod_poly_struct q;
                nmod_poly_init(&q, ctx_wrapper.prime);
                nmod_poly_div(&q, &n2, &gd);
                nmod_poly_mul(&rnum, &q, &n1);
                nmod_poly_div(&rden, &d1, &gd);
                nmod_poly_clear(&q);
            }
            nmod_poly_clear(&gd);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_poly_struct g1, g2, n1prime, n2prime, d1prime, d2prime;
        nmod_poly_init(&g1, ctx_wrapper.prime);
        nmod_poly_init(&g2, ctx_wrapper.prime);

        nmod_poly_init(&n1prime, ctx_wrapper.prime);
        nmod_poly_init(&n2prime, ctx_wrapper.prime);
        nmod_poly_init(&d1prime, ctx_wrapper.prime);
        nmod_poly_init(&d2prime, ctx_wrapper.prime);
        nmod_poly_set(&n1prime, &n1);
        nmod_poly_set(&n2prime, &n2);
        nmod_poly_set(&d1prime, &d1);
        nmod_poly_set(&d2prime, &d2);

        nmod_poly_gcd(&g1, &n1, &d2);
        nmod_poly_gcd(&g2, &n2, &d1);
        if (!nmod_poly_is_one(&g1))
        {
            nmod_poly_div(&n1prime, &n1, &g1);
            nmod_poly_div(&d2prime, &d2, &g1);
        }
        if (!nmod_poly_is_one(&g2))
        {
            nmod_poly_div(&n2prime, &n2, &g2);
            nmod_poly_div(&d1prime, &d1, &g2);
        }
        nmod_poly_mul(&rnum, &n1prime, &n2prime);
        nmod_poly_mul(&rden, &d1prime, &d2prime);

        nmod_poly_clear(&g1);
        nmod_poly_clear(&g2);
        nmod_poly_clear(&n1prime);
        nmod_poly_clear(&n2prime);
        nmod_poly_clear(&d1prime);
        nmod_poly_clear(&d2prime);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator/(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        // Same as code for * operator, except that we swap n2 and d2 in the few lines of code below
        const nmod_poly_struct &d1 = this->denominator;
        const nmod_poly_struct &n2 = a.denominator;
        const nmod_poly_struct &n1 = this->numerator;
        const nmod_poly_struct &d2 = a.numerator;

        // case 1
        if (nmod_poly_equal(&d1, &d2))
        {
            nmod_poly_mul(&rnum, &n1, &n2);
            nmod_poly_mul(&rden, &d1, &d2);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_poly_is_one(&d1))
        {
            nmod_poly_struct gd;
            nmod_poly_init(&gd, ctx_wrapper.prime);
            nmod_poly_gcd(&gd, &n1, &d2);
            if (nmod_poly_is_one(&gd))
            {
                nmod_poly_mul(&rnum, &n1, &n2);
                nmod_poly_set(&rden, &d2);
            }
            else
            {
                nmod_poly_struct q;
                nmod_poly_init(&q, ctx_wrapper.prime);
                nmod_poly_div(&q, &n1, &gd);
                nmod_poly_mul(&rnum, &q, &n2);
                nmod_poly_div(&rden, &d2, &gd);
                nmod_poly_clear(&q);
            }
            nmod_poly_clear(&gd);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_poly_is_one(&d2))
        {
            nmod_poly_struct gd;
            nmod_poly_init(&gd, ctx_wrapper.prime);
            nmod_poly_gcd(&gd, &n2, &d1);
            if (nmod_poly_is_one(&gd))
            {
                nmod_poly_mul(&rnum, &n2, &n1);
                nmod_poly_set(&rden, &d1);
            }
            else
            {
                nmod_poly_struct q;
                nmod_poly_init(&q, ctx_wrapper.prime);
                nmod_poly_div(&q, &n2, &gd);
                nmod_poly_mul(&rnum, &q, &n1);
                nmod_poly_div(&rden, &d1, &gd);
                nmod_poly_clear(&q);
            }
            nmod_poly_clear(&gd);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_poly_struct g1, g2, n1prime, n2prime, d1prime, d2prime;
        nmod_poly_init(&g1, ctx_wrapper.prime);
        nmod_poly_init(&g2, ctx_wrapper.prime);

        nmod_poly_init(&n1prime, ctx_wrapper.prime);
        nmod_poly_init(&n2prime, ctx_wrapper.prime);
        nmod_poly_init(&d1prime, ctx_wrapper.prime);
        nmod_poly_init(&d2prime, ctx_wrapper.prime);
        nmod_poly_set(&n1prime, &n1);
        nmod_poly_set(&n2prime, &n2);
        nmod_poly_set(&d1prime, &d1);
        nmod_poly_set(&d2prime, &d2);

        nmod_poly_gcd(&g1, &n1, &d2);
        nmod_poly_gcd(&g2, &n2, &d1);
        if (!nmod_poly_is_one(&g1))
        {
            nmod_poly_div(&n1prime, &n1, &g1);
            nmod_poly_div(&d2prime, &d2, &g1);
        }
        if (!nmod_poly_is_one(&g2))
        {
            nmod_poly_div(&n2prime, &n2, &g2);
            nmod_poly_div(&d1prime, &d1, &g2);
        }
        nmod_poly_mul(&rnum, &n1prime, &n2prime);
        nmod_poly_mul(&rden, &d1prime, &d2prime);

        nmod_poly_clear(&g1);
        nmod_poly_clear(&g2);
        nmod_poly_clear(&n1prime);
        nmod_poly_clear(&n2prime);
        nmod_poly_clear(&d1prime);
        nmod_poly_clear(&d2prime);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function &rational_function::operator+=(const rational_function &a)
    {
        auto result = (*this) + a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator-=(const rational_function &a)
    {
        auto result = (*this) - a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator*=(const rational_function &a)
    {
        auto result = (*this) * a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator/=(const rational_function &a)
    {
        auto result = (*this) / a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::neg()
    {
        nmod_poly_neg(&numerator, &numerator);
        return *this;
    }

    int rational_function::size()
    {
        return nmod_poly_length(&numerator) + nmod_poly_length(&denominator);
    }

    bool rational_function::operator==(const rational_function &a) const
    {
        if (nmod_poly_equal(&numerator, &(a.numerator)) && nmod_poly_equal(&denominator, &(a.denominator)))
            return true;
        else
        {
            bool result;
            nmod_poly_struct product1, product2;
            nmod_poly_init(&product1, ctx_wrapper.prime);
            nmod_poly_init(&product2, ctx_wrapper.prime);
            nmod_poly_mul(&product1, &numerator, &(a.denominator));
            nmod_poly_mul(&product2, &(a.numerator), &denominator);
            result = nmod_poly_equal(&product1, &product2);
            nmod_poly_clear(&product1);
            nmod_poly_clear(&product2);
            return result;
        }
    }

    std::ostream &operator<<(std::ostream &os, const rational_function &q)
    {
        os << q.to_string();
        return os;
    }

    rational_function pow(const rational_function &a, int b)
    {
        rational_function result;

        if (b > 0)
        {
            nmod_poly_pow(&(result.numerator), &(a.numerator), b);
            nmod_poly_pow(&(result.denominator), &(a.denominator), b);
        }
        else
        {
            nmod_poly_pow(&(result.denominator), &(a.numerator), -b);
            nmod_poly_pow(&(result.numerator), &(a.denominator), -b);
        }
        return result;
    }
}
