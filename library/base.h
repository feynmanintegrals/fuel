/** @file base.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <cstring>
#include <sys/wait.h>
#include <sys/stat.h>
#include <cerrno>
#include <csignal>
#include <memory>
#include <thread>
#include <string>
#include <limits>
#include <iostream>
#include <cctype>
#include <errno.h>
#include <string>
#include <vector>
#include <map>
#include <cstdio>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

namespace fuel {

    /**
     * @brief Base abstract library communication class
     */
    class base {
        private:
            FILE *to = nullptr; //!< pipe to write expressions to in case of pipe version
            FILE *from = nullptr; //!< pipe to read results from in case of pipe version
            char *baseout = nullptr; //!< pointer to the start of expression result to be returned to calling library
            char *fullout = nullptr; //!< pointer to the current end of the result buffer
            char *stopout = nullptr; //!< pointer to the final end of result buffer
            char *fbuf = nullptr; //!< pointer to blocks read from library
            static constexpr inline int ANSWERBUFSIZE = { 1024 }; //!< initial result buffer size

            /**
             * Increases buffer for resulting expressions has enough size to add more data
             * @param addedLen the size of data to be added
             */
            virtual void expandBuffer(const size_t addedLen);

            /**
             * Checks whether buffer for resulting expressions has enough size to add more data
             * @param addedLen the size of data to be added
             * @return whether it is enough
             */
            virtual bool hasEnoughSize(const size_t addedLen);

            /**
             * Forks, starts external program, fills to, from and childpid
             * @param binaryPath path to program
             * @return success
             */
            bool startProgram(const string& binaryPath);
        protected:
#if defined(DOXYGEN_DOCUMENTATION)
            /**
             * Factory pattern map to be able to call proper implementation constructor by name
             */
            inline static map<string, std::unique_ptr<base>> factory;
#else
            //the real definition is here, was changed for doxygen since it parses it incorrectly
            inline static map<string, std::unique_ptr<base>(*)()> factory;
#endif
            inline static map<string, bool> primeSupport; //!< Stores which libraries have support for modular arithmetics
            inline static map<string, bool> libraryUsage; //!< Stores which libraries are used as libraries, without calling external program
            char primeBuf[32]; //!< stores the string representation of the prime number for modular calculations
            bool supportsBrackets = false; //!<whether it supports the [,] syntax

            /**
             * Communicates with the external program, writing initial commands
             * Is impelemented in each library independently
             * @param vars variables to be used in library
             */
            virtual void initializeInternal(const vector<string>& vars) = 0;

            /**
             * Communicates with the external program, simplifying expression
             * Is impelemented in each library independently
             * @param expr The expression that is simplified and changed in-place
             * @param modular whether we should take it modular a prime number, that is set in advance
             */
            virtual void evaluateInternal(const char* expr, bool modular) = 0;

            /**
             * Reads next line from library evaluator
             * @return the pointer to next line
             */
            virtual char* readLine();

            /**
             * Writes expression to the library evaluator
             * @param str the expression
             * @param flush if true, forces the buffer to be flushed, so that the library receives data
             */
            virtual void writeExpression(const char* str, bool flush = false);

            /**
             * Writes expression to the library evaluator skipping spaces. Uses out buffer for intermediate storage
             * @param str the expression
             * @param flush if true, forces the buffer to be flushed, so that the library receives data
             */
            virtual void writeExpressionWithoutSpaces(const char* str, bool flush = false);

            /**
             * Writes symbol to the library evaluator
             * @param c the symbol
             */
            virtual void writeSymbol(const char c);

            /**
             * Reads untill a string starts with what needed
             * @param terminator the required starting expresson
             */
            virtual void readUntil(const char *terminator);

            /**
             * Puts symbol to output buffer being prepared
             * @param ch symbol that is added
             */
            virtual void addToOut(char ch);

            /**
             * Vector of arguments added to the library call. Should be filled in corresponding implementation constructor
             */
            vector<string> launchArguments;

            /**
             * Vector of environments used in the library call. Should be filled in corresponding implementation constructor
             */
            vector<string> launchEnvironments;
            pid_t childpid = 0; //!< Pid of the library process
            int fdin[2]; //!< Pipe descriptors for writing to the process
            int fdout[2]; //!< Pipe descriptors for reading from the process
            bool hasVariables; //!< Whether the current process was initialized with variables

            /**
             * Register a library. Is normally called from libraries.h
             * @param libraryName name of the library
             * @param fun instance creating functor
             * @param supportsPrimes whether this library will work with modular approach
             * @param usedAsALibrary whether simplifier is used as library (not primes)
             */
            static void registerLibrary(const string& libraryName, std::unique_ptr<base>(* fun)(), bool supportsPrimes, bool usedAsALibrary = false) {
                base::factory.emplace(libraryName, fun);
                base::primeSupport.emplace(libraryName, supportsPrimes);
                base::libraryUsage.emplace(libraryName, usedAsALibrary);
            }

            /**
             * Create an instance of evaluation library
             * @param libraryName name of the library coinciding with class name
             * @return the smart pointer to the library
             */
            static unique_ptr<base> create(const string& libraryName) {
                auto itr = base::factory.find(libraryName);
                if (itr != base::factory.end()) {
                    return base::factory[libraryName]();
                } else {
                    cout << "Library not registered: " << libraryName << endl;
                    abort();
                }
            }

        public:
            /**
             * Function that really starts the library, should be called after the constructor
             * @param binaryPath the path to executably library binary
             * @param vars array of variables in use
             * @param prime sets a prime number that can be used for modular calculations if requested
             * @return the process id of launched binary
             */
            virtual pid_t initialize(const string& binaryPath, const vector<string>& vars, uint64_t prime = 0);

            /**
             * Interface function for simplifying the expression
             * @param expr the expression to be simplified and replace. 
             * @param modular whether we should take it modular a prime number set in advance
             */
            virtual void evaluate(string& expr, bool modular);

            /**
             * If the library supports it, switches from internal output to conventional
             * Has an empty implementation for libraries that do not need an override
             */
            virtual void switchToConventional();

            /**
             * Passes an option to the library, if the library supports it
             * @param option the option
             */
            virtual void setOption(const string& option);

            base() {
                if (pipe(fdin) < 0) {
                    perror("pipe");
                    abort();
                }
                if (pipe(fdout) < 0) {
                    perror("pipe");
                    abort();
                }
            }

            virtual ~base() {
                if (this->baseout) {
                    free(this->baseout);
                    this->baseout = nullptr;
                }
                if (this->childpid) {
                    fflush(this->to);
                    fclose(this->to);
                    fclose(this->from);
                    free(this->fbuf);
                    if (waitpid(this->childpid, nullptr, WNOHANG) > 0) {
                        return;
                    }
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    if (waitpid(this->childpid, nullptr, WNOHANG) > 0) {
                        return;
                    }
                    kill(this->childpid, SIGINT);
                    std::this_thread::sleep_for(std::chrono::milliseconds(100));
                    if (waitpid(this->childpid, nullptr, WNOHANG) > 0) {
                        return;
                    }
                    kill(this->childpid, SIGKILL);
                    waitpid(this->childpid, nullptr, 0);
                }
            }
    };

}
