#include "generic_parser.h"
#include "ratfunc_flint.h"
#include "mod_ratfunc_flint.h"
#include "mod_uni_ratfunc_flint.h"
#include <cassert>

using generic_parser::parser, generic_parser::evaluator, generic_parser::operator_type;

/**
 * Test basic polynomial operations
 * @return 0 if the test is successful
*/
int test_poly()
{
    constexpr int nvars = 1;
    fmpz_mpoly_ctx_t ctx;
    fmpz_mpoly_ctx_init(ctx, nvars, ORD_LEX);
    char* var = (char*) "x";
    char* vars[1];
    vars[0] = var;

    fmpz_mpoly_t a, b, c, d, e;
    fmpz_mpoly_init(a, ctx);
    fmpz_mpoly_init(b, ctx);
    fmpz_mpoly_init(c, ctx);
    fmpz_mpoly_init(d, ctx);
    fmpz_mpoly_init(e, ctx);
    fmpz_mpoly_set_str_pretty(a, "-2", (const char**) vars, ctx); // set polynomial a = -2
    fmpz_mpoly_set_str_pretty(b, "1", (const char**) vars, ctx); // set polynomial b = 1
    fmpz_mpoly_mul(c, a, b, ctx); // set c = a * b
    fmpz_mpoly_gcd(d, a, b, ctx); // set d = gcd(a, b)
    fmpz_mpoly_div(e, a, b, ctx); // set e = a/b, assuming exact division

    char* a_string = fmpz_mpoly_get_str_pretty(a, (const char**) vars, ctx);
    char* b_string = fmpz_mpoly_get_str_pretty(b, (const char**) vars, ctx);
    char* c_string = fmpz_mpoly_get_str_pretty(c, (const char**) vars, ctx);
    char* d_string = fmpz_mpoly_get_str_pretty(d, (const char**) vars, ctx);
    char* e_string = fmpz_mpoly_get_str_pretty(e, (const char**) vars, ctx);
    std::cout << " a = " << a_string << std::endl;
    std::cout << " b = " << b_string << std::endl;
    std::cout << " a*b = " << c_string << std::endl;
    std::cout << " gcd(a,b) = " << d_string << std::endl;
    std::cout << " a/b = " << e_string << std::endl;
    free(a_string);
    free(b_string);
    free(c_string);
    free(d_string);
    free(e_string);

    fmpz_mpoly_clear(a, ctx);
    fmpz_mpoly_clear(b, ctx);
    fmpz_mpoly_clear(c, ctx);
    fmpz_mpoly_clear(d, ctx);
    fmpz_mpoly_clear(e, ctx);
    fmpz_mpoly_ctx_clear(ctx);

    return 0;
}

/**
 * Test the generic parser for double-precision numbers
 * @return 0 if the test is successful
*/
int test_double()
{
    parser<double> p1;
    assert(p1.eval_string("1/3^2 + 2/9") == 1.0/3);
    assert(p1.eval_string("1/3 + (4-2) /9 * 3") == 1.0);
    return 0;
}

/**
 * Test rational function arithmetic of our FLINT wrapper
 * @return 0 if the test is successful
*/
int test_ratfunc_flint()
{
    using fuel::ratfunc_flint::rational_function;

    std::vector<std::string> vars1{"x"};
    rational_function::initialize_vars(vars1);

    rational_function a("1-x^2", "(1-x)^2", true);
    rational_function a2("1+x", "1-x", true);
    assert(a == a2);
    assert(a == a2); // Test that the comparison operator doesn't have unwanted side effects, so will return the same result again.

    std::cout << "Test printing with a=(1-x^2)/(1-x)^2, simplified automatically, and a2=(1+x)/(1-x)" << std::endl;
    std::cout << "a = " << a << std::endl;
    std::cout << "a^2 = " << pow(a, static_cast<ulong>(2)) << std::endl;
    std::cout << "a2 = " << a2 << std::endl;
    std::cout << "Numerator of a2 = " << a2.num_string().get() << std::endl;
    std::cout << "Denominator of a2 = " << a2.den_string().get() << std::endl;

    // Test (1-x^2) / (1-x) == 1+x
    assert(rational_function("1-x^2", "(1-x)", true) == rational_function("1+x"));
    assert(rational_function("1-x^2") / rational_function("(1-x)") == rational_function("1+x"));

    // Test (1+x) + (2+x) == 3+x
    assert(rational_function("1+x") + rational_function("2+x") == rational_function("3+2*x"));

    // Test 1/x+x = (x^2+1)/x
    assert(rational_function("1", "x") + rational_function("x") == rational_function("x^2+1", "x", true));

    // Test 1/x-x = (1-x^2)/x
    assert(rational_function("1", "x") - rational_function("x") == rational_function("1-x^2", "x", true));

    // Test x*(1/x) = 1
    assert(rational_function("x") * rational_function("1", "x", true) == rational_function("1"));

    // Test -((1-x^2)/x) = (1-x^2) / (-x)
    assert(-rational_function("1-x^2", "x", true) == rational_function("1-x^2", "-x", true));

    // Test changing the variable list (must be done globally)
    std::vector<std::string> vars2{"x", "y"};
    rational_function::initialize_vars(vars2);
    rational_function b("y-y*x^2", "(1-x)^2", true);
    rational_function b2("y+x*y", "1-x", true);
    assert(b == b2);

    return 0;
}

/**
 * Test parsing
 * @return 0 if the test is successful
*/
int test_evalmath_flint()
{
    using fuel::ratfunc_flint::rational_function;
    using namespace std::string_literals;
    // std::vector<const char *> vars1{"x"};
    std::string s1 = "x";
    std::vector<std::string> vars1{s1};
    rational_function::initialize_vars(vars1);

    // test x/(1+x) + 1/(1+x) == 1
    evaluator<rational_function> e;
    e.insert_operand(rational_function("x", "1+x", true));
    e.insert_operator(operator_type::plus);
    e.insert_operand(rational_function("1", "1+x", true));
    assert(e.eval_final() == rational_function("1"));

    // test (x/(1+x))^2
    e.reset();
    e.insert_operand(rational_function("x", "1+x", true));
    e.insert_operator(operator_type::power);
    e.insert_exponent(2);
    assert(e.eval_final() == rational_function("x^2", "(1+x)^2", true));

    rational_function a_value = rational_function("x", "1+x", true);
    const std::vector<std::pair<std::string, rational_function>> varmap{std::make_pair("a"s, a_value)};
    parser p(varmap);
    assert(p.eval_string("1/137341512542542353542543252435257908709087^2") == rational_function("1", "137341512542542353542543252435257908709087^2", true));
    assert(p.eval_string("a^2") == rational_function("x^2", "(1+x)^2", true));
    assert(p.eval_string("a^2 / 4 + 1") == rational_function("x^2 + 4*(1+x)^2", "4*(1+x)^2", true));
    return 0;
}

/**
 * Test parsing math expression strings and performing rational function arithmetic
 * @return 0 if the test is successful
*/
int test_simplify_string()
{
    using fuel::ratfunc_flint::rational_function;
    std::vector<std::string> vars{"x", "y"};
    auto p = parser<rational_function>(vars);
    assert(p.eval_string("1/x + 1/y + 1") == p.eval_string("(x+y+x*y)/(x*y)"));
    std::cout << "1/x & 1/y & 1 = " << p.eval_string("1/x & 1/y & 1") << std::endl;
    assert(p.eval_string("1/x & 1/y & 1") == p.eval_string("(x+y+x*y)/(x*y)"));
    assert(p.eval_string("1/x & -1/y & 1") == p.eval_string("(-x+y+x*y)/(x*y)"));
    assert(p.eval_string("1/x & 1/y") == p.eval_string("(x+y)/(x*y)"));
    assert(p.eval_string("x^(-1) + y^(-1) + 1") == p.eval_string("(x+y+x*y)/(x*y)"));
    assert(p.eval_string("x^(-2) + 1") == p.eval_string("1 + 1/x^2"));
    assert(p.eval_string("(x+y)^(-3)") == p.eval_string("1/(x+y)^3"));
    assert(p.eval_string("[x+y^2]") == p.eval_string("x+y*y"));
    std::cout << "[x,y] evalutes to: " << p.eval_string("[x,y]") << std::endl;
    assert(p.eval_string("[x,y]") == p.eval_string("x/y"));
    assert(p.eval_string("[x,y]/y+1") == p.eval_string("x/y^2+1"));
    assert(p.eval_string("-[x,y]/y+1") == p.eval_string("-x/y^2+1"));
    assert(p.eval_string("1/([x+2*y^2,y])/y+1") == p.eval_string("1/(x+2*y*y)+1"));
    assert(p.eval_string("(x+y-3)^2 / (x-y+5)^2 + 389/4145") == p.eval_string("(2*(23515 - 10490*x + 2267*x^2 - 14380*y + 3756*x*y + 2267*y^2))/(4145*(5 + x - y)^2)")); // Test data generated by Mathematica

    return 0;
}

/**
 * Test kernel for parallel threads
 * @return 0 if the test is successful
*/
int test_parallel_kernel()
{
    using fuel::ratfunc_flint::rational_function;
    constexpr auto n_iter = 100; // Each parallel thread simplifies a string `n_iter` times

    std::vector<std::string> vars{"x", "y"};
    auto p = parser<rational_function>(vars);

    constexpr auto to_simplify = "(x+y-3)^2 / (x-y+5)^2 + 389/4145";
    constexpr auto simplified = "(2*(23515 - 10490*x + 2267*x^2 - 14380*y + 3756*x*y + 2267*y^2))/(4145*(5 + x - y)^2)";

    for (int i=0; i < n_iter; i++)
        assert(p.eval_string(to_simplify) == p.eval_string(simplified)); // Test data generated by Mathematica
    return 0;
}

/**
 * Test kernel for parallel threads, for modular rational functions
 * @return 0 if the test is successful
*/
int test_parallel_mod_kernel()
{
    using fuel::mod_ratfunc_flint::rational_function;
    constexpr int prime = 7666667;
    constexpr auto n_iter = 100; // Each parallel thread simplifies a string `n_iter` times
    std::vector<std::string> vars{"x", "y"};
    auto p = parser<rational_function>(vars, prime);

    constexpr auto to_simplify = "(x+y-3)^2 / (x-y+5)^2 + 389/4145";
    constexpr auto simplified = "(2*(23515 - 10490*x + 2267*x^2 - 14380*y + 3756*x*y + 2267*y^2))/(4145*(5 + x - y)^2)";

    for (int i=0; i < n_iter; i++)
        assert(p.eval_string(to_simplify) == p.eval_string(simplified)); // Test data generated by Mathematica
    return 0;
}

/**
 * Test submitter for parallel threads
 * @return 0 if the test is successful
*/
int test_parallel()
{
    constexpr int n_kernels = 128;
    #pragma omp parallel for
    for (int j=0; j < n_kernels; j++) {
        test_parallel_kernel();
        test_parallel_mod_kernel();
    }
    return 0;
}

/**
 * Test function for storing numbers
 * @tparam T parser type, which can be `parser<fuel::ratfunc_flint::rational_function>` or `parser<fuel::mod_ratfunc_flint::rational_function>`
 * @param p parser
 * @param input_string expression to be evaluated
 * @return the number for the evaluated expression
 */
template <typename T>
uint64_t eval_string_return_expression_label(T& p, std::string_view input_string)
{
    ++p.n_stored;
    p.stored_expressions[p.n_stored] = p.eval_string(input_string);
    return p.n_stored;
}

/**
 * Test storing expressions to be accessed by integer labels
 * @return 0 if the test is successful
*/
int test_storing()
{
    using fuel::ratfunc_flint::rational_function;
    std::vector<std::string> vars{"x", "y"};
    auto p = parser<rational_function>(vars);

    assert(eval_string_return_expression_label(p, "1/x") == 1);
    assert(eval_string_return_expression_label(p, "1/y") == 2);

    assert(p.eval_string("-{1}+{2}^2") == p.eval_string("-1/x + 1/y^2"));

    p.clear_stored_expressions();
    // Note that the counter is reset, and labels start from 1 again.
    assert(eval_string_return_expression_label(p, "x/y - 1") == 1);
    assert(eval_string_return_expression_label(p, "y/x + 1") == 2);

    assert(p.eval_string("{1} / {2}") == p.eval_string("(x/y-1) / (y/x+1)"));

    // The counter is NOT reset when you delete an individual expression
    p.delete_stored_expression(2);
    assert(eval_string_return_expression_label(p, "2*x") == 3);

    return 0;
}

/**
 * Test storing expressions to be accessed by integer labels
 * @return 0 if the test is successful
*/
int test_mod_storing()
{
    constexpr uint32_t prime = 7666667;
    using fuel::mod_ratfunc_flint::rational_function;
    std::vector<std::string> vars{"x", "y"};
    auto p = parser<rational_function>(vars, prime);

    assert(eval_string_return_expression_label(p, "1/x") == 1);
    assert(eval_string_return_expression_label(p, "1/y") == 2);

    assert(p.eval_string("-{1}+{2}^2") == p.eval_string("-1/x + 1/y^2"));

    p.clear_stored_expressions();
    // Note that the counter is reset, and labels start from 1 again.
    assert(eval_string_return_expression_label(p, "x/y - 1") == 1);
    assert(eval_string_return_expression_label(p, "y/x + 1") == 2);

    assert(p.eval_string("{1} / {2}") == p.eval_string("(x/y-1) / (y/x+1)"));

    // The counter is NOT reset when you delete an individual expression
    p.delete_stored_expression(2);
    assert(eval_string_return_expression_label(p, "2*x") == 3);

    return 0;
}

/**
 * Test rational function arithmetic modulo a prime number
 * @return 0 if the test is successful
*/
int test_mod_uni_ratfunc_flint()
{
    using namespace fuel::mod_uni_ratfunc_flint;

    // std::cout << "Testing rational functions over prime field with modulus 1009." << std::endl;

    std::vector<std::string> vars{"x"};
    mp_limb_t prime = 1009;
    rational_function::initialize_vars(vars, prime);

    rational_function a("x");
    rational_function b("x");
    rational_function c(1000);

    assert(c == rational_function("1000"));

    // modulo 1009
    assert(rational_function("-1") == rational_function("1008"));
    assert(rational_function("1009") == rational_function("0"));
    assert(rational_function("3") / rational_function("2") == rational_function("506"));

    // Since we've wrapped a C library, let's verify that memory management is handled correctly when working with C++ STL
    std::vector<rational_function> expressions;
    expressions.push_back(a);
    expressions.push_back(b);
    expressions.push_back(c);
    assert(expressions[0] == a);
    assert(expressions[1] == b);
    assert(expressions[2] == c);
    expressions.pop_back();
    expressions.emplace_back("x", "x", true);
    assert(expressions.back() == rational_function("x", "x", true));

    assert(rational_function("x^2+2*x*x+x^2", "x+x", true) == rational_function("x+x"));
    assert(rational_function("-x", "-x", true) == rational_function("x", "x", true));

    // overloaded + operator
    assert(rational_function("1", "x", true) + rational_function("1", "x", true) == rational_function("x+x", "x*x", true));
    assert(rational_function("1", "x", true) + rational_function("x-1", "x", true) == rational_function("1"));
    assert(rational_function("x-1", "x", true) + rational_function("1", "x", true) == rational_function("1"));
    assert(rational_function("x+1", "x*x", true) + rational_function("1", "x", true) == rational_function("x+x+1", "x*x", true));

    // overloaded - operator
    assert(rational_function("1", "x", true) - rational_function("1", "x", true) == rational_function("x-x", "x*x", true));
    assert(rational_function("1", "x", true) - rational_function("1-x", "x", true) == rational_function("1"));
    assert(rational_function("-1-x", "x", true) + rational_function("1", "x", true) == rational_function("-1"));
    assert(rational_function("x+1", "x*x", true) - rational_function("1", "x", true) == rational_function("x-x+1", "x*x", true));

    // overloaded * operator
    assert(rational_function("1", "x", true) * rational_function("1", "x", true) == rational_function("1", "x*x", true));
    assert(rational_function("2", "x*x", true) * rational_function("x+x", "x*x", true) == rational_function("2*x+2*x", "x^2*x^2", true));
    assert(rational_function("x") * rational_function("1") == rational_function("x"));
    assert(rational_function("1", "x", true) * rational_function("x", "x", true) == rational_function("1", "x", true));

    // overloaded / operator
    assert(rational_function("1", "x", true) / rational_function("1", "x", true) == rational_function("x", "x", true));
    assert(rational_function("2", "x*x", true) / rational_function("x+x", "x*x", true) == rational_function("2", "x+x", true));
    assert(rational_function("x") / rational_function("1") == rational_function("x"));
    assert(rational_function("1", "x", true) / rational_function("x", "x", true) == rational_function("x", "x^2", true));

    // pow() function
    assert(pow(rational_function("x+x"), 2) ==rational_function("x^2+2*x*x+x^2"));
    assert(pow(rational_function("(x+x)", "x", true), 2) == rational_function("x^2+2*x*x+x^2", "x^2", true));
    assert(pow(rational_function("(x+x)", "x", true), -2) == rational_function("x^2", "x^2+2*x*x+x^2", true));

    flint_cleanup_master();
    return 0;
}

/**
 * Test rational function arithmetic modulo a prime number
 * @return 0 if the test is successful
*/
int test_mod_ratfunc_flint()
{
    using namespace fuel::mod_ratfunc_flint;

    // std::cout << "Testing rational functions over prime field with modulus 1009." << std::endl;

    std::vector<std::string> vars{"x", "y"};
    mp_limb_t prime = 1009;
    rational_function::initialize_vars(vars, prime);

    rational_function a("x");
    rational_function b("y");
    rational_function c(1000);

    assert(c == rational_function("1000"));

    // modulo 1009
    assert(rational_function("-1") == rational_function("1008"));
    assert(rational_function("1009") == rational_function("0"));
    assert(rational_function("3") / rational_function("2") == rational_function("506"));

    // Since we've wrapped a C library, let's verify that memory management is handled correctly when working with C++ STL
    std::vector<rational_function> expressions;
    expressions.push_back(a);
    expressions.push_back(b);
    expressions.push_back(c);
    assert(expressions[0] == a);
    assert(expressions[1] == b);
    assert(expressions[2] == c);
    expressions.pop_back();
    expressions.emplace_back("x", "y", true);
    assert(expressions.back() == rational_function("x", "y", true));

    assert(rational_function("x^2+2*x*y+y^2", "x+y", true) == rational_function("x+y"));
    assert(rational_function("-x", "-y", true) == rational_function("x", "y", true));

    // overloaded + operator
    assert(rational_function("1", "x", true) + rational_function("1", "y", true) == rational_function("x+y", "x*y", true));
    assert(rational_function("1", "x", true) + rational_function("x-1", "x", true) == rational_function("1"));
    assert(rational_function("x-1", "x", true) + rational_function("1", "x", true) == rational_function("1"));
    assert(rational_function("x+1", "x*y", true) + rational_function("1", "x", true) == rational_function("x+y+1", "x*y", true));

    // overloaded - operator
    assert(rational_function("1", "x", true) - rational_function("1", "y", true) == rational_function("y-x", "x*y", true));
    assert(rational_function("1", "x", true) - rational_function("1-x", "x", true) == rational_function("1"));
    assert(rational_function("-1-x", "x", true) + rational_function("1", "x", true) == rational_function("-1"));
    assert(rational_function("x+1", "x*y", true) - rational_function("1", "x", true) == rational_function("x-y+1", "x*y", true));

    // overloaded * operator
    assert(rational_function("1", "x", true) * rational_function("1", "y", true) == rational_function("1", "x*y", true));
    assert(rational_function("2", "x*y", true) * rational_function("x+y", "x*y", true) == rational_function("2*x+2*y", "x^2*y^2", true));
    assert(rational_function("x") * rational_function("1") == rational_function("x"));
    assert(rational_function("1", "x", true) * rational_function("x", "y", true) == rational_function("1", "y", true));

    // overloaded / operator
    assert(rational_function("1", "x", true) / rational_function("1", "y", true) == rational_function("y", "x", true));
    assert(rational_function("2", "x*y", true) / rational_function("x+y", "x*y", true) == rational_function("2", "x+y", true));
    assert(rational_function("x") / rational_function("1") == rational_function("x"));
    assert(rational_function("1", "x", true) / rational_function("x", "y", true) == rational_function("y", "x^2", true));

    // pow() function
    assert(pow(rational_function("x+y"), 2) ==rational_function("x^2+2*x*y+y^2"));
    assert(pow(rational_function("(x+y)", "x", true), 2) == rational_function("x^2+2*x*y+y^2", "x^2", true));
    assert(pow(rational_function("(x+y)", "x", true), -2) == rational_function("x^2", "x^2+2*x*y+y^2", true));

    flint_cleanup_master();
    return 0;
}

/**
 * Test rational function arithmetic modulo a large prime number
 * @return 0 if the test is successful
*/
int test_mod_large_ratfunc_flint()
{
    using namespace fuel::mod_ratfunc_flint;

    // std::cout << "Testing rational functions over prime field with modulus 1009." << std::endl;

    std::vector<std::string> vars{"x", "y"};
    mp_limb_t prime = 18446744073709551557llu;
    rational_function::initialize_vars(vars, prime);
    assert(rational_function("1", "173267347", true) == rational_function("1", "173267347", true));

    auto p = parser<rational_function>(vars, prime);
    std::cout<< p.eval_string("-1") << std::endl;

    flint_cleanup_master();
    return 0;
}

/**
 * Run all tests: `test_ratfunc_flint()`, `test_evalmath_flint()`, `test_simplify_string()`.
 * @return 0 if the test is successful
*/
int main()
{
    // non-modular tests
    assert(test_poly() == 0);
    assert(test_double() == 0);
    assert(test_ratfunc_flint() == 0);
    assert(test_evalmath_flint() == 0);
    assert(test_simplify_string() == 0);
    assert(test_storing() == 0);

    // modular tests
    assert(test_mod_ratfunc_flint() == 0);
    assert(test_mod_uni_ratfunc_flint() == 0);
    assert(test_mod_large_ratfunc_flint() == 0);
    assert(test_mod_storing() == 0);

    // parallel tests (non-modular and modular)
    assert(test_parallel() == 0);

    flint_cleanup_master();
    return 0;
}
