#include "math.h"

namespace fuel {

    void math::initializeInternal(const vector<string>& vars) {
        this->readUntil("Copyright");
        this->writeExpression("$HistoryLength = 1;\n");
        this->writeExpression("SetOptions[$Output,PageWidth->Infinity];\n");
        this->writeExpression("2+2\n", true);
        this->readUntil("Out[3]= 4");
        this->readLine();
    }

    void math::evaluateInternal(const char* expr, bool modular) {
        this->writeExpression("InputForm[Together[");
        this->writeExpression(expr);
        this->writeExpression("]]\n", true);

        char* c = this->readLine();
        while (*c != 'O') c = this->readLine();//Out[3]= 4
        while (*c != '=') ++c;
        c += 2;
        while (*c == '\n') c = this->readLine();

        while (*c != '\n') {
            if (*c > ' ') addToOut(*c);
            c++;
            if (*c == '\0') c = this->readLine();
        }
    }

    math::~math() {
        if (this->childpid) {
            this->writeExpression("Quit\n", true);
        }
    }
}
