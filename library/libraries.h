/** @file libraries.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"
#include "fermat.h"
#include "pari.h"
#ifdef ENABLE_GINAC
#include "ginac.h"
#endif
#include "math.h"
#include "maple.h"
#ifdef ENABLE_SYMBOLICA
#include "symbolica.h"
#endif
#ifdef ENABLE_FLINT
#include "flint.h"
#endif

namespace fuel {

    /**
     * @brief Interface for using different algrbaic libraries
     */
    class libraries : public base {

        private:
            inline static bool initialized = false; //!< Flag that libraries were initialized to avoid repeated initialization

            /**
             * Initializes the librares. Should be called from each public function to be safe, next call changes nothing
             */
            static void init() {
                if (!initialized) {
                    fuel::base::registerLibrary("fermat", &fuel::fermat::make, true);
                    fuel::base::registerLibrary("pari", &fuel::pari::make, true);
#ifdef ENABLE_GINAC
                    fuel::base::registerLibrary("ginac", &fuel::ginac::make, true, true);
#endif
                    fuel::base::registerLibrary("math", &fuel::math::make, false);
                    fuel::base::registerLibrary("maple", &fuel::maple::make, false);
#ifdef ENABLE_SYMBOLICA
                    fuel::base::registerLibrary("symbolica", &fuel::symbolica::make, true, true);
#endif
#ifdef ENABLE_FLINT
                    fuel::base::registerLibrary("flint", &fuel::flint::make, true, true);
#endif
                    initialized = true;
                }
            }

        public:
            /**
             * Creates an instance of this class
             * @param libraryName the name of library to be created
             * @return smart pointer
             */
            static unique_ptr<base> create(const string& libraryName) {
                libraries::init();
                return base::create(libraryName);
            }

            /**
             * Check whether a library was registerd
             * @param libraryName the name
             * @return the check result
             */
            static bool exists(const string& libraryName) {
                libraries::init();
                auto itr = base::primeSupport.find(libraryName);
                if (itr != base::primeSupport.end()) {
                    return true;
                } else {
                    return false;
                }
            }

            /**
             * Check whether a library supports modular arithmetics
             * @param libraryName the name
             * @return the check result
             */
            static bool supportsPrimes(const string& libraryName) {
                libraries::init();
                auto itr = base::primeSupport.find(libraryName);
                if (itr != base::primeSupport.end()) {
                    return base::primeSupport[libraryName];
                } else {
                    return false;
                }
            }

            /**
             * Check whether a library is used as a real library, without program call
             * @param libraryName the name
             * @return the check result
             */
            static bool usedAsALibrary(const string& libraryName) {
                libraries::init();
                auto itr = base::libraryUsage.find(libraryName);
                if (itr != base::libraryUsage.end()) {
                    return base::libraryUsage[libraryName];
                } else {
                    return false;
                }
            }
    };

}


