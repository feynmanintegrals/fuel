
/** @file pari.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

namespace fuel {

    /**
     * @brief Communication with pari: https://pari.math.u-bordeaux.fr/git/pari.git
     */
    class pari : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;

        public:
            pari() {
                this->launchArguments.push_back("-f");
                this->launchArguments.push_back("-s");
                this->launchArguments.push_back("32000000");
            }

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<pari>();
            }

            ~pari();
    };

}
