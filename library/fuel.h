/**
 * @file fuel.h
 * @author Alexander Smirnov
 *
 * This file provides interfaces from FIRE to different algrbraic simplification programs
 *
 */

#pragma once

#include <cstdio>
#include <iostream>
#include <string>

#include "libraries.h"

/**
 * @brief Interface for starting and using different algebraic libraries
 */
namespace fuel {

    extern vector<std::unique_ptr<fuel::base>> externalLibraries;

    extern bool savePids;

    /**
     *  Stores a mapping from library names to their binary paths.
     */
    extern map<string, string> libraryBinaries;

    /**
     * Simplify library name, that will be used
     */
    extern string currentLibrary;

    /**
     * Set evaluation library to be used
     * @param library library name
     */
    void setLibrary(const string& library);

    /**
     * Get evaluation library in use
     * @return library name
     */
    string getLibrary();

    /**
     * Set binary path for library
     * @param library Library name
     * @param binaryPath The path to binary
     */
    void setLibraryPath(const string& library, const string& binaryPath);

    /**
     * Read library binary names from file
     * Format is a number of strings like
     * math: /usr/local/bin/math
     * @param filename Filename
     * @return whether could read without any error
     */
    bool readLibraryPathsFromFile(const string& filename);

    /**
     * Make some initializations and start Fermat.
     * @param vars vector of polynomial variables
     * @param threads number of worker processes to be started
     * @param silent whether messages should be supprssed
     * @param prime prime value in modular arithmetics, can be 0 or skipped for polynomials
     * @return whether could initialize
     */
    bool initialize(const vector<string>& vars, int threads, bool silent, uint64_t prime = 0);

    /**
     * Switches output to conventional for saving tables
     */
    void switchToConventional();

    /**
     * Passes an option to the library, might me library dependant
     * @param option the option
     */
    void setOption(const string& option);

    /**
     * Send job to worker thread.
     * @param expr the expression to be simplified
     * @param thread_number number of thread that'll receive the job.
     * @param modular whether the expression should be modular a preset prime number
     */
    void simplify(string &expr, int thread_number, bool modular = false);

    /**
     * Stop evaluation library and worker threads.
     */
    void close();

}
