/** @file ratfunc_flint.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */
#pragma once

#include "flint/nmod.h"
#include "flint/nmod_mpoly.h"
#include <vector>
#include <utility>
#include <memory>
#include <iostream>

namespace fuel::mod_uni_ratfunc_flint
{
        /**
         * Custom deleter for char* arrays allocated by C libraries, which need `free()` rather than `delete` for cleanup
         * @param p the array
         */
        struct MyDeleter
        {
                /**
                 * The operator to be called
                 * @param p the array
                 */
                void operator()(char *p);
        };

        /**
         * Wrapper struct storing the list of variables and the related FLINT context variable which essentially contains the same information
         */
        struct wrapper_mod_mpoly_ctx_t
        {
                /**
                 * FLINT context variable for multivariate polynomials and rational functions. We need it for the parser
                 */
                nmod_mpoly_ctx_t ctx;

                /**
                 * The prime number
                 */
                mp_limb_t prime;

                /**
                 * FLINT context for fast inversion
                 */
                nmod_t flint_prime;

                /**
                 * Whether initialization has previously taken place. If so, calling `initialize_vars()` will trigger deallocation of previous allocations first
                 */
                bool initialized{false};

                /**
                 * List of variable strings
                 */
                std::vector<std::string> vars_string_vector;

                /**
                 * List of variable strings as C strings
                 */
                std::vector<char *> vars_vector;

                /**
                 * List of variable strings in the `char**` type, required for interfacing with FLINT
                 */
                char **vars;

                /**
                 * Initialize the variable list and the related FLINT context
                 * @param vars1 vector of strings, representing the list of variables
                 * @param prime the prime number
                 */
                void initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime);

                /**
                 * Destructor which handles deallocation of FLINT context
                 */
                ~wrapper_mod_mpoly_ctx_t();
        };

        /**
         * C++ wrapper class for rational functions modulo a prime number, based on FLINT's `nmod_mpoly`, with overloaded arithmetic operators and IO functions
         */
        class rational_function
        {
        public:
                static std::vector<std::pair<std::string, rational_function>> variable_mapping; //!< correspondance of variable string and same variables as flint functions

                /**
                 * Static class member holding the context
                 */
                static wrapper_mod_mpoly_ctx_t ctx_wrapper;

                /**
                 * Set the list of variables for the multivariate rational functions
                 * @param vars1 vector of strings, representing the list of variables
                 * @param prime the prime number
                 */
                static void initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime);

                nmod_poly_struct numerator; //!< expression numerator
                nmod_poly_struct denominator; //!< expression denominator
                bool alive{false}; //!< whether expression was initialized

                /**
                 * Constructor which allocates memory but does not set the content
                 */
                rational_function();

                /**
                 * Copy constructor
                 * @param another rational function being copied
                 */
                rational_function(const rational_function &another);

                /**
                 * Copy assignment operator
                 * @param another rational function being copied
                 * @return the copy
                 */
                rational_function &operator=(const rational_function &another);

                /**
                 * Move constructor
                 * @param another rational function being moved
                 */
                rational_function(rational_function &&another) noexcept;

                /**
                 * Move assignment operator
                 * @param another rational function being moved
                 * @return the moved rational function
                 */
                rational_function &operator=(rational_function &&another) noexcept;

                /**
                 * Destructor which frees memory
                 */
                ~rational_function();

                /**
                 * Construct a rational function that is a constant finite-field element
                 * @param i integer pre-reduced by the prime modulus
                 */
                rational_function(mp_limb_t i);

                /**
                 * Construct a polynomial, i.e. rational function with trivial unit denominator
                 * @param numer_str numerator string
                 */
                rational_function(const char *numer_str);

                /**
                 * Unimplemented parsing constructor used for constructing polynomes from decomposed representation contained in numer_str for numer and denom_str for denom strings
                 * @param is_slow param used to distinguish decomposed representation constructor from others
                 * @param numer_str numerator string
                 * @param denom_str denominator string
                 * @param canonicalize whether we canonicalize the fraction
                 */
                rational_function(bool is_slow, const char *numer_str, const char *denom_str, bool canonicalize);

                /**
                 * Parsing constructor
                 * @param numer_str numerator string
                 * @param denom_str denominator string
                 * @param canonicalize whether we canonicalize the fraction
                 */
                rational_function(const char *numer_str, const char *denom_str, bool canonicalize);

                /**
                 * Convert the ratinonal function to a string
                 * @return the string representation
                 */
                std::string to_string() const;

                /**
                 * Print the rational function into two C strings, one for the numerator and the other for the denominator. The user is reponsible for freeing the memory allocated to the `char*` strings by calling `free()`
                 * @return the pair of C strings
                 */
                std::pair<char *, char *> get_num_den_strings() const;

                /**
                 * Convert the numerator of the rational function to a string
                 * @return A smart pointer that manages the `char*` string
                 */
                std::unique_ptr<char[], MyDeleter> num_string() const;

                /**
                 * Convert the denominator of the rational function to a string
                 * @return A smart pointer that manages the `char*` string
                 */
                std::unique_ptr<char[], MyDeleter> den_string() const;

                /**
                 * When the denominator is just a number, divide the numerator by that number and set the denominator to one
                 */
                void cancel_trivial_denominator();

                /**
                 * Add a rational function
                 * @param a rational function to be added
                 * @return result
                 */
                rational_function operator+(const rational_function &a);

                /**
                 * Substract a rational rational function
                 * @param a rational function to be substracted
                 * @return result
                 */
                rational_function operator-(const rational_function &a);

                /**
                 * Unary minus
                 * @return minus original rational function
                 */
                rational_function operator-();

                /**
                 * Multiply a rational function
                 * @param a rational function to be multiplied
                 * @return result
                 */
                rational_function operator*(const rational_function &a);

                /**
                 * Divide a rational function
                 * @param a rational function to be divided by
                 * @return result
                 */
                rational_function operator/(const rational_function &a);

                /**
                 * In-place addition of a rational function
                 * @param a rational function to be added
                 * @return result
                 */
                rational_function &operator+=(const rational_function &a);

                /**
                 * In-place substraction of a rational function
                 * @param a rational function to be substracted
                 * @return result
                 */
                rational_function &operator-=(const rational_function &a);

                /**
                 * In-place multiplication of a rational function
                 * @param a rational function to be multiplied
                 * @return result
                 */
                rational_function &operator*=(const rational_function &a);

                /**
                 * In-place division of a rational function
                 * @param a rational function to be divided by
                 * @return result
                 */
                rational_function &operator/=(const rational_function &a);

                /**
                 * Turn the rational function to (-1) times itself in-place
                 * @return the opposite rational function
                 */
                rational_function &neg();

                /**
                 * Check equality of a rational function
                 * @param a rational function to be checked
                 * @return result
                 */
                bool operator==(const rational_function &a) const;

                /**
                 * Size of the rational function, defined as the total number of monomial terms in the numerator polynomial and denominator polynomial
                 * @return the size
                 */
                int size();

                /**
                 * parse a polynomial string into a rational function
                 * @param a a polynomial string which cannot contain `/` characters
                 * @return rational function with unit denominator, i.e. actually a polynomial
                 */
                static rational_function parse_poly_to_ratfunc(std::string_view a);

                /**
                 * parse an integer string into a rational function
                 * @param a a string which must represent a valid integer
                 * @return rational function with unit denominator and integer numerator
                 */
                static rational_function parse_int_to_ratfunc(std::string_view a);
        };

        /**
         * Compute an integer power of a rational function
         * @param a the rational function
         * @param b the integer exponent
         * @return `a` raised to the power `b`
         */
        rational_function pow(const rational_function &a, int b);

        /**
         * Put rational function to an ostream
         * @param os stream
         * @param q rational function
         * @return same stream
         */
        std::ostream &operator<<(std::ostream &os, const rational_function &q);
}
