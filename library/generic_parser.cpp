#include "generic_parser.h"

namespace generic_parser
{
    /**
     * Return an integer indicating the precedence level of the operator
     * @param op operator
     * @return precedence
    */
    int operator_precedence(operator_type op)
    {
        switch (op)
        {
        case operator_type::toplevel_plus:
            return 0;
        case operator_type::plus:
            return 1;
        case operator_type::minus:
            return 1;
        case operator_type::mult:
            return 2;
        case operator_type::div:
            return 2;
        case operator_type::unary_minus:
            return 3;
        case operator_type::power:
            return 4;
        default:
            return 0; // won't be encountered in practice
        }
    }

    /**
     * Returns an unsigned long long integer from a string, calls std::stoull
     * @param s string
     * @return integer
     */
    int parse_unsigned_long_long_int(std::string_view s)
    {
        return std::stoull(static_cast<std::string>(s));
    }

    /**
     * Returns an integer from a string, calls std::stoi
     * @param s string
     * @return integer
     */
    int parseint(std::string_view s)
    {
        return std::stoi(static_cast<std::string>(s));
    }
}
