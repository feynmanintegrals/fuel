#pragma once

#include <vector>
#include <queue>
#include <string>
#include <string_view>
#include <unordered_map>
#include <cassert>
#include <mutex>
#include <cmath>

using std::size_t, std::pow;

constexpr int SMART_SUM_THRESHOLD = 1000; //!< Minimum length of expression string, above which the smart summation strategy (dynamical sorting) will be used for top-level sums

namespace generic_parser
{
    /**
     * Enumeration of operators that can appear in strings to be parsed
    */
    enum class operator_type
    {
        toplevel_plus,
        plus,
        minus,
        mult,
        div,
        unary_minus,
        power,
        left_parenthesis,
        right_parenthesis,
    };

    /**
     * Return an integer indicating the precedence level of the operator
     * @param op operator
     * @return precedence
    */
    int operator_precedence(operator_type op);

    /**
     * Returns an unsigned long long integer from a string, calls std::stoull
     * @param s string
     * @return integer
     */
    int parse_unsigned_long_long_int(std::string_view s);

    /**
     * Returns an integer from a string, calls std::stoi
     * @param s string
     * @return integer
     */
    int parseint(std::string_view s);

    /**
     * A class that holds a stack of operands and a stack of operators (binary operators and the unary minus operator). This class is part of the parser for mathematical expressions
     * @tparam T a rational function type
    */
    template <typename T>
    class evaluator
    {
        std::vector<T> expression_stack; //!< stack of operands (rational functions)
        std::vector<operator_type> operator_stack; //!< stack of operators (binary operators and the unary minus operator)

        /**
         * Pop the last operator in operator stack. If this operator is a binary operator, pop the last two operands in the operand stack and push the evaluation result back to the operand stack. If instead the unary minus operator is encountered, the last element of the operand stack is turned into its negative
        */
        void eval_one_step()
        {
            operator_type last_operator = operator_stack.back();
            operator_stack.pop_back();
            if (last_operator == operator_type::unary_minus)
            {
                if constexpr(std::is_arithmetic<T>::value)
                    expression_stack.back() = - expression_stack.back();
                else
                    expression_stack.back().neg();
            }
            else
            {
                T operand2 = expression_stack.back();
                expression_stack.pop_back();
                evaluate_binary_operator_inplace(last_operator, expression_stack.back(), operand2);
            }
        }

    public:
        /**
         * Push an L-value rational function to the end of the operand stack
         * @param a the rational function to be pushed
        */
        void insert_operand(T &a)
        {
            expression_stack.push_back(a);
        }

        /**
         * Push an R-value rational function to the end of the operand stack
         * @param a the rational function to be pushed
        */
        void insert_operand(T &&a)
        {
            expression_stack.push_back(a);
        }

        /**
         * Insert an integer exponent. This is only legal immediately after an exponent operator was inserted. The exponent operator will be popped from the operator stack, and the last element of the operand stack will be replaced by its `e`-th power
         * @param e the power to be inserted
        */
        void insert_exponent(int e)
        {
            operator_stack.pop_back();
            auto result = pow(expression_stack.back(), e);
            expression_stack.back() = result;
        }

        /**
         * Push an operator into the operator stack. This may trigger one or more steps of evaluations, depending on the precedence of the inserted operator compared with the existing operators held in the operator stack
         * @param op operator
        */
        void insert_operator(operator_type op)
        {
            if (op == operator_type::left_parenthesis)
            {
                operator_stack.push_back(op);
                return;
            }

            if (op == operator_type::toplevel_plus)
            {
                while (!operator_stack.empty() && (operator_stack.back() != operator_type::toplevel_plus))
                {
                    eval_one_step();
                }
                operator_stack.push_back(op);
                return;
            }

            if (op == operator_type::right_parenthesis)
            {
                while (!operator_stack.empty() && operator_stack.back() != operator_type::left_parenthesis)
                {
                    eval_one_step();
                }
                operator_stack.pop_back();
                return;
            }

            // now op is a non-bracket, non-&, operator
            while (!operator_stack.empty() && operator_stack.back() != operator_type::left_parenthesis && operator_stack.back() != operator_type::toplevel_plus && operator_precedence(operator_stack.back()) >= operator_precedence(op))
            {
                eval_one_step();
            }
            operator_stack.push_back(op);
        }

        /**
         * Evaluate all the operator / operands held
         * @return result
        */
        T eval_final()
        {
            while (!operator_stack.empty())
            {
                eval_one_step();
            }
            T result = expression_stack.back();
            expression_stack.pop_back();
            return result;
        }

        /**
         * Evaluate the top-level sum by a smart ordering, always adding the two smallest summands first, with a dynamic sorting accomplished by a `priority_queue`
         * @return result
        */
        T smart_sum()
        {
            // evaluate the last component of the top-level sum, before doing the summation. All other components have already been summed upon reading the next '&' character
            while (operator_stack.back() != operator_type::toplevel_plus)
            {
                eval_one_step();
            }

            std::priority_queue<std::pair<int, size_t>, std::vector<std::pair<int, size_t>>, std::greater<std::pair<int, size_t>>> to_sum; // each element is a pair (-size, position in operand stack)
            for (size_t i = 0; i < expression_stack.size(); ++i)
                to_sum.push(std::make_pair(expression_stack[i].size(), i));
            while (to_sum.size() > 1)
            {
                auto summand1 = to_sum.top();
                auto pos1 = summand1.second;
                to_sum.pop();
                auto summand2 = to_sum.top();
                auto pos2 = summand2.second;
                to_sum.pop();
                expression_stack[pos1] += expression_stack[pos2]; // we won't yet erase expression_stack[pos2] yet, even though it's removed from to_sum. we'll run expression_stack.empty() after this function is run.
                int newsize = expression_stack[pos1].size();
                to_sum.push(std::make_pair(newsize, pos1));
            }
            auto final_position = to_sum.top().second;
            return expression_stack[final_position];
        }

        /**
         * Empty the operand stack and operator stack, and reset other internal states of the class
        */
        void reset()
        {
            operator_stack.clear();
            expression_stack.clear();
        }
    };

    /**
     * Applies a proper binary operator to a pair of flint rational functions
     * @tparam a rational function type
     * @param op operator
     * @param a first rational function
     * @param b second rational function
     * @return result
     */
    template <typename T>
    T evaluate_binary_operator(operator_type op, T &a, T &b)
    {
        switch (op)
        {
        case operator_type::toplevel_plus:
            return a + b;
	    case operator_type::plus:
            return a + b;
        case operator_type::minus:
            return a - b;
        case operator_type::mult:
            return a * b;
        case operator_type::div:
            return a / b;
        // case operator_type::power:
        //     return pow(a, b);
        // Above doesn't work generally, since b may not be a numerical type. Implemented separately in insert_exponent().
        default:
            return a; // won't be used in practice
        }
    }

    /**
     * Applies a proper binary operator to a pair of flint rational functions inplace, i.e. changing the first function
     * tparam T a rational function type
     * @param op operator
     * @param a first rational function
     * @param b second rational function
     * @return result
     */
    template <typename T>
    T &evaluate_binary_operator_inplace(operator_type op, T &a, T &b)
    {
        switch (op)
        {
        case operator_type::plus:
            a += b;
            break;
        case operator_type::toplevel_plus:
            a += b;
            break;
        case operator_type::minus:
            a -= b;
            break;
        case operator_type::mult:
            a *= b;
            break;
        case operator_type::div:
            a /= b;
            break;
        default:
            std::abort();
        }
        return a;
    }

    /**
     * Class for parsing strings into rational functions
     @tparam T a rational function type
    */
    template <typename T>
    class parser
    {
    public:
        /**
         * Trivial constructor which does not do anything
        */
        parser() {}

        /**
         * Constructor which initializes a list of mapping from variable strings to their values
         * @param m the mapping
        */
        parser(std::vector<std::pair<std::string, T>> m) : variable_mapping{m} {}

        static inline std::mutex m = {}; //!< for proper initialization in multithreaded
        /**
         * Initialize the list of variables for a symbolic rational function type, and return a `parser` class instance with an initialized list of mapping rules from variable strings to actual variable values
         * @param vars vector of variable strings
         * @param modulus the prime number if nonzero
        */
        parser(const std::vector<std::string> &vars, uint64_t modulus = 0)
        {
            m.lock();
            T::initialize_vars(vars, modulus);
            variable_mapping = T::variable_mapping;
            m.unlock();
        }

        /**
         * Parse a string into a rational function
         * @param input_string string to be parsed
         * @return resulting rational function
        */
        T eval_string(std::string_view input_string)
        {
            e.reset();
            top_sum_operator_counter = 0;
            possible_unary_minus = true;
            expect_int_exponent = false;
            size_t pos{0};
            size_t substring_start{0};
            size_t substring_len{0};
            while (pos < input_string.length())
            {
                if (std::isspace(input_string[pos]))
                {
                    ++pos;
                    continue;
                }
                if (std::isalpha(input_string[pos]))
                {
                    substring_start = pos;
                    ++pos;
                    while ((pos < input_string.length()) && (std::isalnum(input_string[pos])))
                        ++pos;
                    substring_len = pos - substring_start;
                    push_variable_string(input_string.substr(substring_start, substring_len));
                    possible_unary_minus = false;
                    expect_int_exponent = false;
                    continue;
                }
                if (std::isdigit(input_string[pos]))
                {
                    substring_start = pos;
                    ++pos;
                    while ((pos < input_string.length()) && (std::isdigit(input_string[pos])))
                        ++pos;
                    substring_len = pos - substring_start;
                    push_integer_string(input_string.substr(substring_start, substring_len));
                    possible_unary_minus = false;
                    expect_int_exponent = false;
                    continue;
                }

                if (input_string[pos] == '<')
                {
                    ++pos; // skip '<'
                    ++pos; // skip '<'
                    substring_start = pos;
                    while (!(input_string[pos-1] == '>' && input_string[pos] == '.'))
                        ++pos;
                    substring_len = pos - substring_start - 1;
                    std::string numstring0 = static_cast<std::string>(input_string.substr(substring_start, substring_len));
                    const char *numstring = numstring0.c_str();
                    if (input_string[pos] == '.')
                    {
                        ++pos; // skip '<'
                        ++pos;
                        substring_start = pos;
                        while (!(input_string[pos-2] == '>' && input_string[pos-1] == '>' && input_string[pos] == '>'))
                            ++pos;
                        substring_len = pos - substring_start - 1;
                        std::string denstring0 = static_cast<std::string>(input_string.substr(substring_start, substring_len));
                        const char *denstring = denstring0.c_str();
                        if constexpr(std::is_arithmetic<T>::value) {
                            T numerator = static_cast<T>(std::stoi(static_cast<std::string>(numstring)));
                            T denominator = static_cast<T>(std::stoi(static_cast<std::string>(denstring)));
                            e.insert_operand(numerator / denominator);
                        } else {
                            auto constructed = T(true, numstring, denstring, false);
                            e.insert_operand(constructed);
                        }
                    }
                    ++pos; // skip '.' or '>'
                    possible_unary_minus = false;
                    expect_int_exponent = false;
                    continue;
                }

                if (input_string[pos] == '[')
                {
                    ++pos;
                    substring_start = pos;
                    while ((input_string[pos] != ',') && (input_string[pos] != ']'))
                        ++pos;
                    substring_len = pos - substring_start;
                    std::string numstring0 = static_cast<std::string>(input_string.substr(substring_start, substring_len));
                    const char *numstring = numstring0.c_str();
                    if (input_string[pos] == ',')
                    {
                        ++pos;
                        substring_start = pos;
                        while (input_string[pos] != ']')
                            ++pos;
                        substring_len = pos - substring_start;
                        std::string denstring0 = static_cast<std::string>(input_string.substr(substring_start, substring_len));
                        const char *denstring = denstring0.c_str();
                        if constexpr(std::is_arithmetic<T>::value)
                            {
                                T numerator = static_cast<T>(std::stoi(static_cast<std::string>(numstring)));
                                T denominator = static_cast<T>(std::stoi(static_cast<std::string>(denstring)));
                                e.insert_operand(numerator / denominator);
                            }
                        else
                            e.insert_operand(T(numstring, denstring, false));
                        // e.insert_operand(T(numstring) /= T(denstring));
                        // e.insert_operand(T(numstring));
                        // e.insert_operator(operator_type::div);
                        // e.insert_operand(T(denstring));
                    }
                    else // input_string[pos] == ']'
                    {
                        if constexpr(std::is_arithmetic<T>::value)
                            {
                                T numerator = static_cast<T>(std::stoi(static_cast<std::string>(numstring)));
                                e.insert_operand(numerator);
                            }
                        else
                            e.insert_operand(T(numstring));
                    }
                    ++pos; // skip ',' or ']'
                    possible_unary_minus = false;
                    expect_int_exponent = false;
                    continue;
                }

                if (input_string[pos] == '{')
                {
                    ++pos;
                    substring_start = pos;
                    while (input_string[pos] != '}')
                        ++pos;
                    substring_len = pos - substring_start;
                    uint64_t label = parse_unsigned_long_long_int(input_string.substr(substring_start, substring_len));
                    push_expression(stored_expressions[label]);
                    continue;
                }

                if ((input_string[pos] == '(') && (expect_int_exponent))
                {
                    ++pos;
                    assert(input_string[pos] == '-');
                    substring_start = pos;
                    while (input_string[pos] != ')')
                        ++pos;
                    substring_len = pos - substring_start;
                    push_integer_string(input_string.substr(substring_start, substring_len));
                    ++pos; // Skip the ')' character
                    possible_unary_minus = false;
                    continue;
                }

                // Now assume the input is valid and the current character is an operator
                char c = input_string[pos];
                push_operator(c);

                possible_unary_minus = (c == '(' || c == '&');
                expect_int_exponent = (c == '^');
                ++pos;
            }

            if ((pos <= SMART_SUM_THRESHOLD) || (top_sum_operator_counter <= 1)) // pos now hold the length of the input
            {
                return e.eval_final();
            }
            else
            {
                if constexpr(std::is_arithmetic<T>::value)
                    return e.eval_final();
                else
                    return e.smart_sum();
            }
        }

        /**
         * Delete the stored expression corresponding the `label`
         * @param label integer label for the stored expression
        */
        void delete_stored_expression(uint64_t label)
        {
            stored_expressions.erase(label);
        }

        /**
         * Delete the all stored expressions
        */
        void clear_stored_expressions()
        {
            stored_expressions.clear();
            n_stored = 0;
        }

        /**
         * Recognize an operator character, and push an appropriate `operator_type` representation of the operator to the evaluator. Depending on the context, `-` characters may be recognized as a unary minus operator or a binary minus operator
         * @param c operator
        */
        void push_operator(char c)
        {
            switch (c)
            {
            case '+':
                e.insert_operator(operator_type::plus);
                break;
            case '&':
                e.insert_operator(operator_type::toplevel_plus);
                ++top_sum_operator_counter;
                break;
            case '*':
                e.insert_operator(operator_type::mult);
                break;
            case '/':
                e.insert_operator(operator_type::div);
                break;
            case '(':
                e.insert_operator(operator_type::left_parenthesis);
                break;
            case ')':
                e.insert_operator(operator_type::right_parenthesis);
                break;
            case '^':
                e.insert_operator(operator_type::power);
                break;
            case '-':
                if (possible_unary_minus)
                    e.insert_operator(operator_type::unary_minus);
                else
                    e.insert_operator(operator_type::minus);
                break;
            }
        }

        /**
         * Parse an integer string. If the integer string appears after an exponential `^` operator, the string is parsed into the `int` type as an integer exponent. The exponent is then inserted to the evaluator. If the integer string appears in other contexts, the string is parsed into a rational function (actually holding an arbitrary-precision integer) and inserted to the evaluator
         * @param integer_string string of integers
        */
        void push_integer_string(std::string_view integer_string)
        {
            if (!expect_int_exponent)
            {
                T a;
                if constexpr(std::is_arithmetic<T>::value)
                    a = static_cast<T>(std::stoi(static_cast<std::string>(integer_string)));
                else
                    a = T::parse_int_to_ratfunc(integer_string);
                e.insert_operand(std::move(a));
            }
            else
            {
                int ex = parseint(integer_string);
                e.insert_exponent(ex);
            }
        }

        /**
         * Parse a string representing a variable and convert it into an expression using the mapping stored in `variable_mapping`. The expression is pushed to the evaluator
         * @param variable_string string containing a variable
        */
        void push_variable_string(std::string_view variable_string)
        {
            for (auto it = variable_mapping.begin(); it != variable_mapping.end(); ++it)
            {
                // std::cout << "it=>first == " << it->first << std::endl;
                if (variable_string == it->first)
                {
                    // std::cout << "matched!\n";
                    e.insert_operand(it->second);
                    return;
                }
            }
            std::abort();
        }

        /**
         * Push an expression to the evaluator; currently only used to parse labeled expressions like {label_number}
         * @param expression the expression to be pushed
        */
        void push_expression(T expression)
        {
            e.insert_operand(expression);
        }

        /**
         * Call `eval_final()` of the evaluator to finish off parsing
         * @return resulting expression
        */
        T eval()
        {
            return e.eval_final();
        }

    private:
        /**
         * List of rules mapping variable strings to their actual values
        */
        std::vector<std::pair<std::string, T>> variable_mapping;

    public:
        /**
         * Counter for the number of stored expressions
        */
        uint64_t n_stored {0};

        /**
         * A map storing expressions (as values) which can be accessed by their integer labels (as keys)
        */
        std::unordered_map<uint64_t, T> stored_expressions;

    private:
        /**
         * An `evaluator` class instance used for evaluation
        */
        evaluator<T> e;

        /**
         * An internal state of the parser. True at the beginning of expression, or immediately after a left bracket. Otherwise, a `-` operator cannot be a unary operator
        */
        bool possible_unary_minus{true};

        /**
         * An internal state of the parser. True immediately after the `^` operator. We require `^` to be followed by an interger, instead of a compound expression that evaluates to an integer
        */
        bool expect_int_exponent{false};

        /**
         * A counter that keeps track of the number of top-level summation operators, which affects whether a smart summation strategy will be used to evaluate the expression
        */
        int top_sum_operator_counter{0};
    };
}
