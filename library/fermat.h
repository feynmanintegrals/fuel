/** @file fermat.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

namespace fuel {

    /**
     * @brief Communication with fermat: http://home.bway.net/lewis/
     */
    class fermat : public base {
        private:
            void initializeInternal(const vector<string>& vars) override;
            void evaluateInternal(const char* expr, bool modular) override;
            
        public:
            fermat() {}
            
            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<fermat>();
            }

            ~fermat();
    };


}


