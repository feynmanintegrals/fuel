#include "mod_ratfunc_flint.h"
#include <mutex>
#include <cassert>
#include <vector>

namespace fuel::mod_ratfunc_flint
{
    void MyDeleter::operator()(char *p)
    {
        free(p);
    }

    std::mutex ctx_wrapper_init_mutex; //!< For initialization of `ctx_wrapper` by different threads

    void wrapper_nmod_mpoly_ctx_t::initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime)
    {
        size_t nvars = vars1.size();
        if (initialized)
            nmod_mpoly_ctx_clear(ctx);
        nmod_mpoly_ctx_init(ctx, nvars, ORD_LEX, prime);

        vars_vector.resize(nvars);
        vars_string_vector.resize(nvars);
        for (size_t i = 0; i < nvars; ++i)
        {
            vars_string_vector[i] = vars1[i];
            vars_vector[i] = (char *)vars_string_vector[i].c_str();
        }
        vars = &vars_vector[0];

        initialized = true; // ctx and vars have been initialized
    }

    wrapper_nmod_mpoly_ctx_t::~wrapper_nmod_mpoly_ctx_t()
    {
        if (initialized)
            nmod_mpoly_ctx_clear(ctx);
    }

    std::vector<std::pair<std::string, rational_function>> rational_function::variable_mapping;

    wrapper_nmod_mpoly_ctx_t rational_function::ctx_wrapper;

    void rational_function::initialize_vars(const std::vector<std::string> &vars1, mp_limb_t prime)
    {
        std::lock_guard<std::mutex> guard(ctx_wrapper_init_mutex);
        ctx_wrapper.initialize_vars(vars1, prime);
        variable_mapping.clear();
        for (auto &varstring : vars1)
        {
            rational_function value = rational_function::parse_poly_to_ratfunc(varstring);
            auto mapping_rule = std::make_pair(varstring, value);
            variable_mapping.push_back(mapping_rule);
        }
    }

    rational_function::rational_function()
    {
        nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
        nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        alive = true;
    }

    rational_function::rational_function(const rational_function &another)
    {
        nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
        nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        nmod_mpoly_set(&numerator, &(another.numerator), ctx_wrapper.ctx);
        nmod_mpoly_set(&denominator, &(another.denominator), ctx_wrapper.ctx);
        alive = true;
    }

    rational_function &rational_function::operator=(const rational_function &another)
    {
        if (!alive)
        {
            nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
            nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        }
        nmod_mpoly_set(&numerator, &(another.numerator), ctx_wrapper.ctx);
        nmod_mpoly_set(&denominator, &(another.denominator), ctx_wrapper.ctx);
        alive = true;
        return *this;
    }

    rational_function::rational_function(rational_function &&another) noexcept
    {
        if (another.alive) {
            nmod_mpoly_swap(&numerator, &another.numerator, ctx_wrapper.ctx);
            nmod_mpoly_swap(&denominator, &another.denominator, ctx_wrapper.ctx);
            if (alive) {
                nmod_mpoly_clear(&another.numerator, ctx_wrapper.ctx);
                nmod_mpoly_clear(&another.denominator, ctx_wrapper.ctx);
            }
            another.alive = false;
        }
        alive = true;
    }

    rational_function &rational_function::operator=(rational_function &&another) noexcept
    {
        if (another.alive) {
            nmod_mpoly_swap(&numerator, &another.numerator, ctx_wrapper.ctx);
            nmod_mpoly_swap(&denominator, &another.denominator, ctx_wrapper.ctx);
            if (alive) {
                nmod_mpoly_clear(&another.numerator, ctx_wrapper.ctx);
                nmod_mpoly_clear(&another.denominator, ctx_wrapper.ctx);
            }
            another.alive = false;
        }
        alive = true;
        return *this;
    }

    rational_function::~rational_function()
    {
        if (alive)
        {
            nmod_mpoly_clear(&numerator, ctx_wrapper.ctx);
            nmod_mpoly_clear(&denominator, ctx_wrapper.ctx);
        }
    }

    std::string rational_function::to_string() const
    {
        if (nmod_mpoly_is_zero(&numerator, ctx_wrapper.ctx))
        {
            return "0";
        }
        char *numstring = nmod_mpoly_get_str_pretty(&numerator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        char *denstring = nmod_mpoly_get_str_pretty(&denominator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        std::string result = "(";
        result += numstring;
        result += ")/(";
        result += denstring;
        result += ")";
        free(numstring);
        free(denstring);
        return result;
    }

    std::pair<char *, char *> rational_function::get_num_den_strings() const
    {
        char *numstring = nmod_mpoly_get_str_pretty(&numerator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        char *denstring = nmod_mpoly_get_str_pretty(&denominator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        return std::make_pair(numstring, denstring);
    }

    std::unique_ptr<char[], MyDeleter> rational_function::num_string() const
    {
        char *numstring = nmod_mpoly_get_str_pretty(&numerator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        std::unique_ptr<char[], MyDeleter> result(numstring);
        return result;
    }

    std::unique_ptr<char[], MyDeleter> rational_function::den_string() const
    {
        char *denstring = nmod_mpoly_get_str_pretty(&denominator, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        std::unique_ptr<char[], MyDeleter> result(denstring);
        return result;
    }

    rational_function::rational_function(mp_limb_t i)
    {
        nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
        nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        alive = true;
        nmod_mpoly_set_ui(&numerator, i, ctx_wrapper.ctx);
        nmod_mpoly_one(&denominator, ctx_wrapper.ctx);
    }

    rational_function::rational_function(const char *numer_str)
    {
        nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
        nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        alive = true;

        nmod_mpoly_set_str_pretty(&numerator, numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        nmod_mpoly_one(&denominator, ctx_wrapper.ctx);
    }

    rational_function::rational_function(bool is_slow, const char *numer_str, const char *denom_str, bool canonicalize) {
        // unimplemented
    }

    rational_function::rational_function(const char *numer_str, const char *denom_str, bool canonicalize)
    {
        nmod_mpoly_init(&numerator, ctx_wrapper.ctx);
        nmod_mpoly_init(&denominator, ctx_wrapper.ctx);
        alive = true;

        nmod_mpoly_set_str_pretty(&numerator, numer_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);
        nmod_mpoly_set_str_pretty(&denominator, denom_str, (const char **)ctx_wrapper.vars, ctx_wrapper.ctx);

        if (canonicalize)
        {
            nmod_mpoly_struct gd;
            nmod_mpoly_init(&gd, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&gd, &numerator, &denominator, ctx_wrapper.ctx);
            nmod_mpoly_divides(&numerator, &numerator, &gd, ctx_wrapper.ctx);
            nmod_mpoly_divides(&denominator, &denominator, &gd, ctx_wrapper.ctx);
            nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            cancel_trivial_denominator();
        }
    }

    rational_function rational_function::parse_poly_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        return rational_function(s.c_str());
    }

    rational_function rational_function::parse_int_to_ratfunc(std::string_view a)
    {
        auto s = static_cast<std::string>(a);
        return rational_function(s.c_str());

        // The version below is faster but only works for small enough integers to be parsed, e.g. integers that were already reduced modulo the prime
        // mp_limb_t intresult = std::stoul(s);
        // rational_function result(intresult);
        // return result;
    }

    void rational_function::cancel_trivial_denominator()
    {
        if (nmod_mpoly_is_ui(&denominator, ctx_wrapper.ctx) && !nmod_mpoly_is_one(&denominator, ctx_wrapper.ctx))
        {
            nmod_mpoly_divides(&numerator, &numerator, &denominator, ctx_wrapper.ctx);
            nmod_mpoly_one(&denominator, ctx_wrapper.ctx);
        }
    }

    rational_function rational_function::operator+(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_mpoly_struct &d1 = this->denominator;
        const nmod_mpoly_struct &d2 = a.denominator;
        const nmod_mpoly_struct &n1 = this->numerator;
        const nmod_mpoly_struct &n2 = a.numerator;

        // case 1
        if (nmod_mpoly_equal(&d1, &d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_add(&rnum, &n1, &n2, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
                nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            else
            {
                nmod_mpoly_struct gd;
                nmod_mpoly_init(&gd, ctx_wrapper.ctx);
                nmod_mpoly_gcd(&gd, &rnum, &d1, ctx_wrapper.ctx);

                if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
                    nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
                else
                {
                    nmod_mpoly_divides(&rnum, &rnum, &gd, ctx_wrapper.ctx);
                    nmod_mpoly_divides(&rden, &d1, &gd, ctx_wrapper.ctx);
                }

                nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            }
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct product;
            nmod_mpoly_init(&product, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product, &n1, &d2, ctx_wrapper.ctx);
            nmod_mpoly_add(&rnum, &product, &n2, ctx_wrapper.ctx);
            nmod_mpoly_set(&rden, &d2, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_mpoly_is_one(&d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct product;
            nmod_mpoly_init(&product, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product, &n2, &d1, ctx_wrapper.ctx);
            nmod_mpoly_add(&rnum, &product, &n1, ctx_wrapper.ctx);
            nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_mpoly_struct gd, product1, product2;
        nmod_mpoly_init(&gd, ctx_wrapper.ctx);
        nmod_mpoly_init(&product1, ctx_wrapper.ctx);
        nmod_mpoly_init(&product2, ctx_wrapper.ctx);

        nmod_mpoly_gcd(&gd, &d1, &d2, ctx_wrapper.ctx);
        if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
        {
            nmod_mpoly_mul(&product1, &n1, &d2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product2, &n2, &d1, ctx_wrapper.ctx);
            nmod_mpoly_add(&rnum, &product1, &product2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&rden, &d1, &d2, ctx_wrapper.ctx);
        }
        else
        {
            nmod_mpoly_struct q1, q2, t;
            nmod_mpoly_init(&q1, ctx_wrapper.ctx);
            nmod_mpoly_init(&q2, ctx_wrapper.ctx);
            nmod_mpoly_init(&t, ctx_wrapper.ctx);

            nmod_mpoly_divides(&q1, &d1, &gd, ctx_wrapper.ctx);
            nmod_mpoly_divides(&q2, &d2, &gd, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product1, &q1, &n2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product2, &q2, &n1, ctx_wrapper.ctx);
            nmod_mpoly_add(&rnum, &product1, &product2, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&t, &rnum, &gd, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&t, ctx_wrapper.ctx))
                nmod_mpoly_mul(&rden, &q2, &d1, ctx_wrapper.ctx);
            else
            {
                nmod_mpoly_divides(&rnum, &rnum, &t, ctx_wrapper.ctx);
                nmod_mpoly_divides(&gd, &d1, &t, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rden, &gd, &q2, ctx_wrapper.ctx);
            }

            nmod_mpoly_clear(&q1, ctx_wrapper.ctx);
            nmod_mpoly_clear(&q2, ctx_wrapper.ctx);
            nmod_mpoly_clear(&t, ctx_wrapper.ctx);

        }
        nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
        nmod_mpoly_clear(&product1, ctx_wrapper.ctx);
        nmod_mpoly_clear(&product2, ctx_wrapper.ctx);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator-(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_mpoly_struct &d1 = this->denominator;
        const nmod_mpoly_struct &d2 = a.denominator;
        const nmod_mpoly_struct &n1 = this->numerator;
        const nmod_mpoly_struct &n2 = a.numerator;

        // case 1
        if (nmod_mpoly_equal(&d1, &d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_sub(&rnum, &n1, &n2, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
                nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            else
            {
                nmod_mpoly_struct gd;
                nmod_mpoly_init(&gd, ctx_wrapper.ctx);
                nmod_mpoly_gcd(&gd, &rnum, &d1, ctx_wrapper.ctx);

                if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
                    nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
                else
                {
                    nmod_mpoly_divides(&rnum, &rnum, &gd, ctx_wrapper.ctx);
                    nmod_mpoly_divides(&rden, &d1, &gd, ctx_wrapper.ctx);
                }

                nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            }
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct product;
            nmod_mpoly_init(&product, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product, &n1, &d2, ctx_wrapper.ctx);
            nmod_mpoly_sub(&rnum, &product, &n2, ctx_wrapper.ctx);
            nmod_mpoly_set(&rden, &d2, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_mpoly_is_one(&d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct product;
            nmod_mpoly_init(&product, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product, &n2, &d1, ctx_wrapper.ctx);
            nmod_mpoly_sub(&rnum, &n1, &product, ctx_wrapper.ctx);
            nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_mpoly_struct gd, product1, product2;
        nmod_mpoly_init(&gd, ctx_wrapper.ctx);
        nmod_mpoly_init(&product1, ctx_wrapper.ctx);
        nmod_mpoly_init(&product2, ctx_wrapper.ctx);

        nmod_mpoly_gcd(&gd, &d1, &d2, ctx_wrapper.ctx);
        if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
        {
            nmod_mpoly_mul(&product1, &n1, &d2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product2, &n2, &d1, ctx_wrapper.ctx);
            nmod_mpoly_sub(&rnum, &product1, &product2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&rden, &d1, &d2, ctx_wrapper.ctx);
        }
        else
        {
            nmod_mpoly_struct q1, q2, t;
            nmod_mpoly_init(&q1, ctx_wrapper.ctx);
            nmod_mpoly_init(&q2, ctx_wrapper.ctx);
            nmod_mpoly_init(&t, ctx_wrapper.ctx);

            nmod_mpoly_divides(&q1, &d1, &gd, ctx_wrapper.ctx);
            nmod_mpoly_divides(&q2, &d2, &gd, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product1, &q1, &n2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product2, &q2, &n1, ctx_wrapper.ctx);
            nmod_mpoly_sub(&rnum, &product2, &product1, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&t, &rnum, &gd, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&t, ctx_wrapper.ctx))
                nmod_mpoly_mul(&rden, &q2, &d1, ctx_wrapper.ctx);
            else
            {
                nmod_mpoly_divides(&rnum, &rnum, &t, ctx_wrapper.ctx);
                nmod_mpoly_divides(&gd, &d1, &t, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rden, &gd, &q2, ctx_wrapper.ctx);
            }

            nmod_mpoly_clear(&q1, ctx_wrapper.ctx);
            nmod_mpoly_clear(&q2, ctx_wrapper.ctx);
            nmod_mpoly_clear(&t, ctx_wrapper.ctx);

        }
        nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
        nmod_mpoly_clear(&product1, ctx_wrapper.ctx);
        nmod_mpoly_clear(&product2, ctx_wrapper.ctx);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator*(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        const nmod_mpoly_struct &d1 = this->denominator;
        const nmod_mpoly_struct &d2 = a.denominator;
        const nmod_mpoly_struct &n1 = this->numerator;
        const nmod_mpoly_struct &n2 = a.numerator;

        // case 1
        if (nmod_mpoly_equal(&d1, &d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_mul(&rnum, &n1, &n2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&rden, &d1, &d2, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct gd;
            nmod_mpoly_init(&gd, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&gd, &n1, &d2, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
            {
                nmod_mpoly_mul(&rnum, &n1, &n2, ctx_wrapper.ctx);
                nmod_mpoly_set(&rden, &d2, ctx_wrapper.ctx);
            }
            else
            {
                nmod_mpoly_struct q;
                nmod_mpoly_init(&q, ctx_wrapper.ctx);
                nmod_mpoly_divides(&q, &n1, &gd, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rnum, &q, &n2, ctx_wrapper.ctx);
                nmod_mpoly_div(&rden, &d2, &gd, ctx_wrapper.ctx);
                nmod_mpoly_clear(&q, ctx_wrapper.ctx);
            }
            nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_mpoly_is_one(&d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct gd;
            nmod_mpoly_init(&gd, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&gd, &n2, &d1, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
            {
                nmod_mpoly_mul(&rnum, &n2, &n1, ctx_wrapper.ctx);
                nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            }
            else
            {
                nmod_mpoly_struct q;
                nmod_mpoly_init(&q, ctx_wrapper.ctx);
                nmod_mpoly_divides(&q, &n2, &gd, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rnum, &q, &n1, ctx_wrapper.ctx);
                nmod_mpoly_div(&rden, &d1, &gd, ctx_wrapper.ctx);
                nmod_mpoly_clear(&q, ctx_wrapper.ctx);
            }
            nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_mpoly_struct g1, g2, n1prime, n2prime, d1prime, d2prime;
        nmod_mpoly_init(&g1, ctx_wrapper.ctx);
        nmod_mpoly_init(&g2, ctx_wrapper.ctx);

        nmod_mpoly_init(&n1prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&n2prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&d1prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&d2prime, ctx_wrapper.ctx);
        nmod_mpoly_set(&n1prime, &n1, ctx_wrapper.ctx);
        nmod_mpoly_set(&n2prime, &n2, ctx_wrapper.ctx);
        nmod_mpoly_set(&d1prime, &d1, ctx_wrapper.ctx);
        nmod_mpoly_set(&d2prime, &d2, ctx_wrapper.ctx);

        nmod_mpoly_gcd(&g1, &n1, &d2, ctx_wrapper.ctx);
        nmod_mpoly_gcd(&g2, &n2, &d1, ctx_wrapper.ctx);
        if (!nmod_mpoly_is_one(&g1, ctx_wrapper.ctx))
        {
            nmod_mpoly_divides(&n1prime, &n1, &g1, ctx_wrapper.ctx);
            nmod_mpoly_divides(&d2prime, &d2, &g1, ctx_wrapper.ctx);
        }
        if (!nmod_mpoly_is_one(&g2, ctx_wrapper.ctx))
        {
            nmod_mpoly_divides(&n2prime, &n2, &g2, ctx_wrapper.ctx);
            nmod_mpoly_divides(&d1prime, &d1, &g2, ctx_wrapper.ctx);
        }
        nmod_mpoly_mul(&rnum, &n1prime, &n2prime, ctx_wrapper.ctx);
        nmod_mpoly_mul(&rden, &d1prime, &d2prime, ctx_wrapper.ctx);

        nmod_mpoly_clear(&g1, ctx_wrapper.ctx);
        nmod_mpoly_clear(&g2, ctx_wrapper.ctx);
        nmod_mpoly_clear(&n1prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&n2prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&d1prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&d2prime, ctx_wrapper.ctx);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function rational_function::operator/(const rational_function &a)
    {
        rational_function result;
        auto &rnum = result.numerator;
        auto &rden = result.denominator;

        // Same as code for * operator, except that we swap n2 and d2 in the few lines of code below
        const nmod_mpoly_struct &d1 = this->denominator;
        const nmod_mpoly_struct &n2 = a.denominator;
        const nmod_mpoly_struct &n1 = this->numerator;
        const nmod_mpoly_struct &d2 = a.numerator;

        // case 1
        if (nmod_mpoly_equal(&d1, &d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_mul(&rnum, &n1, &n2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&rden, &d1, &d2, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 2
        if (nmod_mpoly_is_one(&d1, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct gd;
            nmod_mpoly_init(&gd, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&gd, &n1, &d2, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
            {
                nmod_mpoly_mul(&rnum, &n1, &n2, ctx_wrapper.ctx);
                nmod_mpoly_set(&rden, &d2, ctx_wrapper.ctx);
            }
            else
            {
                nmod_mpoly_struct q;
                nmod_mpoly_init(&q, ctx_wrapper.ctx);
                nmod_mpoly_divides(&q, &n1, &gd, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rnum, &q, &n2, ctx_wrapper.ctx);
                nmod_mpoly_div(&rden, &d2, &gd, ctx_wrapper.ctx);
                nmod_mpoly_clear(&q, ctx_wrapper.ctx);
            }
            nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 3
        if (nmod_mpoly_is_one(&d2, ctx_wrapper.ctx))
        {
            nmod_mpoly_struct gd;
            nmod_mpoly_init(&gd, ctx_wrapper.ctx);
            nmod_mpoly_gcd(&gd, &n2, &d1, ctx_wrapper.ctx);
            if (nmod_mpoly_is_one(&gd, ctx_wrapper.ctx))
            {
                nmod_mpoly_mul(&rnum, &n2, &n1, ctx_wrapper.ctx);
                nmod_mpoly_set(&rden, &d1, ctx_wrapper.ctx);
            }
            else
            {
                nmod_mpoly_struct q;
                nmod_mpoly_init(&q, ctx_wrapper.ctx);
                nmod_mpoly_divides(&q, &n2, &gd, ctx_wrapper.ctx);
                nmod_mpoly_mul(&rnum, &q, &n1, ctx_wrapper.ctx);
                nmod_mpoly_div(&rden, &d1, &gd, ctx_wrapper.ctx);
                nmod_mpoly_clear(&q, ctx_wrapper.ctx);
            }
            nmod_mpoly_clear(&gd, ctx_wrapper.ctx);
            result.cancel_trivial_denominator();
            return result;
        }

        // case 4
        nmod_mpoly_struct g1, g2, n1prime, n2prime, d1prime, d2prime;
        nmod_mpoly_init(&g1, ctx_wrapper.ctx);
        nmod_mpoly_init(&g2, ctx_wrapper.ctx);

        nmod_mpoly_init(&n1prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&n2prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&d1prime, ctx_wrapper.ctx);
        nmod_mpoly_init(&d2prime, ctx_wrapper.ctx);
        nmod_mpoly_set(&n1prime, &n1, ctx_wrapper.ctx);
        nmod_mpoly_set(&n2prime, &n2, ctx_wrapper.ctx);
        nmod_mpoly_set(&d1prime, &d1, ctx_wrapper.ctx);
        nmod_mpoly_set(&d2prime, &d2, ctx_wrapper.ctx);

        nmod_mpoly_gcd(&g1, &n1, &d2, ctx_wrapper.ctx);
        nmod_mpoly_gcd(&g2, &n2, &d1, ctx_wrapper.ctx);
        if (!nmod_mpoly_is_one(&g1, ctx_wrapper.ctx))
        {
            nmod_mpoly_divides(&n1prime, &n1, &g1, ctx_wrapper.ctx);
            nmod_mpoly_divides(&d2prime, &d2, &g1, ctx_wrapper.ctx);
        }
        if (!nmod_mpoly_is_one(&g2, ctx_wrapper.ctx))
        {
            nmod_mpoly_divides(&n2prime, &n2, &g2, ctx_wrapper.ctx);
            nmod_mpoly_divides(&d1prime, &d1, &g2, ctx_wrapper.ctx);
        }
        nmod_mpoly_mul(&rnum, &n1prime, &n2prime, ctx_wrapper.ctx);
        nmod_mpoly_mul(&rden, &d1prime, &d2prime, ctx_wrapper.ctx);

        nmod_mpoly_clear(&g1, ctx_wrapper.ctx);
        nmod_mpoly_clear(&g2, ctx_wrapper.ctx);
        nmod_mpoly_clear(&n1prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&n2prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&d1prime, ctx_wrapper.ctx);
        nmod_mpoly_clear(&d2prime, ctx_wrapper.ctx);

        result.cancel_trivial_denominator();
        return result;
    }

    rational_function &rational_function::operator+=(const rational_function &a)
    {
        auto result = (*this) + a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator-=(const rational_function &a)
    {
        auto result = (*this) - a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator*=(const rational_function &a)
    {
        auto result = (*this) * a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::operator/=(const rational_function &a)
    {
        auto result = (*this) / a;
        (*this) = result;
        return *this;
    }

    rational_function &rational_function::neg()
    {
        nmod_mpoly_neg(&numerator, &numerator, ctx_wrapper.ctx);
        return *this;
    }

    int rational_function::size()
    {
        return nmod_mpoly_length(&numerator, ctx_wrapper.ctx) + nmod_mpoly_length(&denominator, ctx_wrapper.ctx);
    }

    bool rational_function::operator==(const rational_function &a) const
    {
        if (nmod_mpoly_equal(&numerator, &(a.numerator), ctx_wrapper.ctx) && nmod_mpoly_equal(&denominator, &(a.denominator), ctx_wrapper.ctx))
            return true;
        else
        {
            bool result;
            nmod_mpoly_struct product1, product2;
            nmod_mpoly_init(&product1, ctx_wrapper.ctx);
            nmod_mpoly_init(&product2, ctx_wrapper.ctx);
            nmod_mpoly_mul(&product1, &numerator, &(a.denominator), ctx_wrapper.ctx);
            nmod_mpoly_mul(&product2, &(a.numerator), &denominator, ctx_wrapper.ctx);
            result = nmod_mpoly_equal(&product1, &product2, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product1, ctx_wrapper.ctx);
            nmod_mpoly_clear(&product2, ctx_wrapper.ctx);
            return result;
        }
    }

    std::ostream &operator<<(std::ostream &os, const rational_function &q)
    {
        os << q.to_string();
        return os;
    }

    rational_function pow(const rational_function &a, int b)
    {
        rational_function result;

        if (b > 0)
        {
            nmod_mpoly_pow_ui(&(result.numerator), &(a.numerator), b, rational_function::ctx_wrapper.ctx);
            nmod_mpoly_pow_ui(&(result.denominator), &(a.denominator), b, rational_function::ctx_wrapper.ctx);
        }
        else
        {
            nmod_mpoly_pow_ui(&(result.denominator), &(a.numerator), -b, rational_function::ctx_wrapper.ctx);
            nmod_mpoly_pow_ui(&(result.numerator), &(a.denominator), -b, rational_function::ctx_wrapper.ctx);
        }
        return result;
    }
}
