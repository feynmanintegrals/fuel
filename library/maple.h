
/** @file maple.h
 *  @author Alexander Smirnov
 *
 *  This file is a part of the FIRE package
 */

#pragma once

#include "base.h"

namespace fuel {

    /**
     * @brief Communication with pari: https://pari.math.u-bordeaux.fr/git/pari.git
     */
    class maple : public base {
        private:
            void initializeInternal(const vector<string>& vars) override; // communicates with the external program, writing initial commands
            void evaluateInternal(const char* expr, bool modular) override;

        public:
            maple() {
                this->launchArguments.push_back("-q");
                this->launchArguments.push_back("-u");
                this->launchArguments.push_back("--historysize=1");
            }

            /**
             * Creates an instance of this class
             * @return smart pointer
             */
            static std::unique_ptr<base> make() {
                return std::make_unique<maple>();
            }

            ~maple();
    };

}
